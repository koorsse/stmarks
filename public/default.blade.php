<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
  <body>
    <header >
        @include('includes.navbar')
    </header>
    <div class="wrapper">

          @include('includes.sidebar')
          <div class="main column col-sm-10 col-xs-11" id="main">
            <!-- @if (isset($pagetitle))
            <div class="banner-div">
              <p>{{ $pagetitle }}</p>
            </div>
            @endif -->
            @yield('content')
          </div>

    </div>

<footer>
@include('includes.footer')
</footer>
@include('layouts.script')
</body>
</html>
