<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('churchchildrens', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('lastname'); //child
          $table->timestamp('cc_date');
          $table->string('dob');
          $table->integer('grade');
          $table->integer('gender')->default(0);
          $table->string('name_mom')->nullable();
          $table->string('name_dad')->nullable();
          $table->string('contact_mom')->nullable();
          $table->string('contact_dad')->nullable();
          $table->string('school')->nullable();
          $table->string('email1')->nullable();
          $table->string('email2')->nullable();
          $table->string('homeaddr')->nullable();
          $table->boolean('visitor')->default(0);
          $table->string('photo')->nullable();
          $table->softDeletes();
          $table->timestamps();
      });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
