<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SitesForScheduler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sites', function (Blueprint $table) {
          $table->increments('id');
          $table->string('site_name');

          $table->softDeletes();
          $table->timestamps();
      });

      Schema::create('site_schedule', function (Blueprint $table) {
          $table->integer('schedule_id');
          $table->integer('site_id');
        
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
