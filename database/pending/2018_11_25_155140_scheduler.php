<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Scheduler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description'); //child
            $table->timestamp('start_date')->nullable();
            $table->integer('week_counter')->default(0);
            $table->integer('day_counter')->default(0);
            $table->boolean('active')->default(false);
            $table->string('site')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id');
            $table->string('name');
            $table->string('start_time');
            $table->integer('order')->default(0);
            $table->integer('duration');
            $table->integer('week')->default(0);
            $table->integer('day')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('medialist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_id');
            $table->string('filepath');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
