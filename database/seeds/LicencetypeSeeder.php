<?php

use Illuminate\Database\Seeder;

class LicencetypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('licencetypes')->insert([
                 [
             'description' => 'SAFA D',
             'level' => 'Grassroots',
             'roleFor' => 'Coach'
           ],
           [
             'description' => 'CAF/SAFA C',
             'level' => 'Introductory',
             'roleFor' => 'Coach'
           ],
           [
             'description' => 'CAF/SAFA B',
             'level' => 'Level 1',
             'roleFor' => 'Coach'
           ],
           [
             'description' => 'CAF/SAFA A',
             'level' => 'Level 2',
             'roleFor' => 'Coach'
           ],
           [
             'description' => 'Professional',
             'level' => 'Level 3',
             'roleFor' => 'Coach'
           ]
      ]);
    }
}
