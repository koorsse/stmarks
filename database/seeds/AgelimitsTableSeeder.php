<?php

use Illuminate\Database\Seeder;
use App\Agelimit;
class AgelimitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agelimit=[
        	['agelimit'=>9,
        	'description'=>'Under 9'],
        	['agelimit'=>10,
        	'description'=>'Under 10'],
        	['agelimit'=>11,
        	'description'=>'Under 11'],
        	['agelimit'=>12,
        	'description'=>'Under 12'],
        	['agelimit'=>13,
        	'description'=>'Under 13'],
        	['agelimit'=>14,
        	'description'=>'Under 14'],
        	['agelimit'=>15,
        	'description'=>'Under 15'],
        	['agelimit'=>16,
        	'description'=>'Under 16'],
        	['agelimit'=>17,
        	'description'=>'Under 17'],
        	['agelimit'=>18,
        	'description'=>'Under 18'],
        	['agelimit'=>19,
        	'description'=>'Under 19'],
        	['agelimit'=>20,
        	'description'=>'Under 20'],
        	['agelimit'=>21,
        	'description'=>'Under 21'],
        	['agelimit'=>22,
        	'description'=>'Senior']

        ];
           foreach ($agelimit as $key => $value) {
            Agelimit::create($value);
        }
    }
}
