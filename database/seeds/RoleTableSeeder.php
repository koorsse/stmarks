<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
		        	[
		        		'name' => 'Super Admin',
		        		'display_name' => 'Super Admin',
		        		'description' => 'Has Access to the full system'
		        	],
		            [
		        		'name' => 'RFA Super Admin',
		        		'display_name' => 'Region Super Admin',
		        		'description' => 'Has Access to the full system but restricted to rfa'
		        	],
		        	[
		        		'name' => 'LFA Super Admin',
		        		'display_name' => 'LFA Super Admin',
		        		'description' => 'Has Access to the full system but restricted to lfa'
		        	],
		        	[
		        		'name' => 'RFA Admin',
		        		'display_name' => 'RFA Admin',
		        		'description' => 'Can add LFA,Players and Clubs'
		        	],
		        	[
		        		'name' => 'LFA Admin',
		        		'display_name' => 'LFA Admin',
		        		'description' => 'Can add Clubs and Players'
		        	],
		        	[
		        		'name' => 'RFA User',
		        		'display_name' => 'RFA User',
		        		'description' => 'Can register lfa, players and clubs'
		        	],
		            [
		                'name' => 'LFA User',
		                'display_name' => 'Display list of LFA,Clubs and all stakeholders',
		                'description' => 'Can register players and clubs'
		            ]
		        ];

           foreach ($role as $key => $value) {
        	Role::create($value);
        }
    }
}
