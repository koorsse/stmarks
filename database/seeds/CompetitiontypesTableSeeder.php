<?php

use Illuminate\Database\Seeder;
use App\Competitiontype;
class CompetitiontypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $competitiontype=[
        	['competitiontype'=>'Home/Away'],
        	['competitiontype'=>'Round Robin'],
        	['competitiontype'=>'Tournament']
        ];
       foreach ($competitiontype as $key => $value) {
            Competitiontype::create($value);
        }
    }
}
