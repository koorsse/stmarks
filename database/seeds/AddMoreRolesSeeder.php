<?php

use Illuminate\Database\Seeder;

class AddMoreRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         
         DB::table('roles')->update([
                    [
		        		'name' => 'RFA Super Admin',
		        		'display_name' => 'Region Super Admin',
		        		'description' => 'Has Access to the full system but restricted to rfa'
		        	],
		        	[
		        		'name' => 'LFA Super Admin',
		        		'display_name' => 'LFA Super Admin',
		        		'description' => 'Has Access to the full system but restricted to lfa'
		        	]
         ]);		
    }
}
