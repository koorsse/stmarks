<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $permission = [
		        	[
		        		'name' => 'admin-list',
		        		'display_name' => 'Display Role Listing',
		        		'description' => 'See only Listing Of Role'
		        	],
		        	[
		        		'name' => 'admin-create',
		        		'display_name' => 'Create Role',
		        		'description' => 'Create New Role'
		        	],
		        	[
		        		'name' => 'admin-edit',
		        		'display_name' => 'Edit Role',
		        		'description' => 'Edit Role'
		        	],
		        	[
		        		'name' => 'admin-delete',
		        		'display_name' => 'Delete Role',
		        		'description' => 'Delete Role'
		        	],
		            [
		                'name' => 'rfaAdmin-list',
		                'display_name' => 'Display list of LFA,Clubs and all stakeholders',
		                'description' => 'See only list of Stakeholders in the RFA'
		            ],
		            [
		                'name' => 'rfaAdmin-create',
		                'display_name' => 'Create LFA,Clubs or all stakeholders',
		                'description' => 'Create New Stakeholder in RFA'
		            ],
		            [
		                'name' => 'rfaAdmin-edit',
		                'display_name' => 'Edit LFA,Clubs or any stakeholders',
		                'description' => 'Edit stakeholder in RFA'
		            ],
		            [
		                'name' => 'rfaAdmin-delete',
		                'display_name' => 'Delete LFA,Clubs or any stakeholders',
		                'description' => 'Delete Stakeholder in RFA'
		            ],
		            [
		                'name' => 'lfaAdmin-list',
		                'display_name' => 'Display List of LFA,Clubs and all stakeholders',
		                'description' => 'See only Stakeholders in LFA'
		            ],
		            [
		                'name' => 'lfaAdmin-create',
		                'display_name' => 'Create LFA,Clubs or all stakeholders',
		                'description' => 'Create New Stakeholder in LFA'
		            ],
		            [
		                'name' => 'lfaAdmin-edit',
		                'display_name' => 'Edit LFA,Clubs or any stakeholders',
		                'description' => 'Edit StakeHolder in LFA'
		            ],
		            [
		                'name' => 'lfaAdmin-delete',
		                'display_name' => 'Delete LFA,Clubs or any stakeholders',
		                'description' => 'Delete Stakeholder in LFA'
		            ],
		            [
		                'name' => 'rfaUser-list',
		                'display_name' => 'Display List of LFA,Clubs and all stakeholders in RFA',
		                'description' => 'See only list of Stakeholders in LFA'
		            ],
		        	[
		        		'name' => 'lfaUser-list',
		        		'display_name' => 'Display List of Clubs and all stakeholders in LFA',
		        		'description' => 'See only list of Stakeholders in LFA'
		        	]

		        ];

           foreach ($permission as $key => $value) {
        	Permission::create($value);
        }
    }
}

