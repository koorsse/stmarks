<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use App\Http\Traits\Encryptable;

class Person extends Model
{
  // use Encryptable;
  //
  // protected $encryptable = [
  //     'firstname',
  //     'surname',
  //     'maiden',
  //     'gender',
  //     'email',
  //     'cellno',
  //     'worktel',
  //     'res_id',
  // ];


  use SoftDeletes;
  protected $guarded = ['id'];

  public function church()
  {
     return $this->belongsTo('App\Church');
  }

  public function status()
  {
     return $this->belongsTo('App\Status');
  }

  public function residence()
  {
     return $this->belongsTo('App\Residence', 'res_id');
  }

  public static function allLiving()
  {
    return Person::whereNull('deceased_date');
  }
}
