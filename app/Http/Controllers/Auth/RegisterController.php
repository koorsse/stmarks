<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Temp_Registration; 
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Hash;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(30);

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code
        ]);

        Mail::send('auth.passwords.email_verify', $confirmation_code, function($message) {
            $message->to($data['email'], $data['name'])
                ->subject('Verify your email address');
        });

        Flash::message('Thanks for signing up! Please check your email.');

        return Redirect::home();
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => bcrypt($data['password']),
        //     'confirmation_code' => $confirmation_code
        // ]);
    }
    public function confirm($confirmation_code)
    {
           // dd($confirmation_code);
            if( ! $confirmation_code)
            {
               return view('errors.invalidcode');

            }
           // dd($confirmation_code);
            $temp_user = Temp_Registration::whereConfirmationCode($confirmation_code)->first();
            $user = User::whereConfirmationCode($confirmation_code)->first();
            //dd($temp_user,$user);
            if ( ! $user)
            {
                 return view('errors.invalidcode');
                //throw new InvalidConfirmationCodeException;
            }


            $temp_user->confirmed=1;
           // $temp_user->confirmation_code=null;
            $user->status=1;
             $temp_user->save();
           $user->save();
           // Flash::message('You have successfully verified your account.');
             $status="You have successfully verified your account. Please proceed by clicking ";

            return view('auth.email_verified',compact('status','confirmation_code'));
        }
    public function showsetPasswordView(Request $request, $confirmation_code)
    {
       // dd($request, $confirmation_code);
        $token=$confirmation_code;
        return view('auth.passwords.config_password')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function set(Request $request)
    {
      //  dd($request);
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $user=User::where('email',$request->input('email'))->first();
        //dd($user);
        if($user->confirmation_code==$request->input('token'))
        {
            $user->confirmed = 1;
            //$user->confirmation_code = -1111;
            $user->password=Hash::make($request['password']);
            $user->save();
           return redirect($this->redirectPath())
                            ->with('status', 'Password successfully saved');
        }
        // $response = $this->broker()->reset(
        //     $this->credentials($request), function ($user, $password) {
        //         $this->resetPassword($user, $password);
        //     }
        // );

        
        return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => 'Invalid code']);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
                            ->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
