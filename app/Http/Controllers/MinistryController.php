<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Yajra\Datatables\Datatables;
use Response;
use Input;


use App\Ministry;
use App\Minister;
use App\MinistryMember;
use App\Church;

class MinistryController extends Controller
{
  private function picUrl($fname)
  {
    if ($fname) {
      $directory  = url('images/documents');

      // Storage::disk('public_docs')->getDriver()->getAdapter()->getPathPrefix();

      $storagePath =  $directory.'/'. $fname .'?' . rand(1,32000);
    } else {
      $storagePath =  "";//url('images') . '/unknown.gif';
      // Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix('images/unknown.gif');
    }
    return $storagePath;
  }

  private function photoUrl($fname)
  {
    if ($fname) {
      $directory  = url('images/documents');

      // Storage::disk('public_docs')->getDriver()->getAdapter()->getPathPrefix();

      $storagePath =  $directory.'/'. $fname .'?' . rand(1,32000);
    } else {
      $storagePath =  url('images') . '/unknown.gif';
      // Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix('images/unknown.gif');
    }
    return $storagePath;
  }

  public function listMinistries()
  {
    $ministries = Ministry::get();
    foreach($ministries as $mini) {
      $mini->pic = $this->picUrl($mini->pic);
    }
    return view('ministries.listministries', ['ministries' => $ministries]);
  }

  private function savePic($fname, $imagebase64 )
  {
    $data = $imagebase64;

    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);

    if (strpos($fname, '.jp') === FALSE) {
      $xname = explode('.', $fname);
      $fname = $xname[0] . '.jpeg';
    }

    file_put_contents(base_path() . '/public/images/documents/'. $fname, $data);

    return $fname;

  }


  public function showMinistry($id)
  {
    if ($id > 0) {
      $ministry = Ministry::find($id);
    } else {
      $ministry = new Ministry();
    }
    $photo_url = $this->picUrl($ministry->pic);
    $list_ministers = Minister::join('people as p', 'p.id', 'person_id')->select(DB::raw('concat(firstname, " ", surname) as name'), 'ministers.id')->pluck('name', 'id');

    return View::make('ministries.edit_ministry', ['ministry' => $ministry,
                'photo_url' => $photo_url, 'list_ministers' => $list_ministers]);

  }

  public function updateMinistry(Request $request)
  {
    if (Input::get('cancel')) {
      // return redirect()->action('MinistryController@listMinistries');
      if ($request->id > 0) {
        return redirect()->action('MinistryController@listNotifications', ['id' => $request->id]);
      } else {
        return redirect()->action('MinistryController@listMinistries');
      }
    }
    $ministryid = $request->id;
    if ($ministryid > 0) {
      $ministry = Ministry::find($ministryid);

    } else {
      $ministry = new Ministry();

    }

    $ministry->name = $request->name;
    $ministry->description = $request->description;
    $ministry->minister_id = $request->minister_id;
    $ministry->save();

    if ($request->headshot_file && $request->imagebase64) {

      $imageName = 'ministry_'.$ministry->id . '.' .
              $request->file('headshot_file')->getClientOriginalExtension();
      $ministry->pic = $this->savePic($imageName, $request->imagebase64);

    } else if ($ministry->pic && !$request->pic) {
      $ministry->pic = '';
    }

    $ministry->save();

    return redirect()->action('MinistryController@listNotifications', ['id' => $ministryid]);
  }

  public function listNotifications($id)
  {
     //get list of notifications for this

     $ministry = Ministry::find($id);
     return view('ministries.notifications', ['ministry' => $ministry]);
  }

  public function showMinistrymembers($id)
  {
    $members = MinistryMember::join('people as p', 'p.id', 'person_id')->where('ministry_id', $id)
              ->select('p.id','firstname', 'surname', 'role', 'photo')->get();
    $ministry = Ministry::find($id);

    foreach($members as $mem) {
      $mem->photo = $this->photoUrl($mem->photo);
    }
    return view('ministries.listministrymembers', ['members' => $members, 'ministry' => $ministry]);
  }
}
