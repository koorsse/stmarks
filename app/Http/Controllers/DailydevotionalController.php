<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dailydevotional;
use App\Notifications\FacebookNotification;
use DateTime;
use Auth;
use Input;
use App\Mail\Devotionalmail;
use App\Mail\Notifymail;
use App\User;
use App\Person;
use App\Libraries\DailydevotionalPublish;

use DB;
use Mail;

class DailydevotionalController extends Controller
{
    //
    public function emaildaily($thedate = null)
    {
      if (!$thedate) {
       $todaydate = date('Y-m-d');
     } else {
       $todaydate = $thedate;
     }
      $daily = Dailydevotional::where('message_date', $todaydate)->first();
            // ->where('can_publish',1)->where('has_published', 0)->first();
      if (!$daily) {
        //get last published....
        // $daily = Dailydevotional::where('message_date', '<', $todaydate)->orderBy('message_date', 'desc')->first();
        // if (!$daily) {
          dd('No daily devotional today');
        // }
      } else {

        $dayweek = date_format(new DateTime($daily->message_date),"l");
        $displaydate = date_format(new DateTime($daily->message_date),"d F Y");
        $verseformat = str_replace(":", ".", $daily->verseref);
        // dd($daily->messagebody);
        return view('emails.daily', ['daily' => $daily, 'displaydate' => $displaydate,
                'dayweek' => $dayweek, 'verseformat' => $verseformat ]);
      }

    }

    public function subscribe()
    {
      return view('dailydevotional.mailchimpsubscribe');
    }


    public function showdaily()
    {
      //get today's one....
      $todaydate = date('Y-m-d');
      $daily = Dailydevotional::where('message_date', $todaydate)->first();
            // ->where('can_publish',1)->where('has_published', 0)->first();
      if (!$daily) {
        //get last published....
        $daily = Dailydevotional::where('message_date', '<', $todaydate)->orderBy('message_date', 'desc')->first();
        if (!$daily) {
          dd('No daily devotional today');
        }
      }

      $dayweek = date_format(new DateTime($daily->message_date),"l");
      $displaydate = date_format(new DateTime($daily->message_date),"d F Y");
      $verseformat = str_replace(":", ".", $daily->verseref);
      // dd($daily->messagebody);
      return view('dailydevotional.showdaily', ['daily' => $daily, 'displaydate' => $displaydate,
              'dayweek' => $dayweek, 'verseformat' => $verseformat ]);
    }

    public function sendFB()
    {

      $dd = new Dailydevotional();
      $todaydate = date('Y-m-d');
      $dd = Dailydevotional::where('message_date', $todaydate)->first();
      if ($dd) {
        $dd->notify(new FacebookNotification());
        return response()->json(array('success' => true));
      } else {
        return response()->json(array('success' => false));
      }


    }

    public function indexDaily()
    {
      //list all daily devotionals

    }

    public function viewDaily()
    {
      //view a previous Daily

    }

    public function editDaily($thedate = null)
    {

      //edit daily devotional
      //show last entry in database....
      $state = "Published";
      $todaydate = date('Y-m-d');
      // $thelist = Dailydevotional::where('status','!=',$state)
      //           ->where('message_date', '>=', $todaydate)
      //           ->orderBy('message_date','desc')
      //           ->select('id', DB::Raw("concat(date(message_date),' ', upper(status), ' - ',  author, ': ', title) as descr"))
      //           ->pluck('descr', 'id');

      $thedateslist = Dailydevotional::where('status','!=',$state)
                ->where('message_date', '>=', $todaydate)
                ->orderBy('message_date','desc')
                ->select('message_date', DB::Raw("concat(upper(status), ' - ',  author, ': ', title) as descr"))
                ->pluck('descr', 'message_date');

      if (!$thedate) {
        $daily = Dailydevotional::orderBy('message_date','desc')->first();
      } else {
        $daily = Dailydevotional::where('message_date', $thedate)->first();
      }
      $readonly = false;
      $canpublish = true;
      $didpublish = 0;
      if (isset($daily)) {
        if ($daily->has_published) {
          $readonly = true;
          $canpublish = false;
          $didpublish = 1;
        } else if (isset($thedate)) {
          $dt = strtotime($thedate);
          $nowdt = strtotime(date('Y-m-d'));
          // dd($nowdt);
          if ($dt < $nowdt) {
            $canpublish = false;
            $didpublish = 2;
          }
        }
      }

      if (!$daily) {
        $daily = new Dailydevotional();
        $daily->id = 0;
        if ($thedate) {
          $daily->message_date = $thedate;
        } else {
        //  $daily->message_date = date('d/m/Y');
        }

      }

      $displaydate = date_format(new DateTime($daily->message_date),"l, d F Y");
      $thisnicedate = date_format(new DateTime($daily->message_date),"Y-m-d");
      $prev_date = date('Y-m-d', strtotime($daily->message_date .' -1 day'));
      $next_date = date('Y-m-d', strtotime($daily->message_date .' +1 day'));
// dd($didpublish);
      // if ($didpublish == 2) {
      //   $steps = array('complete draft','complete ready', 'active notpublish', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon');
      // } else if ($daily->can_publish) {
      //   $steps = array('complete draft','active ready', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff');
      // } else if ($daily->has_published) {
      //   $steps = array('complete draft','complete ready', 'active publish', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon');
      // } else {
      //   $steps = array('active draft','disabled', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff');
      // }

      if ($daily->status == "Ready") {
         $steps = array('complete','complete','active', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff');
       } else if ($daily->has_published) {
         $steps = array('complete','complete', 'complete', 'active ', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon');
       } else if ($daily->status == "Review") {
         $steps = array('complete','active', 'disabled', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff');
       } else {
         $steps = array('active','disabled', 'disabled', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff');
       }

// dd($daily);
$reviewer_id = 5; //Sandy Woolard (would get this from database eventually)
$manager_id = 7;  //email manager that a particular dd has been set for review...
      $canready = false;
      if (Auth::user()->id == $reviewer_id || Auth::user()->id == $manager_id) {
        $canready = true;
      }

      return view('dailydevotional.editdaily', ['daily' => $daily, 'didpublish' => $didpublish, 'canpublish' => $canpublish,
            'displaydate' => $displaydate, 'prevdate' => $prev_date, 'nextdate' => $next_date, 'steps' => $steps,
            'canready' => $canready, 'thisnicedate' => $thisnicedate,  'thedateslist' => $thedateslist]);

    }

    public function updateDaily(Request $request)
    {
      //update new daily devotional
      if (Input::get('cancel')) {
        return redirect()->action('DailydevotionalController@editDaily');
      }

      if (Input::get('fbbtn')) {
        DailydevotionalPublish::postToFB();
        return redirect()->action('DailydevotionalController@editDaily');
      } else {

      if ($request->id == 0) {
        $chkDaily = Dailydevotional::where('message_date', $request->message_date)->first();
        if (!$chkDaily) {
          $newdaily = new Dailydevotional();
          $newdaily->can_publish = 0;
          $newdaily->status = "Draft"; //would only read the default value from the DB after saved so on first time not set
        } else {
          $newdaily = $chkDaily;
        }
      } else {
        $newdaily = Dailydevotional::find($request->id);
      //  $newdaily->can_publish = $request->chkpublish == 'on' ? 1 : 0;
      }

      if ($newdaily->status != "Draft" || (Input::get('review'))) {
        $validatedData = [
                'author' => 'required|max:255',
                'title' => 'required|max:255',
                'messagebody' => 'required',
                'versetext' => 'required',
                'verseref' => 'required|max:255',
            ];

        $this->validate($request, $validatedData);
      }

      $newdaily->author = $request->author;
      $newdaily->user_id = Auth::user()->id;
      $newdaily->messagebody = $request->messagebody;
      $newdaily->message_date = $request->message_date;
      $newdaily->title = $request->title;
      $newdaily->versetext = $request->versetext;
      $newdaily->verseref = $request->verseref;
      $newdaily->prayer = $request->prayer;

      $newdaily->save();

      $readonly = false;
      $canpublish = true;
      $didpublish = 0;
      if (isset($newdaily)) {
        if ($newdaily->has_published) {
          $readonly = true;
          $canpublish = false;
          $didpublish = 1;
        } else if (isset($thedate)) {
          $dt = strtotime($thedate);
          $nowdt = strtotime(date('Y-m-d'));
          // dd($nowdt);
          if ($dt < $nowdt) {
            $canpublish = false;
            $didpublish = 2;
          }
        }
      }

      // if (!$newdaily) {
      //   $newdaily = new Dailydevotional();
      //   $newdaily->id = 0;
      //   // if ($thedate) {
      //   //   $newdaily->message_date = $thedate;
      //   // } else {
      //   // //  $daily->message_date = date('d/m/Y');
      //   // }

      // }

      $displaydate = date_format(new DateTime($newdaily->message_date),"l, d F Y");
      $thisnicedate = date_format(new DateTime($newdaily->message_date),"Y-m-d");
      $prev_date = date('Y-m-d', strtotime($newdaily->message_date .' -1 day'));
      $next_date = date('Y-m-d', strtotime($newdaily->message_date .' +1 day'));

      $reviewer_id = 5; //5 Sandy Woolard (would get this from database eventually)
      $manager_id = 7;  //7 email manager that a particular dd has been set for review...
      $usermanager = User::find($manager_id);
      if ($usermanager) {
        $pid = Person::find($usermanager->personid);
        if ($pid) {
          $manager_name = $pid->firstname;
        } else {
          $manager_name = "";
        }

      } else {
        $manager_name = "";
      }

    //  dd($manager_name);
      $canready = false;
      if (Auth::user()->id == $reviewer_id || Auth::user()->id == $manager_id) {
        $canready = true;
      }
      //if set to review or if saving while in review state by someone other than the reviewer....
      if (Input::get('review') || ($newdaily->status == "Review" && Auth::user()->id != $reviewer_id)) {
        if (Input::get('review')) {
          $reason = "Daily devotional for " . $displaydate . " is ready for review.";

          $message = "This is to let you know that the Daily devotional for " . $displaydate . " status was changed from DRAFT to REVIEW by " . Auth::user()->name;
          $emails=[User::find($manager_id)->email, 'mkoorsse@gmail.com'];//, 'charl.vandermerwe@gmail.com']; //
          Mail::to($emails)->send(new Notifymail($manager_name, $message, 'Daily devotional: State changed'));
        } else {
          $reason = "Daily devotional for " . $displaydate . " was changed while in review state by " . Auth::user()->name;
        }
        //send email to reviewer....mail email for them to review.....
        $emails=[User::find($reviewer_id)->email, 'mkoorsse@gmail.com'];//, 'charl.vandermerwe@gmail.com'];
        Mail::to($emails)->send(new Devotionalmail($newdaily->message_date, $reason, Auth::user()->email));

        $newdaily->status = "Review";
        $newdaily->save();
      }

      if (Input::get('ready')) {
        $newdaily->status = "Ready";
        $newdaily->save();

        $message = "This is to let you know that the Daily devotional for " . $displaydate . " status was changed from REVIEW to READY TO PUBLISH by " . Auth::user()->name;
        if (User::find($manager_id)) {
          $emails=[User::find($manager_id)->email];//, 'charl.vandermerwe@gmail.com'];
          Mail::to($emails)->send(new Notifymail($manager_name, $message, 'Daily devotional: Ready to Publish'));
        } else {
          $emails = ['mkoorsse@gmail.com'];
          Mail::to($emails)->send(new Notifymail('Melisa', 'I cannot find a manager for the daily devotional with id '. $manager_id . '. Please check.', 'Attention: Daily devotional - No manager'));
        }

      }

      if ($newdaily->status == "Ready") { //just set to or saved while in review....
        //Prepare FACEBOOK post for Sean....
        $dd = $newdaily;
        $msg = 'Daily Devotional - ' . date_format(new DateTime($dd->message_date),"d F Y") . "\r\n\n";
        $msg .= strtoupper($dd->title) . "\r\n\n";
        $msg .= '"' . $dd->versetext . '" - ' . $dd->verseref . "\n\n";
        $msg .= strip_tags(html_entity_decode($dd->messagebody)) . "\r\n\n";
        $msg .= 'PRAYER' . "\r\n\n";
        $msg .= $dd->prayer . ' AMEN' . "\r\n\n";
        $msg .= 'By ' . $dd->author . "\r\n\n";
        $msg .= "To receive our Daily Devotional please click the link below and fill in your details. Remember to select the mailing list you'd like to subscribe to. database.stmarks.org.za/subscribe" . "\r\n";
        // $msg .= 'To receive your Daily Devotional, please subscribe to our mailing list at database.stmarks.org.za/subscribe' . "\r\n";
        $msg .= '#StMarksPE #DailyDevotional';
      //  dd($msg);
        // $msg = str_replace( "\n", '<br />', $msg );

        $emails = ['seand@stmarks.org.za'];
        Mail::to($emails)->send(new Notifymail('Sean', $msg, 'Facebook post for ' . $displaydate));
      }



      if ($newdaily->status == "Ready") {
         $steps = array('complete','complete','active', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff');
       } else if ($newdaily->has_published) {
         $steps = array('complete','complete', 'complete', 'active ', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon');
       } else if ($newdaily->status == "Review") {
         $steps = array('complete','active', 'disabled', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff');
       } else {
         $steps = array('active','disabled', 'disabled', 'disabled', 'bs-wizard-stepnumon', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff', 'bs-wizard-stepnumoff');
       }
       $state = "Published";
       $todaydate = date('Y-m-d');
       $thedateslist = Dailydevotional::where('status','!=',$state)
                 ->where('message_date', '>=', $todaydate)
                 ->orderBy('message_date','desc')
                 ->select('message_date', DB::Raw("concat(upper(status), ' - ',  author, ': ', title) as descr"))
                 ->pluck('descr', 'message_date');

      return view('dailydevotional.editdaily', ['daily' => $newdaily, 'didpublish' => $didpublish, 'canpublish' => $canpublish,
            'displaydate' => $displaydate, 'prevdate' => $prev_date, 'nextdate' => $next_date, 'steps' => $steps,
            'canready' => $canready, 'thisnicedate' => $thisnicedate, 'thedateslist' => $thedateslist]);


     }

    }

}
