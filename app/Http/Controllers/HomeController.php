<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\Devotionalmail;

use DB;
use Mail;

class HomeController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // return View::make('home');
        //  dd(env("FACEBOOK_ACCESS_TOKEN", "somedefaultvalue"));
        return view('home');
    }

    public function testmail()
    {

       $emails=['melisa.vdmerwe@gmail.com', 'melisa@inqaku.com'];//, 'charl.vandermerwe@gmail.com'];
      // //  $emails=['warrenw@stmarks.org.za', 'melisa@converttocode.com', 'melisa@inqaku.com'];
       Mail::to($emails)->send(new Devotionalmail('2020-08-06', 'Ready', 'Pietie'));
    //   $data=[];
    //    Mail::send('emails.daily', $data, function($message) {
    //       $message->to('melisa.vdmerwe@gmail.com')
    //               ->subject('St Marks Daily Devotional');
    //       $message->from('mkoorsse@gmail.com','Melisa Koorsse');
    //   });
    }




}
