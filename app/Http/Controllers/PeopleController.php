<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use View;
use Yajra\Datatables\Datatables;
use Response;
use Input;
use DateTime;
use Excel;
use Validator;

use App\Person;
use App\Status;
use App\Church;
use App\Residence;
use App\Ministry;
use App\Minister;
use App\MinistryMember;
use App\User;
use App\Churchchildren;
use App\Exports\BdayExport;


class PeopleController extends Controller
{
    public function index()
    {
      $counts['all'] = count(Person::allLiving()->get());
      $counts['member'] = count(Person::allLiving()->where('status_id',1)->get());
      $counts['non'] = count(Person::allLiving()->where('status_id',2)->get());

      $months = ["1" => "January", "2" => "February", "3" => "March", "4" => "April", "5" => "May", "6" => "June",
                  "7" => "July", "8" => "August", "9" => "September", "10" => "October", "11" => "November", "12" => "December"];

      return view('people.listpeople')->with(['counts' => $counts, 'months' => $months]);
    }

    public function ajaxPeopleList()
    {
    $counts['all'] = count(Person::allLiving()->get());
      $counts['member'] = count(Person::allLiving()->where('status_id',1)->get());
      $counts['non'] = count(Person::allLiving()->where('status_id',2)->get());

      $months = ["1" => "January", "2" => "February", "3" => "March", "4" => "April", "5" => "May", "6" => "June",
                  "7" => "July", "8" => "August", "9" => "September", "10" => "October", "11" => "November", "12" => "December"];

      if (request()->ajax()) {
        //filter that only logged in user's linked people...
        // $user=Auth::user();
        // $userPerson=Person::find($user->person_id);

        $people = DB::table('people as p')
                  ->leftjoin('status as s', 's.id', 'p.status_id')
                  ->leftjoin('churches as c', 'c.id', 'p.church_id')
                  ->leftjoin('residences as r', 'r.id', 'res_id')
                  ->select('p.id', 'p.firstname', 'p.surname', 's.status', 'c.churchname', 'p.cellno', 'r.street')->get();

        return Datatables::of($people)->make();
      }

       return view('people.listpeople')->with(['counts' => $counts, 'months' => $months]);
    }

    public function birthdaylist($month)
    {
      //$month integer???


      $dateObj   = DateTime::createFromFormat('!m', $month);
      $monthName = $dateObj->format('F');
      $filename = 'birthdaylist_'.$monthName.'.xlsx';
      return (new BdayExport($month))->download($filename);

      // $bday_list = DB::select("select date_format(str_to_Date(birthday,'%d/%m/%Y'),'%d') as bday,
      //             title, if(preferred_name = '',firstname,preferred_name) as `name`, surname,
      //             if(year(str_to_Date(birthday,'%d/%m/%Y')) != 1900,year(current_timestamp) - year(str_to_Date(birthday,'%d/%m/%Y')),'') as age,
      //             telephone, cellno, worktel
      //             from people p inner join residences r on p.res_id = r.id
      //             where deceased_date is null and birthday != '01/01/1901' and month(str_to_Date(birthday,'%d/%m/%Y')) = ?
      //             and status_id = 1
      //             order by date_format(str_to_Date(birthday,'%d/%m/%Y'),'%d');",[$month]);
      //                     // where birthday != '01/01/1901' and month(str_to_Date(birthday,'%d/%m/%Y')) = :month
      //             // ->setBindings(['month' => $month])
      //             // ->get();

      //     $data = json_decode(json_encode($bday_list), true);

      //     $type = 'xlsx';
         

      //     return Excel::create($filename, function($excel) use ($data) {
      //       $excel->sheet('birthdaylist', function($sheet) use ($data)
      //       {
      //         $sheet->fromArray($data);
      //       });
      //     })->export($type);
      

    }

    private function picUrl($fname)
    {
      if ($fname) {
        $directory  = url('images/documents');

        // Storage::disk('public_docs')->getDriver()->getAdapter()->getPathPrefix();

        $storagePath =  $directory.'/'. $fname .'?' . rand(1,32000);
      } else {
        $storagePath =  url('images') . '/unknown.gif';
        // Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix('images/unknown.gif');
      }
      return $storagePath;
    }

    private function savePic($fname, $imagebase64 )
    {
      $data = $imagebase64;

      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);

      if (strpos($fname, '.jp') === FALSE) {
        $xname = explode('.', $fname);
        $fname = $xname[0] . '.jpeg';
      }

      file_put_contents(base_path() . '/public/images/documents/'. $fname, $data);

      return $fname;

    }


    public function showPerson($id)
    {
      
        $livethere = null;

        if ($id > 0) {
          // $person = DB::table('people')->where('id',$id)->first();
          $person = Person::find($id);


         $resid = Residence::find($person->res_id);

          if ($resid) {
            $person->street = $resid->street;
            $person->suburb = $resid->suburb;
            $person->city = $resid->city;
            $person->province = $resid->province;
            $person->country = $resid->country;
            $person->code = $resid->code;
            $person->mailtopostal = $resid->mailtopostal;
            $person->postal_address = $resid->postal_address;
            $person->postal_suburb = $resid->postal_suburb;
            $person->postal_city = $resid->postal_city;
            $person->postal_country = $resid->postal_country;
            $person->telephone = $resid->telephone;
            $person->address_to = $resid->address_to;
            $person->gps_location = $resid->gpslocation;

            $livethere = Person::where('res_id', $resid->id)->
                    where('id', '<>', $id)->select('id', 'firstname', 'surname','photo')->get();
            foreach($livethere as $occ) {
              $occ->photo = $this->picUrl($occ->photo);
            }
          }
        } else {
          $person = new Person();
          $person->id = $id;
          $person->mailtopostal = 0;
          $person->gender = 'M';
          $person->res_id = -1;
          $livethere = [];
        }
        $ministries = Ministry::select('id', 'name');
        // dd($ministries);
        foreach($ministries as $min) {
          $mem = MinistryMember::where('ministry_id', $min->id)->where('person_id', $id)->first();
          $min['member'] = false;

          if ($mem) {
            $min['member'] = true;
          }

        }
        
        
        $strtype = "Minister";
        $minister = Minister::where('person_id', $id)->where('type',$strtype)->count();
        $strtype = "Pastor";
        $pastor = Minister::where('person_id', $id)->where('type',$strtype)->count();
        $photo_url = $this->picUrl($person->photo);

        $statuslist = Status::pluck('status', 'id');
        // dd('ping');
        //$statuslist = Status::select('id','status')->count();
        //dd($statuslist);
        $titlelist = [
          'Mr' => 'Mr',
          'Mrs' => 'Mrs',
          'Miss' => 'Miss',
          'Ms' => 'Ms',
          'Dr' => 'Dr',
          'Prof' => 'Prof',
          'Adv' => 'Adv',
          'Rev' => 'Rev',

        ];

        $churches = Church::pluck('churchname', 'id');
  // return view('people.testmap');

        $isuser = User::where('person_id', $person->id)->first();
        if ($isuser) {
          $userrole = $isuser->hasRole('Admin') ? "Admin" : "Read";
        } else {
          $userrole = "";
        }
        if (Auth::user()->hasRole('ReadUser')) {
          $readonly = true;
        } else {
          $readonly = false;
        }

        return View::make('people.edit_person', ['person' => $person, 'statuslist' => $statuslist,
                    'churches' => $churches, 'photo_url' => $photo_url, 'titlelist' => $titlelist,
                    'livethere' => $livethere, 'ministries' => $ministries, 'minister' => $minister,
                    'pastor' => $pastor, 'isuser' => $isuser, 'userrole' => $userrole, 'readonly' => $readonly]);
    }

    public function checkPersonExists(Request $request)
    {

      //ajax call from page to show list of people and ask if user wants to continue to add the new person!!
      $str1 = '%'.trim($request->name).'%';
      $str2 = '%'.trim($request->surname).'%';
      $search = Person::where('firstname', 'like',  $str1)->where('surname','like',  $str2)->get();

      if (count($search) > 0) {
        $returnData = array(
          'search' => $search,
          'exists' => true,
        );
      } else {
        $returnData = array(
          'exists' => false,
        );
      }
        // return response()->json(array('success' => true));
      return Response::json($returnData);


    }

    public function updatePerson(Request $request)
    {


      if (Input::get('cancel')) {

      return $this->index();
        // return view('people.listpeople');
      }
      //pre: have already checked for duplicate person if new person being entered.
      $validatedData = [
              'firstname' => 'required|max:255',
              'surname' => 'required|max:255',
          ];

      $this->validate($request, $validatedData);


      $personid = $request->id;
      if ($personid > 0) {
        $person = Person::find($personid);

      } else {

        $person = new Person();
        // $person->id = $personid;
        $person->res_id = -1;
      }
      if (Auth::user()->hasRole("ReadUser")) {
        $person->comments = $request->comments;
        $person->save();
      } else {
        $person->firstname = trim($request->firstname);
        $person->preferred_name = trim($request->preferred_name);
        $person->surname = trim($request->surname);
        $person->title = $request->title;
        $person->birthday = $request->birthday;
        $person->gender = $request->gender;
        $person->maiden = trim($request->maiden);
        $person->email = $request->email;
        $person->cellno = $request->cellno;
        $person->worktel = $request->worktel;
        $person->occupation = $request->occupation;
        $person->church_id = $request->church_id;
        $person->status_id = $request->status_id;
        $person->joindate = $request->joindate;
        $person->voice_edistr = $request->voic_edistr ? 1 : 0;
        $person->comments = $request->comments;
        // $person->deceased = $request->deceased ? 1 : 0;
        $person->deceased_date = $request->deceased ? $request->deceased_date : null;
        $person->married_date = $request->married ? $request->married_date : null;


        //if new person or adding address for first time: could be new address (reg_id = -1) or an existing reg_id then just assign
          //NEW or UPDATE RESID
        //if already a person then could be changing address:

        //1) Person stays alone and changing to new address //UPDATE CURRENT*
        //2) Person stays alone and changing to existing address //UPDATE TO NEW RESID*
        //3) Person shares and changes to new address but only the person //NEW*
        //4) Person shares and changes to new address AND all who share with updated //UPDATE CURRENT RECORD*
        //5) Person shares and changes to existing address but only the person //UPDATE RESID OF PERSON ONLY
        //6) Person shares and changes to existing address AND all who share with updated //UPDATE ALL TO NEW RESID*
        $updateallothers = false;
        $updateaddress = true;



        if ($person->res_id == -1) { //address not set
          if ($request->street != "" || $request->postal_address != "") {
            //and there is an address now...
            if ($request->res_id > 0) {
              //existing address
                $resid = Residence::find($request->res_id);
            } else {
              //new address
                $resid = new Residence();
            }
          } else {
            $updateaddress = false;
          }
        } else {
          //is the person changing their address??
          // echo $request->updateallresidence == 'false';
          // return;
          if ($person->res_id == $request->res_id || ($request->updateallresidence == 'true' && $request->res_id == -1)) {
            //no just fetch current in case updating... OR //case 1 and 4:
            $resid = Residence::find($person->res_id);
            $updateallothers = true;
            // echo 'one';
          } else if ($request->res_id == -1 && $request->updateallresidence == 'false') {
            //case 3:
            $resid = new Residence();
            // echo 'two';
          } else if ($request->res_id > 0 && $request->updateallresidence == 'true') {
            //case 2 and case 6:
            $resid = Residence::find($request->res_id);
            $updateallothers = true;
            // echo 'three';
          } else {
            //case 5:
            $resid = Residence::find($request->res_id);
            // echo 'five';
          }
          // return;
          //empty address??
        }

        // if ($request->street != "" || $request->postal_address != "") {
        //   if ($person->id < 1 && )
        //   //is there an address to add....
        //   if ($resid->id != $person->res_id) {
        //     //has changed to another existing
        //   }
        //
        //   if ($request->res_id > 0) {
        //     //residence id already in Database
        //     $resid = Residence::find($request->res_id);
        //     // echo $request->updateallresidence != true ? 'right' : 'wrong';
        //     // // echo $resid->street != $request->street && $request->updateallresidence;
        //     // return;
        //     if ($resid->id != $person->res_id && $request->updateallresidence != true) {
        //         //changed street address so could just be that person then create a new entry!!
        //         //updateallresidence is true if no other residents or if user has indicated that all should be updated.
        //
        //     }
        //   } else {
        //     //need to create a new one...for this person!!!
        //     $resid = new Residence();
        //   }
        // dd($request);
        if ($updateaddress){
          $resid->street = trim($request->street);
          $resid->suburb = trim($request->suburb);
          $resid->city = trim($request->city);
          $resid->province = $request->province;
          $resid->country = $request->country;
          $resid->code = $request->code;

          $resid->mailtopostal = $request->mailtopostal ? 1 : 0;
          $resid->postal_address = $request->postal_address;
          $resid->postal_suburb = $request->postal_suburb;
          $resid->postal_city = $request->postal_city;
          $resid->postal_country = $request->postal_country;
          $resid->telephone = $request->telephone;
          $resid->address_to = $request->address_to;
          $resid->gps_location = $request->gpslocation;
          $resid->save();

          $r_id = $resid->id;

        } else {
          $r_id = -1;
        }
        $prev = $person->res_id;
        $person->res_id = $r_id;
        if ($updateallothers) {
          $residents = Person::allLiving()->where('res_id', $prev)->update(['res_id' => $r_id]);
        }

        if ($request->headshot_file && $request->imagebase64) {

          $imageName = $person->id . '.' .
                  $request->file('headshot_file')->getClientOriginalExtension();
          $person->photo = $this->savePic($imageName, $request->imagebase64);

        } else if ($person->photo && !$request->photo) {
          $person->photo = '';
        }
        $person->save();

        //update ministries.....
        $ministries = Ministry::select('id', 'name')->get();
        // dd($ministries);
        foreach($ministries as $min) {
          $getrequest = Input::get(str_replace(' ','_',$min->name));
          $mem = MinistryMember::where('ministry_id', $min->id)->where('person_id', $request->id)->first();

          if ($getrequest && !$mem) {
            //create
            $mms = MinistryMember::where('ministry_id', $min->id)->where('person_id', $request->id)->withTrashed()->first();
            if ($mms) {
              $mms->restore();
            } else {
              $newmem = new MinistryMember;
              $newmem->person_id = $person->id;
              $newmem->ministry_id = $min->id;
              $newmem->save();
            }
          }
          if ($mem && !$getrequest) {
            //remove
            $mem->delete();
          }

        }
        $strtype = "Minister";
        $minister = Minister::where('person_id', $request->id)->where('type',$strtype)->first();
        if (!$minister && Input::get('minister')) {
          $min = Minister::where('person_id', $request->id)->where('type',$strtype)->withTrashed()->first();
          if ($min) {
            $min->restore();
          } else {
            $newmin = new Minister;
            $newmin->person_id = $person->id;
            $newmin->save();
          }
        }
        if ($minister && !Input::get('minister')) {
          $minister->delete();
        }

        $strtype = "Pastor";
        $minister = Minister::where('person_id', $request->id)->where('type',$strtype)->first();
        if (!$minister && Input::get('pastor')) {
          $min = Minister::where('person_id', $request->id)->where('type',$strtype)->withTrashed()->first();
          if ($min) {
            $min->restore();
          } else {
            $newmin = new Minister;
            $newmin->person_id = $person->id;
            $newmin->type = 'Pastor';
            $newmin->save();
          }
        }
        if ($minister && !Input::get('pastor')) {
          $minister->delete();
        }
      } //end else if ReadUser




      return $this->index();

    //  return view('people.listpeople');
    }

    public function getAddressDetails(Request $request)
     {

        $residence = Residence::where('street', $request->street)->get();//->first();//->where('city', $request->city)->first();

        if (!$residence || $residence->count() == 0) {
          $returnData = array(
            'exists' => false,
          );
        } else {
          // $list = new {};
          // $reslist = [];
          // array_push($list, { 'text' : 'Not in the list', 'value' : -1});
          // foreach ($residence as $res) {
          //   dd($res);
          //   $list = [];
          //   $list['text'] = $res->street .", " . $res->suburb . ", " . $res->city;
          //   $list['value'] = $res->id;
          //   array_push($reslist, $list);
          // }

        //  $reslist = Residence::where('street', $request->street)->select(DB::raw('concat(street,", ",suburb, ", ", city) as text'), 'id as value')->get();
          $reslist = DB::table('people as p')->leftJoin('residences as r','r.id', 'p.res_id')->where('street', $request->street)->select(DB::raw('concat(surname,": ",street,", ",suburb, ", ", city) as text'), 'r.id as value')->distinct('r.id')->get();
        //  dd($reslist);
          $returnData = array(
            'reslist' => $reslist,
            'residence' => $residence,
            'exists' => true,
          );
        }
        // return response()->json(array('success' => true));
      return Response::json($returnData);
    }

    public function indexChildren()
    {
      return view('children.listchildrenchurch');
    }

    public function ajaxChildrenList()
    {

      if (request()->ajax()) {
        //filter that only logged in user's linked people...
        // $user=Auth::user();
        // $userPerson=Person::find($user->person_id);

        $children = DB::table('churchchildrens as c')
                  ->select('c.id', 'c.name', 'c.lastname', 'c.grade')->get();

        return Datatables::of($children)->make();
      }

       return view('children.listchildrenchurch');
    }

    public function showChildren($id)
    {
        $grades = ["0" => "R", "1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5",
                  "6" => "6", "7" => "7"];//, "8" => "8", "9" => "9", "10" => "10", "11" => "11" , "12" => "12"];
          if ($id > 0) {
            // $person = DB::table('people')->where('id',$id)->first();
            $child = Churchchildren::find($id);



          } else {
            $child = new Churchchildren();
            $child->id = $id;

            $child->gender = 'M';

          }


          $photo_url = $this->picUrl($child->photo);


          if (Auth::user()->hasRole('ReadUser')) {
            $readonly = true;
          } else {
            $readonly = false;
          }

          return View::make('children.edit_child', ['child' => $child,  'photo_url' => $photo_url,
           'readonly' => $readonly, 'grades' => $grades]);

    }

    public function updateChild(Request $request)
    {



      if (Input::get('cancel')) {

        return $this->indexChildren();

      }
      //pre: have already checked for duplicate person if new person being entered.
      $validatedData = [
              'name' => 'required|max:255',
              'lastname' => 'required|max:255',
          ];

      $this->validate($request, $validatedData);


      $personid = $request->id;
      if ($personid > 0) {
        $person = Churchchildren::find($personid);

      } else {

        $person = new Churchchildren();


      }

        $person->name = trim($request->name);
        $person->lastname = trim($request->lastname);
        $person->dob = $request->dob;
        $person->gender = $request->gender;
        $person->visitor = 0;
        $person->grade = $request->grade;
        $person->email1 = $request->email1;
        $person->email2 = $request->email2;
        $person->name_mom = trim($request->name_mom);
        $person->name_dad = trim($request->name_dad);
        $person->contact_mom = $request->contact_mom;
        $person->contact_dad = $request->contact_dad;
        $person->homeaddr = trim($request->homeaddr);



        if ($request->headshot_file && $request->imagebase64) {

          $imageName = $person->id . 'child.' .
                  $request->file('headshot_file')->getClientOriginalExtension();
          $person->photo = $this->savePic($imageName, $request->imagebase64);

        } else if ($person->photo && !$request->photo) {
          $person->photo = '';
        }
        $person->save();





       return $this->indexChildren();
    }


    public function checkChildExists(Request $request)
    {

      //ajax call from page to show list of people and ask if user wants to continue to add the new person!!
      $str1 = '%'.trim($request->name).'%';
      $str2 = '%'.trim($request->surname).'%';
      $search = Churchchildren::where('name', 'like',  $str1)->where('lastname','like',  $str2)//->where('dob', $request->dob)
                ->select('name', 'lastname')->get();

      if (count($search) > 0) {
        $returnData = array(
          'search' => $search,
          'exists' => true,
        );
      } else {
        $returnData = array(
          'exists' => false,
        );
      }
        // return response()->json(array('success' => true));
      return Response::json($returnData);


    }

    public function registerchildchurch()
    {
      //show page that allows user to enter name and surname then look to see if child already been registered...
      
    }

    public function addnew()
    {
      //show the register page
      $id = 0;
      $grades = ["0" => "R", "1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5",
                "6" => "6", "7" => "7"];//, "8" => "8", "9" => "9", "10" => "10", "11" => "11" , "12" => "12"];
        if ($id > 0) {
          // $person = DB::table('people')->where('id',$id)->first();
          $child = Churchchildren::find($id);



        } else {
          $child = new Churchchildren();
          $child->id = $id;

          $child->gender = 'M';

        }


        $photo_url = $this->picUrl($child->photo);


        if (Auth::user()->hasRole('ReadUser')) {
          $readonly = true;
        } else {
          $readonly = false;
        }

        return View::make('children.publicadd', ['child' => $child,  'photo_url' => $photo_url,
         'readonly' => $readonly, 'grades' => $grades]);

    }
}
