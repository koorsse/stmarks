<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Yajra\Datatables\Datatables;
use Response;
use Input;

use App\Minister;

class MinisterController extends Controller
{

  private function picUrl($fname)
  {
    if ($fname) {
      $directory  = url('images/documents');

      // Storage::disk('public_docs')->getDriver()->getAdapter()->getPathPrefix();

      $storagePath =  $directory.'/'. $fname .'?' . rand(1,32000);
    } else {
      $storagePath =  url('images') . '/unknown.gif';
      // Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix('images/unknown.gif');
    }
    return $storagePath;
  }

  public function listMinisters()
  {
    $ministers = Minister::join('people', 'person_id', 'people.id')
                ->select('ministers.id','firstname', 'surname', 'title', 'photo')->get();
    foreach($ministers as $mini) {
      $mini->photo = $this->picUrl($mini->photo);
    }
    return view('ministers.listministers', ['ministers' => $ministers]);
  }

  public function showMinister($id)
  {
    dd("work in progress");
    if ($id > 0) {

      $minister = Minister::find($id)->join('people', 'person_id', 'people.id')
                  ->select('firstname', 'surname', 'title', 'photo')->get();
      $photo_url = $this->picUrl($minister->photo);
    } else {
      $minister = new Minister();
      $photo_url = url('images') . '/unknown.gif';
    }

    return 'View and controller method - to be completed';


    // return View::make('ministries.edit_ministry', ['ministry' => $ministry,
    //             'photo_url' => $photo_url, 'list_ministers' => $list_ministers]);

  }
}
