<?php
namespace App\Http\Controllers;

// use App\Http\Traits\TraitFunctions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Person;
use App\Temp_Registration;
use App\Mail\Invitation;
use View;
use App\Organisation;
use App\League;
use DB;
use Hash;
use Yajra\Datatables\Datatables;
// use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Http\Traits\TraitCountries;
use JsValidator;
use Illuminate\Support\Facades\Auth;
use Input;
use Validator;
use Response;
use Mail;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
class UserController extends Controller
{

    // use EntrustUserTrait;
    // use TraitFunctions;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    

     public function index()
     {

       return view('users.listusers');
     }

     public function ajaxUsersList()
     {
//dd('ping');
       if (request()->ajax()) {
         //filter that only logged in user's linked people...
         // $user=Auth::user();
         // $userPerson=Person::find($user->person_id);

         $users = DB::table('users as u')->leftJoin('model_has_roles as ru', 'ru.model_id', 'u.id')->leftjoin('roles as r', 'r.id', 'ru.role_id')
                       ->select('u.id','u.name', 'email', 'r.name as role')
                       ->orderBy('u.name')->get();

         return Datatables::of($users)->make();
       }

        return [];
     }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function tempAddNewUser(Request $request)
    {
        //get personid (use to get name and email)
        //get password for the person to use....
        $person = Person::find($request->personid);
        if ($person) {
          //check if already a user....
          $usercheck = User::where('person_id',$request->personid)->first();
          if (!$usercheck) {
            $newuser = new User();
            $newuser->name = $person->firstname . " " . $person->surname;
            $newuser->email = $person->email;
            $newuser->person_id = $request->personid;
            $newuser->password = Hash::make($request->password);
            $newuser->save();

            if ($request->role == 'admin') {
              $newuser->assignRole('Admin');
              // $newuser->attachRole(1); //Admin user
            } else {
              // $newuser->attachRole(3); //Admin user
              $newuser->assignRole('ReadUser');
            }

          } else {
            //update user details.....including email????
            $newuser = $usercheck;
            $newuser->name = $person->firstname . " " . $person->surname;
            $newuser->email = $person->email;
            $newuser->password = Hash::make($request->password);
            $newuser->save();

            //what role previously???
            $prevrole = DB::table('model_has_roles')->where('model_id', $usercheck->id)->first();

            if ($request->role == 'admin' && $prevrole->role_id == 3) {
              // $newuser->roles()->detach(3);
              // $newuser->attachRole(1); //Admin user
              $newuser->assignRole('Admin');
              $newuser->removeRole('ReadUser');
            } else if ($request->role == 'readonly' && $prevrole->role_id == 1){
              // echo 'here1';
              // $newuser->attachRole(3); //read user
              $newuser->assignRole('ReadUser');
              // $newuser->roles()->detach(1);
              $newuser->removeRole('Admin');
            }
          }

          return Response::json('success', 200);

        }

        return Response::json('error', 422);


    }

    public function showAccount()
    {
      //show account info for current user...
      $user = Auth::user();
      $user->password = '';

      return view('users.editAccount',['user' => $user]);
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAccount(Request $request)
    {
// dd($request);
        $user=User::findOrFail($request->id);
        if ($request->password != '') {
          $rules=array(
              'password'=>'required|confirmed',
              'password_confirmation'=>'sometimes|required_with:password',
          );
        } else {
          $rules = array();
        }

        $rules['email']='required|email|unique:users,email,'.$user->id;
        //dd($this->rules);
        $v = Validator::make($request->all(), $rules);
        //dd('ping');
        if ($v->passes()){
            unset($request['password_confirmation']);
            if ($request['password'] != '' ) {
              $request['password']=bcrypt($request['password']);
            } else {
              $request['password']=$user->password;
            }
            $user->update($request->all());

            $request->session()->flash('flash_message', 'User was successfuly updated!');
        }
        // dd($v);
        return redirect()->route('user.account')->withErrors($v)->withInput();

    }





}
