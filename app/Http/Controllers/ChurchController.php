<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Yajra\Datatables\Datatables;
use Response;
use Input;

use App\Church;

class ChurchController extends Controller
{
    public function listChurches()
    {
      $churches = Church::get();
      foreach($churches as $kerk) {
        $kerk->churchpic = $this->picUrl($kerk->churchpic);
      }
      return view('churches.listchurches', ['churches' => $churches]);
    }

    private function picUrl($fname)
    {
      if ($fname) {
        $directory  = url('images/documents');

        // Storage::disk('public_docs')->getDriver()->getAdapter()->getPathPrefix();

        $storagePath =  $directory.'/'. $fname .'?' . rand(1,32000);
      } else {
        $storagePath =  "";//url('images') . '/unknown.gif';
        // Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix('images/unknown.gif');
      }
      return $storagePath;
    }

    private function savePic($fname, $imagebase64 )
    {
      $data = $imagebase64;

      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);
      $data = base64_decode($data);

      if (strpos($fname, '.jp') === FALSE) {
        $xname = explode('.', $fname);
        $fname = $xname[0] . '.jpeg';
      }

      file_put_contents(base_path() . '/public/images/documents/'. $fname, $data);

      return $fname;

    }

    public function ajaxChurchesList()
    {
      if (request()->ajax()) {
        //filter that only logged in user's linked people...
        // $user=Auth::user();
        // $userPerson=Person::find($user->person_id);

        $church = Church::get();

        return Datatables::of($church)->make();
      }
       return view('churches.listchurches');
    }

    public function showChurch($id)
    {
      if ($id > 0) {
        $church = Church::find($id);
      } else {
        $church = new Church();
      }
      $photo_url = $this->picUrl($church->churchpic);

      return View::make('churches.edit_church', ['church' => $church,
                  'photo_url' => $photo_url]);

    }

    public function updateChurch(Request $request)
    {
      if (Input::get('cancel')) {
        return redirect()->action('ChurchController@listChurches');
      }
      $churchid = $request->id;
      if ($churchid > 0) {
        $church = Church::find($churchid);

      } else {
        $church = new Church();

      }

      $church->churchname = $request->churchname;
      $church->address = $request->address;
      $church->email = $request->email;
      $church->contacttel = $request->contacttel;
      $church->save();

      if ($request->headshot_file && $request->imagebase64) {

        $imageName = 'church_'.$church->id . '.' .
                $request->file('headshot_file')->getClientOriginalExtension();
        $church->churchpic = $this->savePic($imageName, $request->imagebase64);

      } else if ($church->churchpic && !$request->churchpic) {
        $church->churchpic = '';
      }

      $church->save();

      return redirect()->action('ChurchController@listChurches');
    }
}
