<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use Config;
use App\Libraries\MailChimp;

class MailChimpController extends Controller
{


    // public $mailchimp;



    public function __construct()
    {
        // $this->mailchimp = $mailchimp;
    }

    public function sendCompaign(Request $request)
   {

     $listid = '5cc158e7fe'; //test devotional
    //  $listid = '6fc0b9fc2b'; //live master list

       try {


         $options = [
         'list_id'   => $listid,
         'subject' => 'St Marks Daily Devotional',
         'from_name' => 'St Marks Devotional',
         'from_email' => 'devotional@stmarks.org.za',
         'to_name' => 'mkoorsse@gmail.com'
         ];

         $html = '<html><body><h2>Daily Devotional</h2><p>For God so loved the world that He sent His only begotten Son, so that we may not perish, but have eternal life.</p></body></html>';

         $content = [
         'html' => $html,
         'text' => strip_tags($html)
         ];
        //  $conditions = array();
        //   $conditions[] = array('condition_type'=>'Interests', 'field'=>'interests-9939431071', 'op'=>'one', 'value'=>'9939431071');
        //   $segment_opts = array('match'=>'all', 'conditions'=>$conditions);
          $subj = 'St Marks Daily Devotional';
          $segment_id = 167329;
          $monkey = new MailChimp($listid, $segment_id, $html, $subj);

          $monkey->sendCampaign();
        //  $segment_opts = array('saved_segment_id' => '167329'); //TEST DAILY CAMPAIGN
        
        //Testing Faith-fill living segment id =
         //Testing Daily devotional segment id = 167329

         //LIVE list must be set......
         //LIVE Faith-fill living segment id = 166493
        //  $segment_opts = array('saved_segment_id' => '166553'); //LIVE DAILY CAMPAIGN
         //LIVE Daily devotional segment id = 166553
         //LIVE The Voice segement id = 166497
          // $retval = $api->campaignCreate($type, $opts, $content, $segment_opts);

        //  $mailchimp = new \Mailchimp();
        //  $campaign = $mailchimp->campaigns->create('regular', $options, $content, $segment_opts);
        //  $mailchimp = new \Mailchimp();
        //*******THIS LINE BELOW TO SEND TO EVERYONE**********
        //  $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
        //  $this->mailchimp->campaigns->send($campaign['id']);

         return response()->json(array('success' => true));
        //  return redirect()->back()->with('success','send campaign successfully');


       } catch (Exception $e) {
        //  return redirect()->back()->with('error','Error from MailChimp');
        return response()->json(array('error' => true));
       }
   }
}
