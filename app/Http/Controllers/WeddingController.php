<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Yajra\Datatables\Datatables;
use Response;
use Input;

class WeddingController extends Controller
{
    public function index()
    {
      return view('weddings.listweddings');
    }

    public function ajaxWeddingList()
    {
      if (request()->ajax()) {
        //filter that only logged in user's linked people...
        // $user=Auth::user();
        // $userPerson=Person::find($user->person_id);
        $weddings = DB::table('weddings as w')
                    ->join('people as b', 'b.id', 'w.bride_id')
                    ->join('people as g', 'g.id', 'w.groom_id')
                    ->join('ministers as m', 'm.id', 'minister_id')
                    ->join('people as r', 'r.id', 'm.person_id')
                    ->join('churches as c', 'c.id', 'w.church_id')
                    ->select(DB::raw('w.id','concat(g.firstname," ",g.surname, " - ",b.firstname," ",b.surname) as couple'),
                        'r.surname as minister', DB::raw('if(w.church_id is null,venue,c.churchname) as venue', 'wedding_date', 'wedding_time'))
                    ->get();


        return Datatables::of($weddings)->make();
      }
       return view('weddings.listweddings');
    }
}
