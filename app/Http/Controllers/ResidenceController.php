<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResidenceController extends Controller
{
    //
    public function index($resid, $surname)
    {

      return view('residence.listresidence');
    }

    public function ajaxResidenceList()
    {

      if (request()->ajax()) {
        //filter that only logged in user's linked people...
        // $user=Auth::user();
        // $userPerson=Person::find($user->person_id);
        $resid = request()->resid;
        $surname = request()->surname;
        $reslist = null;
        return Datatables::of($people)->make();
      }
      return view('residence.listresidence');
       //return view('people.listpeople')->with(['counts' => $counts, 'months' => $months]);
    }
}
