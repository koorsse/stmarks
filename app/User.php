<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Person;
class User extends Authenticatable
{
    // use EntrustUserTrait;
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function people(){

        return $this->hasOne('App\Person');
    }
     public function person()
     {
            return $this->belongsTo(Person::class, 'user_id');
     }



    //  public function hasRole($rolename)
    //  {
    //      $findrole = Role::join('role_user as ru','ru.role_id', 'roles.id')
    //                    ->where('name', $rolename)
    //                   ->where('ru.user_id', $this->id)->get();

    //      if (count($findrole) > 0 ) {
    //        return true;
    //      }
    //      return false;
    //  }


}
