<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Person;

class Minister extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];

  public function person()
  {
     return $this->hasOne('App\Person');
  }
}
