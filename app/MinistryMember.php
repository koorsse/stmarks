<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Person;
use App\Ministry;

class MinistryMember extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];
  protected $table = 'ministrymembers';

  public function person()
  {
     return $this->belongsTo('App\Person', 'person_id');
  }

  public function ministry()
  {
    return $this->belongsTo('App\Ministry', 'ministry_id');
  }
}
