<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Minister;
use App\Person;

class Ministry extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];
  protected $table = 'ministries';

  public function people()
  {
     return $this->hasMany('App\Person', 'person_id');
  }

  public function minister()
  {
    return $this->belongsTo('App\Minister', 'minister_id');
  }

  public function events()
  {
    
  }
}
