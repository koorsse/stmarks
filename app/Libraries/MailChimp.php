<?php
namespace App\Libraries;


class MailChimp
{
    private $list_id;
    private $segment_id;
    private $html;
    private $apiKey;
    private $campaign_id;
    private $dataCenter;
    private $subject;

    public function __construct($list_id, $segment_id, $html, $subject)
    {
      $this->list_id = $list_id;
      $this->segment_id = $segment_id;
      $this->html = $html;
      $this->apiKey =  env('MAILCHIMP_APIKEY');
      $this->dataCenter = substr($this->apiKey,strpos($this->apiKey,'-')+1);
      $this->subject = $subject;
    }

    public function sendCampaign()
    {
      $this->campaign_id = $this->createCampaign();
      if ($this->campaign_id) {
        //set the content...
        if ($this->setContent()) {
          $this->sendIt();
        }
      }
    }

    private function createCampaign()
    {
      // $apiKey = "b41c81a62a2da66404e0e1ef1ebd0200-us15";
      if (!$this->segment_id) {
        $data = array("recipients" => array("list_id" => $this->list_id), "type" => "regular", "settings" => array("subject_line" => $this->subject, "title" => $this->subject, "reply_to" => 'mail@stmarks.org.za', "from_name" => "St Marks Congregational Church"));
      } else {
        $data = array("recipients" => array("list_id" => $this->list_id, "segment_opts" => array("saved_segment_id" => $this->segment_id)), "type" => "regular", "settings" => array("subject_line" => $this->subject, "title" => $this->subject, "reply_to" => 'mail@stmarks.org.za', "from_name" => "St Marks Congregational Church"));
      }

      $data = json_encode($data);
      $curl = curl_init();
      curl_setopt_array($curl, array(
         //Sample url
         CURLOPT_URL => "https://". $this->dataCenter.".api.mailchimp.com/3.0/campaigns",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_CUSTOMREQUEST => "POST",
         CURLOPT_POSTFIELDS => $data,
         CURLOPT_HTTPHEADER => array(
            "authorization: apikey " . $this->apiKey
         ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
         $response = $err;
         return false;
      }
      $result = json_decode($response, true);
      
      //return $result;
      return $result['id'];
    }

    private function setContent()
    {
      $data = array("html" => $this->html, 'raw_text' => strip_tags($this->html));
      $data = json_encode($data);
      $curl = curl_init();
      curl_setopt_array($curl, array(
         //Sample url
         CURLOPT_URL => "https://". $this->dataCenter.".api.mailchimp.com/3.0/campaigns/".$this->campaign_id."/content",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_CUSTOMREQUEST => "PUT",
         CURLOPT_POSTFIELDS => $data,
         CURLOPT_HTTPHEADER => array(
            "authorization: apikey " . $this->apiKey
         ),
      ));


      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
         $response = $err;
         return false;
      }
      return true;

    }

    private function sendIt()
    {
      $curl = curl_init();
      curl_setopt_array($curl, array(
         //Sample url
         CURLOPT_URL => "https://". $this->dataCenter.".api.mailchimp.com/3.0/campaigns/".$this->campaign_id."/actions/send",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_CUSTOMREQUEST => "POST",

         CURLOPT_HTTPHEADER => array(
            "authorization: apikey " . $this->apiKey
         ),
      ));


      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
         $response = $err;
      }
    }


}
