<?php
namespace App\Libraries;

use App\Dailydevotional;
use App\Notifications\FacebookNotification;
use App\Mail\Devotionalmail;
use App\Mail\Notifymail;
use Auth;
use Config;
use DateTime;
use Log;
use Mail;
use App\User;
use App\Person;
use App\Libraries\MailChimp;


class DailydevotionalPublish
{

    static public $mailchimp;

    // static public function __construct(\Mailchimp $mailchimp)
    // {
    //     // $this->mailchimp = $mailchimp;
    // }
    static public function firstCheck()
    {
      //first check on Mondays....follow up with assigned person...

    }

    static public function weeklyReview()
    {
      //send out on Monday - breakdown of daily devotional status for next two weeks excl weekends
      $chkDate = date('Y-m-d');
      // $datetime = new DateTime($todaydate);
      // $datetime->modify('+1 day');
      // $chkDate = $datetime->format('Y-m-d');
      $status = 'Ready';
      $counter = 0;
      $body = "<br/><table class='table'><tr><th>Date</th><th>Current Status</th></tr>";

      $failsafe = 0;
      while ($counter < 10 && $failsafe < 50) {
        $failsafe++;
        $datetime = new DateTime($chkDate);
        $datetime->modify('+1 day');
        $chkDate = $datetime->format('Y-m-d');
        $wkday = date('w', strtotime($chkDate)); // day of week
        $dayweek = date_format(new DateTime($chkDate),"l");
        // echo $chkDate . " " . $wkday . " " . $counter;
        if ($wkday != 0 &&  $wkday != 6) {

          $dd = Dailydevotional::where('message_date', $chkDate)
                  ->first();
          if (!$dd) {
            $body .=  "<tr><td>" . $chkDate . " " . $dayweek ."</td><td style='color: red;'>NO Devotional</td></tr>";
          } else {

            $body .=  "<tr><td>" . $chkDate ." " . $dayweek . "</td>";
            if ($dd->status == "Ready") {
              $body .= "<td style='color: green;'>";
            } else if ($dd->status == "Review") {
              $body .= "<td style='color: #FFBB33;'>";
            } else if ($dd->status == "Draft") {
              $body .= "<td style='color: #FF8633;'>";
            } else {
              $body .= "<td>";
            }

            $body .=  $dd->status ."</td></tr>";
          }
          if ($counter == 3 || $counter == 8) {
            $body .= "<tr ><td style='border-top: 1px solid #eee;'></td><td style='border-top: 1px solid #eee;'></td></tr>";
          }
          $counter++;


          // if ($counter <= 3) {
          //
          //   $status = "Ready";
          // } else if ($counter <= 6) {
          //   // dd('here');
          //   $status = "Review";
          // } else {
          //   $status = "Draft";
          // }
        }

        // echo $counter . " " . $chkDate;

      }

      $reviewer_id = 5; //5 Sandy Woolard (would get this from database eventually)
      $manager_id = 7;  //7 email manager that a particular dd has been set for review...
      // $usermanager = User::find($manager_id);
      // if ($usermanager) {
      //   $pid = Person::find($usermanager->personid);
      //   if ($pid) {
      //     $manager_name = $pid->firstname;
      //   } else {
      //     $manager_name = "";
      //   }
      //
      // } else {
      //   $manager_name = "";
      // }

      $body = "<p>The daily devotional statuses for the next two weeks are as follows: </p>" . $body;
      $emails=[User::find($reviewer_id)->email,User::find($manager_id)->email,'mkoorsse@gmail.com'];//, 'charl.vandermerwe@gmail.com']; //
      Mail::to($emails)->send(new Notifymail("", $body, 'Daily devotional: Upcoming 2 weeks review'));
    }

    static public function alertDevotionals()
    {
      //daily check...
      //next three days must be in ready....

      //todays date....

      $chkDate = date('Y-m-d');
      // $datetime = new DateTime($todaydate);
      // $datetime->modify('+1 day');
      // $chkDate = $datetime->format('Y-m-d');
      $status = 'Ready';
      $counter = 0;
      $body = "<table><tr><th>Date</th><th>Current Status</th></tr>";
      $found = false;


      $failsafe = 0;
      while ($counter < 3 && $failsafe < 50) {
        $failsafe++;

        $datetime = new DateTime($chkDate);
        $datetime->modify('+1 day');
        $chkDate = $datetime->format('Y-m-d');
        $wkday = date('w', strtotime($chkDate)); // day of week

        // echo $chkDate . " " . $wkday . " " . $counter;
        if ($wkday != 0 && $wkday != 6) {
          $dd = Dailydevotional::where('message_date', $chkDate)
                  ->first();
          if (!$dd) {
            $found = true;
            $body .=  "<tr><td>" . $chkDate .  "</td><td style='color: red;'><b>NO Devotional</b></td></tr>";
          } else if ($dd->status != $status) {
            $found = true;
            $body .=  "<tr><td>" . $chkDate .   "</td><td><b>". $dd->status ."</b></td></tr>";
          }
          $counter++;


          // if ($counter <= 3) {
          //
          //   $status = "Ready";
          // } else if ($counter <= 6) {
          //   // dd('here');
          //   $status = "Review";
          // } else {
          //   $status = "Draft";
          // }
        }


      }
      // dd($body);
      if ($found) {
        $reviewer_id = 5; //5 Sandy Woolard (would get this from database eventually)
        $manager_id = 7;  //7 email manager that a particular dd has been set for review...
        // $usermanager = User::find($manager_id);
        // if ($usermanager) {
        //   $pid = Person::find($usermanager->personid);
        //   if ($pid) {
        //     $manager_name = $pid->firstname;
        //   } else {
        //     $manager_name = "";
        //   }
        //
        // } else {
        //   $manager_name = "";
        // }

        $body = "<p>The following is a list of devotionals scheduled for the next 3 days that need <b>urgent attention</b>. The devotionals should be in the READY TO PUBLISH state. </p>" . $body;
        $emails=[User::find($reviewer_id)->email,User::find($manager_id)->email,'mkoorsse@gmail.com'];//, 'charl.vandermerwe@gmail.com']; //
        Mail::to($emails)->send(new Notifymail("", $body, 'Daily devotional: Status alert'));
      }
    }

    static public function sendCompaign()
    {
      $todaydate = date('Y-m-d');
      $stat = "Ready";
      $dd = Dailydevotional::where('message_date', $todaydate)
              ->where('status',$stat)->first();
      // $dd = Dailydevotional::where('message_date', $todaydate)->first();
      if ($dd) {
         //$listid = '5cc158e7fe'; //test devotional
          $listid = '6fc0b9fc2b'; //live master list

           try {
             $dayweek = date_format(new DateTime($dd->message_date),"l");
             $displaydate = date_format(new DateTime($dd->message_date),"d F Y");
             $subj = $dayweek. ", " . $displaydate;
            //  $options = [
            //  'list_id'   => $listid,
            //  'subject' => $subj,
            //  'from_name' => 'St Marks Congregational Church',
            //  'from_email' => 'mail@stmarks.org.za'
            //  ];

            // $html = '<html><body><h2>Daily Devotional</h2><p>For God so loved the world that He sent His only begotten Son, so that we may not perish, but have eternal life.</p></body></html>';
             $html = self::emailTemplate();
             $content = [
             'html' => $html,
             'text' => strip_tags($html)
             ];

             //  $conditions = array();
             //   $conditions[] = array('condition_type'=>'Interests', 'field'=>'interests-9939431071', 'op'=>'one', 'value'=>'9939431071');
             //   $segment_opts = array('match'=>'all', 'conditions'=>$conditions);

            //  $segment_opts = array('saved_segment_id' => '167329'); //TEST DAILY CAMPAIGN
             //Testing Faith-fill living segment id =
             //Testing Daily devotional segment id = 167329

             //LIVE list must be set......
             //LIVE Faith-fill living segment id = 166493
             //$segment_opts = array('saved_segment_id' => '166553'); //LIVE DAILY CAMPAIGN
             //LIVE Daily devotional segment id = 166553
             //LIVE The Voice segement id = 166497

             // $retval = $api->campaignCreate($type, $opts, $content, $segment_opts);
             $segment_id = 166553;
             $monkey = new MailChimp($listid, $segment_id, $html, $subj);

             $monkey->sendCampaign();
             //$mailchimp = new \Mailchimp();
             //$campaign = $mailchimp->campaigns->create('regular', $options, $content, $segment_opts);
             //  $mailchimp = new \Mailchimp();
             //*******THIS LINE BELOW TO SEND TO EVERYONE**********
             //  $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
            // $mailchimp->campaigns->send($campaign['id']);
            //  Log::info('Mailchimp caimpaign sent!');
            //  return response()->json(array('success' => true));
            //  return redirect()->back()->with('success','send campaign successfully');


           } catch (Exception $e) {
            //  return redirect()->back()->with('error','Error from MailChimp');
            // return response()->json(array('error' => true));
           }
        } else {
          //email could not send campaign
        }

   }


    static function postToFB()
    {
      //when this is called.... find the dailydevotional with today's date....
      //must not have been published already...

      $dd = new Dailydevotional();
        // $dd->notify(new FacebookNotification());
      $todaydate = date('Y-m-d');
      $stat = "Ready";
      $dd = Dailydevotional::where('message_date', $todaydate)
              ->where('status',$stat)->first();
      if ($dd) {
        $dd->notify(new FacebookNotification());
        Log::info('Daily devotional Posted to Facebook');
      } else {
        //email no FB
        Log::info('Not posted - no dd to publish');
      }


      // return response()->json(array('success' => true));

    }

    static function setToPublished()
    {
      //get today's devotional....
      $todaydate = date('Y-m-d');
      $stat = "Ready";
      $daily = Dailydevotional::where('message_date', $todaydate)
              ->where('status',$stat)->first();
      if ($daily) {
        $daily->has_published = 1;
        $daily->status = "Published";
        $daily->publish_date = $todaydate;
        $daily->save();
      }

    }


    static private function emailTemplate()
    {
      $subject="St Mark's Daily devotional";

      $todaydate = date('Y-m-d');
      $stat = "Ready";
      $daily = Dailydevotional::where('message_date', $todaydate)
             ->where('status',$stat)->first();

      if (!$daily) {
        //get last published....
        // $daily = Dailydevotional::where('message_date', '<', $todaydate)->orderBy('message_date', 'desc')->first();
        if (!$daily) {
          //email
          dd('No daily devotional today');
        }
      }

      $dayweek = date_format(new DateTime($daily->message_date),"l");
      $displaydate = date_format(new DateTime($daily->message_date),"d F Y");
      $verseformat = str_replace(":", ".", $daily->verseref);

      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns:v="urn:schemas-microsoft-com:vml">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
          <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
          <!--[if !mso]><!-- -->
          <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel="stylesheet">
          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
          <!-- <![endif]-->

          <title>St Marks Daily Devotional</title>

          <style type="text/css">
              body {
                  width: 100%;
                  background-color: #ffffff;
                  margin: 0;
                  padding: 0;
                  -webkit-font-smoothing: antialiased;
                  mso-margin-top-alt: 0px;
                  mso-margin-bottom-alt: 0px;
                  mso-padding-alt: 0px 0px 0px 0px;
              }

              p,
              h1,
              h2,
              h3,
              h4 {
                  margin-top: 0;
                  margin-bottom: 0;
                  padding-top: 0;
                  padding-bottom: 0;
              }

              span.preheader {
                  display: none;
                  font-size: 1px;
              }

              html {
                  width: 100%;
              }

              table {
                  font-size: 14px;
                  border: 0;
              }
              /* ----------- responsivity ----------- */

              @media only screen and (max-width: 640px) {
                  /*------ top header ------ */
                  .main-header {
                      font-size: 20px !important;
                      line-height: 40px !important;
                  }

                  .main-header-title {
                    font-size: 36px !important;
                  }

                  .main-header-other {
                    font-size: 16px !important;
                  }

                  .main-section-header {
                      font-size: 28px !important;
                  }

                  .imgicon {
                    width: 50px !important;
                  }
                  .show {
                      display: block !important;
                  }
                  .hide {
                      display: none !important;
                  }
                  .align-center {
                      text-align: center !important;
                  }
                  .no-bg {
                      background: none !important;
                  }
                  /*----- main image -------*/
                  .main-image img {
                      width: 440px !important;
                      height: auto !important;
                  }
                  /* ====== divider ====== */
                  .divider img {
                      width: 440px !important;
                  }
                  /*-------- container --------*/
                  .container590 {
                      width: 440px !important;
                  }
                  .container580 {
                      width: 400px !important;
                  }
                  .main-button {
                      width: 220px !important;
                  }
                  /*-------- secions ----------*/
                  .section-img img {
                      width: 320px !important;
                      height: auto !important;
                  }
                  .team-img img {
                      width: 100% !important;
                      height: auto !important;
                  }
              }

              @media only screen and (max-width: 479px) {
                  /*------ top header ------ */
                  .main-header {
                      font-size: 22px !important;
                      line-height: 35px !important;
                  }

                  .main-header-title {
                    font-size: 30px !important;
                  }

                  .main-header-other {
                    font-size: 12px !important;
                  }

                  .main-section-header {
                      font-size: 26px !important;
                  }

                  .imgicon {
                    width: 50px !important;
                  }


                  /* ====== divider ====== */
                  .divider img {
                      width: 280px !important;
                  }
                  /*-------- container --------*/
                  .container590 {
                      width: 280px !important;
                  }
                  .container590 {
                      width: 280px !important;
                  }
                  .container580 {
                      width: 260px !important;
                  }
                  /*-------- secions ----------*/
                  .section-img img {
                      width: 280px !important;
                      height: auto !important;
                  }
              }




          </style>
          <!-- [if gte mso 9]><style type=”text/css”>
              body {
              font-family: arial, sans-serif!important;
              }
              </style>
          <![endif]-->
      </head>


      <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
          <!-- pre-header -->
          <table style="display:none!important;">
              <tr style="text-align: center;">
                  <td>
                      <div style="overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;">
                          <p>Daily devotional</p>
                          <p></p>
                      </div>
                  </td>
              </tr>
          </table>
          <!-- pre-header end -->
          <!-- header -->


          <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" >

              <tr bgcolor="#256F85">
                  <td align="center">
                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                          <tr >
                              <td style="text-align:center;">

                                  <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                                      <tr height="70" style="text-align:center;">

                                          <td align="center" bgcolor="#256F85" style="padding: 10px 0px;font-family: Source Sans Pro, sans-serif;
                                          font-size: 45px;
                                          color: #fff;
                                          font-weight: 300;
                                          text-align:center;
                                          vertical-align:center;
                                          line-height: 70px;
                                          letter-spacing: 3px;" class="main-header">
                                              <!-- <table width="70" style="background-color: pink;margin: 0px; cellpadding: 0; cellspacing: 0;"> -->
                                            <table border="0" width="70" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                                <tr style="margin: 0px;">
                                                  <td style="margin: 0px;">
                                                    <img class="imgicon" style="vertical-align:sub;" width="70" border="0" style="width: 70px;" src="https://database.stmarks.org.za/public/images/sm-logo-vertical.png" alt="" />
                                                  </td>
                                                </tr>
                                            </table>
                                            <!-- <table border="0"  align="left" cellpadding="0" cellspacing="0" style="font-size: 45px;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                                <tr style="margin: 0px;text-align:center;">
                                                  <td style="margin: 0px;text-align:center;">
                                                    <span ><i>Daily Devotional</i></span>
                                                  </td>
                                                </tr>
                                            </table> -->
      <span ><i>Daily Devotional</i></span>
                                              <!-- <div style="line-height: 35px" > -->

                                              <!-- </div> -->
                                          </td>
        <!-- line-height: 35px; padding: 10px 0px;"  -->
                                          <!-- <td align="left" bgcolor="#256F85" style="font-family: Source Sans Pro, sans-serif;
                                          font-size: 45px;
                                          color: #fff;
                                          font-weight: 300;
                                          letter-spacing: 3px; line-height: 35px; padding: 20px 0px;" class="main-header">
                                              <div style="line-height: 35px" >
                                                <i>Daily Devotional</i>
                                              </div>
                                          </td> -->
                                      </tr>
                                  </table>
                              </td>
                          </tr>

                        <!--  <tr>
                              <td class="hide" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr> -->

                      </table>
                  </td>
              </tr>
          </table>

          <!-- end header -->

          <!-- big image section -->
          <!-- <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#256F85">

              <tr>
                  <td align="center">
                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">


                          <tr>
                              <td align="center">

                                  <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                                    <tr style="text-align: center;">

                                        <td align="center" bgcolor="#256F85" style="font-family: Source Sans Pro, sans-serif;
                                        font-size: 40px;
                                        color: #fff;
                                        font-weight: 300;
                                        letter-spacing: 3px; line-height: 35px; padding: 20px 0px;" class="main-header">
                                            <div style="line-height: 35px" >
                                              <i>Daily Devotional</i>
                                            </div>
                                        </td>
                                    </tr>
                                  </table>
                              </td>
                          </tr>



                      </table>
                  </td>
              </tr>
          </table> -->
          <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffffff" class="bg_color">

              <tr>
                  <td align="center">
                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" >

                          <tr>
                              <td height="20"  style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>
                          <tr>
                              <td align="center" style="color: rgb(88,89,91); font-size: 18px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;" class="main-header-other">


                                  <div style="line-height: 35px">'.
                                     $dayweek . ', ' . $displaydate .
                                  '</div>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 15px;">&nbsp;</td>
                          </tr>
                          <tr style="text-align:center;">
                              <td align="center" style="font-family: Source Sans Pro, sans-serif; font-size: 51px;
                              color: rgb(0, 69, 99);
                              font-weight: 600;
                              letter-spacing: 3px;" class="main-header-title">
                                  <div >
                                    <i>'. $daily->title .'</i>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 20px;">&nbsp;</td>
                          </tr>
                          <tr style="text-align:center;">
                              <td align="center" style="font-weight: 600; color: rgb(0, 69, 99); font-family: Source Sans Pro, sans-serif; font-size: 16px; letter-spacing: 3px; line-height: 35px;" class="main-header-other">


                                  <div style="line-height: 35px">
                                    by <b>'. $daily->author .'</b>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>

                          <tr>
                              <td align="center">
                                  <table border="0" width="590" align="center" cellpadding="0" cellspacing="0" bgcolor="999999" class="container590">
                                      <tr>
                                          <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>
                          <tr>
                              <td align="center">
                                  <table border="0" width="580" align="center" cellpadding="0" cellspacing="0" class="container590">
                                      <tr>
                                          <td align="justify" style="color: #555555; font-size: 16px; font-family: Source Sans Pro, sans-serif; line-height: 24px;">


                                              <div style="line-height: 24px">

                                                <p style="text-align: justify; font-size: 18px; color: rgb(0, 69, 99);"><i>“'.$daily->versetext.'”</i></p>
                                                <p style="text-align: right; font-size: 18px; color: rgb(0, 69, 99);">
                                                  <b>'. $daily->verseref .'<b>
                                                </p>
                                              </div>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>
                          <tr>
                              <td align="center">
                                  <table border="0" width="590" align="center" cellpadding="0" cellspacing="0" bgcolor="999999" class="container590">
                                      <tr>
                                          <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>

                          <tr>
                              <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                          </tr>

                          <tr>
                              <td align="center">
                                  <table border="0" width="590" align="center" cellpadding="0" cellspacing="0" class="container590">
                                      <tr>
                                          <td align="justify" style="color: #555555; font-size: 16px; font-family: Source Sans Pro, sans-serif; line-height: 24px;">


                                              <div style="line-height: 24px">'.

                                                 str_replace( "\n", '<br />', $daily->messagebody) .
                                              '</div>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>

                          <tr>
                              <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                          </tr>

                      </table>

                  </td>
              </tr>

          </table>
          <!-- end section -->
          <!-- prayer section -->
          <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffffff" class="bg_color">

              <tr>
                  <td align="center">
                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" style="border: 1px solid black; color: rgb(88,89,91);">

                          <tr>
                              <td height="20"  style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                          </tr>


                          <tr style="text-align:center;">
                              <td align="center" class="main-header-title">
                                <p style="text-align: center; font-weight: 200; font-size: 51px; font-family: Source Sans Pro, sans-serif; font-style: italic; ">Prayer</p>
                              </td>
                          </tr>
                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 20px;">&nbsp;</td>
                          </tr>


                          <!-- <div class="col-md-12" style="border: 2px #eee solid;min-height: 250px; font-family: Source Sans Pro; color: rgb(88,89,91);">
                            <h2 style="text-align: center; padding-bottom: 10px; font-weight: 200; font-size: 51px; font-family: Source Sans Pro; font-style: italic; ">Prayer</h2>
                            <p style="text-align: center; padding-bottom: 10px; font-size: 18px; font-weight: 600;"><b><i>'. $daily->prayer .'</i></b></p>
                            <p style="text-align: center; font-weight: 800; font-size: 26px;"><b><i>Amen</i></b></p>
                          </div> -->

                          <tr>
                              <td align="center">
                                  <table border="0" width="590" align="center" cellpadding="10" cellspacing="0" class="container590">
                                      <tr>
                                          <td align="justify" style="color: #555555; font-size: 16px; font-family: Source Sans Pro, sans-serif; line-height: 24px;">


                                              <div style="line-height: 24px">

                                                <p style="text-align: center; font-size: 18px; font-weight: 600;"><b><i>'.$daily->prayer.'</b></i></p>

                                              </div>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>

                          <tr>
                              <td height="10" style="font-size: 10px; line-height: 20px;">&nbsp;</td>
                          </tr>

                          <tr style="text-align:center;">
                              <td align="center" style="font-family: Source Sans Pro, sans-serif; text-align: center; font-weight: 800; font-size: 26px;">
                                <b><i>Amen</i></b>

                              </td>
                          </tr>
                          <tr>
                              <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                          </tr>

                      </table>

                  </td>
              </tr>
              <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
              </tr>

          </table>

          <!-- end prayer section -->

          <!-- contact section -->
          <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

              <tr >
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
              </tr>
              <!-- <tr>
                  <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
              </tr> -->

              <tr>
                  <td height="20" style="border-top: 1px solid #e0e0e0;font-size: 20px; line-height: 20px;">&nbsp;</td>
              </tr>

              <tr>
                  <td align="center">
                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590 bg_color">

                          <tr>
                              <td>
                                <!--first column -->
                                  <table border="0" width="100" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                      <tr>
                                          <!-- logo -->
                                          <td align="center">
                                              <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="80" border="0" style="display: block; width: 80px;" src="https://database.stmarks.org.za/public/images/sm-logo-2.png" alt="" /></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td  height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                      </tr>

                                      <tr>
                                          <td align="center">
                                              <table border="0" align="" cellpadding="0" cellspacing="0">
                                                  <tr>
                                                      <td>
                                                          <a href="https://twitter.com/StMarksPE" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/Qc3zTxn.png" alt=""></a>
                                                      </td>
                                                      <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                      <td>
                                                          <a href="https://www.facebook.com/StMarksPE" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/RBRORq1.png" alt=""></a>
                                                      </td>

                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>


                                      <!-- <tr>
                                          <td align="left" style="color: #888888; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; line-height: 23px;" class="text_color">
                                              <div style="color: #333333; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                  Email us: <br/> <a href="mailto:" style="color: #888888; font-size: 14px; font-family: Hind Siliguri, Calibri, Sans-serif; font-weight: 400;">info@stmarks.org.za</a>

                                              </div>
                                          </td>
                                      </tr> -->

                                  </table>
                                  <!-- end first column -->
                                  <!-- empty second column -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                      <tr>
                                          <td width="2" height="10" style="font-size: 10px; line-height: 10px;"></td>
                                      </tr>
                                      <tr>
                                          <td align="center" style="color: #888888; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; line-height: 23px;" class="text_color">
                                              <div style="color: #333333; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                  Email us: <a href="mailto:" style="color: #888888; font-size: 14px; font-family: Hind Siliguri, Calibri, Sans-serif; font-weight: 400;">info@stmarks.org.za</a>

                                              </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="center" style="color: #888888; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; line-height: 23px;" class="text_color">
                                              <div style="color: #333333; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                  Website: <a href="http://www.stmarks.org.za" style="color: #888888; font-size: 14px; font-family: Hind Siliguri, Calibri, Sans-serif; font-weight: 400;">www.stmarks.org.za</a>

                                              </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="center" style="color: #888888; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; line-height: 23px;" class="text_color">
                                              <div style="color: #333333; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                  Tel: <span  style="color: #888888; font-size: 14px; font-family: Hind Siliguri, Calibri, Sans-serif; font-weight: 400;">+27 41 360 6060</span><br/>
                                                  Fax: <span style="color: #888888; font-size: 14px; font-family: Hind Siliguri, Calibri, Sans-serif; font-weight: 400;">+27 41 360 9217</span>


                                              </div>
                                          </td>
                                      </tr>
                                  </table>
                                  <!-- end second column -->
                                  <!-- third column with twitter facebook -->
                                  <!-- <table border="0" width="200" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                      <tr>
                                          <td class="hide" height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                                      </tr>



                                      <tr>
                                          <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                      </tr>

                                      <tr>
                                          <td>
                                              <table border="0" align="right" cellpadding="0" cellspacing="0">
                                                  <tr>
                                                      <td>
                                                          <a href="https://www.facebook.com/StMarksPE" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/Qc3zTxn.png" alt=""></a>
                                                      </td>
                                                      <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                      <td>
                                                          <a href="https://twitter.com/StMarksPE" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="http://i.imgur.com/RBRORq1.png" alt=""></a>
                                                      </td>

                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>

                                  </table> -->
                                  <!-- end third column -->
                              </td>

                          </tr>
                      </table>
                  </td>
              </tr>

              <tr>
                  <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
              </tr>

          </table>


          <!-- end section -->
          <!-- footer section -->

          <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

              <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
              </tr>

              <tr>
                  <td align="center">

                      <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                          <tr>


                                <td align="center" style="color: #aaaaaa; font-size: 11px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">
                                    <div style="line-height: 24px;">
                                        <span style="color: #333333;">You are subscribed to the St Mark\'s Daily Devotional Mailing list</span>
                                    </div>

                                <td>
                          </tr>
                          <tr>
                            <td align="center" style="color: #aaaaaa; font-size: 11px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">
                                <div style="line-height: 24px;">
                                    <span style="color: #333333;">Our mailing address is:</span>
                                    <p>*|HTML:LIST_ADDRESS_HTML|*<br /></p>
                                </div>

                            <td>
                          </tr>
                          <tr>
                            <td align="center" style="color: #aaaaaa; font-size: 11px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">
                                <div style="line-height: 24px;">
                                    <a style="font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;color: #5caad2; text-decoration: none;font-weight:bold;" href="*|UNSUB|*"><b>UNSUBSCRIBE</b></a> from this list.
                                </div>


                            <td>
                          </tr>
                          <tr>
                            <td align="center" style="color: #aaaaaa; font-size: 11px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;">
                                <div style="line-height: 24px;">
                                    <a style="font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 24px;color: #5caad2; text-decoration: none;font-weight:bold;" href="*|UPDATE_PROFILE|*">Update your profile</a>
                                </div>


                            <td>
                          </tr>

                      </table>
                  </td>
              </tr>

              <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
              </tr>

          </table>

          <!-- end footer --->


      </body>

      </html>

    ';

    return $html;
  }



}



 ?>
 <!-- footer ====== -->
 <!-- <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

     <tr>
         <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
     </tr>

     <tr>
         <td align="center">

             <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                 <tr>
                     <td>
                         <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                             <tr>
                                 <td align="left" style="color: #aaaaaa; font-size: 14px; font-family: Work Sans, Calibri, sans-serif; line-height: 24px;">
                                     <div style="line-height: 24px;">

                                         <span style="color: #333333;">To subscribe: </span>

                                     </div>
                                 </td>
                             </tr>
                         </table>

                         <table border="0" align="left" width="5" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                             <tr>
                                 <td height="20" width="5" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                             </tr>
                         </table>


                     </td>
                 </tr>

             </table>
         </td>
     </tr>

     <tr>
         <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
     </tr>

 </table> -->
 <!-- end footer ====== -->
