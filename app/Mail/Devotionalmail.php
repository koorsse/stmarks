<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Dailydevotional;
use DateTime;

class Devotionalmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $dd_date;
    public $rede;
    public $wie;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($thedate, $reason, $who)
    {
      $this->dd_date = $thedate;
      $this->rede = $reason;
      $this->wie = $who;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //sending this to review....
        $subject="REVIEW: St Mark's Daily devotional";

        // $todaydate = date('Y-m-d');
        $stat = "Ready";
        $daily = Dailydevotional::where('message_date', $this->dd_date)->first();
                //->where('status',$stat)->first();

        if (!$daily) {
          //get last published....
          // $daily = Dailydevotional::where('message_date', '<', $todaydate)->orderBy('message_date', 'desc')->first();
          // if (!$daily) {
            //send email!!
            dd('No daily devotional today');
          // }
        } else {

          $dayweek = date_format(new DateTime($daily->message_date),"l");
          $displaydate = date_format(new DateTime($daily->message_date),"d F Y");
          $verseformat = str_replace(":", ".", $daily->verseref);
          $encryptid = base64_encode($daily->id);
          $reason = $this->rede;
          $who = $this->wie;
          // dd($daily->messagebody);
          return $this->view('emails.daily', compact('daily', 'displaydate','dayweek','reason','who','encryptid'))->subject($subject);
        }


    }
}
