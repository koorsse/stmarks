<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use DateTime;

class Notifymail extends Mailable
{
    use Queueable, SerializesModels;
    public $username;
    public $messagebody;
    public $subj;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usern, $message, $subject = null)
    {
      $this->username = $usern;
      $this->messagebody = $message;
      $this->subj = $subject;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //sending this to review....
        if (!$this->subj) {
          $subject = "St Mark's System";
        } else {
          $subject = $this->subj;
        }

          $uname = $this->username;
          $messagebody = $this->messagebody;

          return $this->view('emails.notification', compact('uname', 'messagebody'))->subject($subject);



    }
}
