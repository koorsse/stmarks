<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromCollection;


use DateTime;
use DB;

class BdayExport implements  FromCollection
{
  use Exportable;

  public function __construct(int $month)
  {
    $this->month = $month;
  }

  public function collection()
  {
    //$bday_list = DB::select("select title from people");
    $bday_list = DB::select("select date_format(str_to_Date(birthday,'%d/%m/%Y'),'%d') as bday,
                  title, if(preferred_name = '',firstname,preferred_name) as `name`, surname,
                  if(year(str_to_Date(birthday,'%d/%m/%Y')) != 1900,year(current_timestamp) - year(str_to_Date(birthday,'%d/%m/%Y')),'') as age,
                  telephone, cellno, worktel
                  from people p inner join residences r on p.res_id = r.id
                  where deceased_date is null and birthday != '01/01/1901' and month(str_to_Date(birthday,'%d/%m/%Y')) = ?
                  and status_id = 1
                  order by date_format(str_to_Date(birthday,'%d/%m/%Y'),'%d');",[$this->month]);

                          // where birthday != '01/01/1901' and month(str_to_Date(birthday,'%d/%m/%Y')) = :month
                  // ->setBindings(['month' => $month])
                  // ->get();

    return collect($bday_list);

  }

  // /**
  //   * @return string
  //   */
  //   public function title(): string
  //   {
  //     $dateObj   = DateTime::createFromFormat('!m', $this->month);
  //     $monthName = $dateObj->format('F');
  //       return 'Birthdays_'.$monthName;
  //   }
}
