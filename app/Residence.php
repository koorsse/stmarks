<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Residence extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];

  public function residents()
  {
     return $this->hasMany('App\Person', 'res_id');
  }


}
