
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Person;

class Cradleroll extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];
    //

  public function child()
  {
    return $this->hasOne('App\Person');
  }
}
