<?php

namespace App\Notifications;

use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;
use Illuminate\Notifications\Notification;
use App\Dailydevotional;
use DateTime;

class FacebookNotification extends Notification
{
  //         $fbpost = 'Reminder to visit our website and to subscribe to receive your Faith-Fill Living email updates and reminders.
  // If you already receive the Voice or Daily Devotion you will receive an "error" message when trying to subscribe. Please select "Click here to update my profile" to add yourself to the Faith-Fill Living mailing list too.
  // "If something needs to be done, do something!"
  // www.stmarks.org.za';

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FacebookPosterChannel::class];
    }

    public function toFacebookPoster($notifiable) {
        //$dd = Dailydevotional::find(1);
        $fbpost = $this->buildFBPost();
      //  $fbpost = "GOD WITH US - In the valley, in the wilderness, in the storm. God's presence in our lives is not dependent on our emotions, situation or understanding. GOD is ALWAYS with us.";
        // dd($fbpost);
        if ($fbpost) {
          return new FacebookPosterPost($fbpost);
        } else {
          //
        }


        // return (new FacebookPosterPost('Laravel notifications are awesome!'))->withLink('https://laravel.com');
    }

    public function buildFBPost()
    {
      //find today's devotional....

      $todaydate = date('Y-m-d');
      $stat = "Ready";
      $dd = Dailydevotional::where('message_date', $todaydate)
            ->where('status',$stat)->first();

      if ($dd) {
          $msg = 'Daily Devotional - ' . date_format(new DateTime($dd->message_date),"d F Y") . "\r\n\n";
          $msg .= strtoupper($dd->title) . "\r\n\n";
          $msg .= '"' . $dd->versetext . '" - ' . $dd->verseref . "\n\n";
          $msg .= $this->stripTags($dd->messagebody) . "\r\n\n";
          $msg .= 'PRAYER' . "\r\n\n";
          $msg .= $dd->prayer . ' AMEN' . "\r\n\n";
          $msg .= 'By ' . $dd->author . "\r\n\n";
          $msg .= "To receive our Daily Devotional please click the link below and fill in your details. Remember to select the mailing list you'd like to subscribe to. database.stmarks.org.za/subscribe" . "\r\n";
          // $msg .= 'To receive your Daily Devotional, please subscribe to our mailing list at database.stmarks.org.za/subscribe' . "\r\n";
          $msg .= '#StMarksPE #DailyDevotional';
        //  dd($msg);
          return $msg;
      }
      return false;

    }

    private function stripTags($text)
    {
      $test = strip_tags(html_entity_decode($text));
      return $test;
    }
}

?>
