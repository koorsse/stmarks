<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\DailydevotionalPublish;

class DailyDevotional_alert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devotional:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send alert if any of next 3 days is not in READY state';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
          // DailydevotionalPublish::sendCompaign();
          // DailydevotionalPublish::postToFB();
          // DailydevotionalPublish::setToPublished();
          // DailydevotionalPublish::weeklyReview();
          DailydevotionalPublish::alertDevotionals();
        //facebook post
        //mailchimp campaign
        //don't need to change the daily - just change to has_published = true and set the date....
    }
}
