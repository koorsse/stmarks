<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\DailydevotionalPublish;

class DailyDevotional extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devotional:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the daily devotional to different mediums';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* Following 3 uncommented on live server */
           //DailydevotionalPublish::sendCompaign();
          // DailydevotionalPublish::postToFB();
          // DailydevotionalPublish::setToPublished();


          // DailydevotionalPublish::weeklyReview();
          // DailydevotionalPublish::alertDevotionals();
        //facebook post
        //mailchimp campaign
        //don't need to change the daily - just change to has_published = true and set the date....
    }
}
