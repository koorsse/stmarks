<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\DailyDevotional',
        'App\Console\Commands\DailyDevotional_weekly',
        'App\Console\Commands\DailyDevotional_alert'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        /** PHILLIPIANS 4:13: "I can do all things through Christ, who strengthens me." **/
        $schedule->command('devotional:publish')->dailyAt('04:13');
        $schedule->command('devotional:alert')->dailyAt('07:05');
        $schedule->command('devotional:weekly')->weeklyOn(1, '7:05'); //every Monday at
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
