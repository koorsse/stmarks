<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Person;

class Wedding extends Model
{
  use SoftDeletes;
  protected $guarded = ['id'];

  public function bride()
  {
    return $this->hasOne('App\Person', 'bride_id');
  }

  public function groom()
  {
    return $this->hasOne('App\Person', 'groom_id');
  }

}
