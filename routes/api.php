<?php

use Illuminate\Http\Request;
use App\Dailydevotional;


//use Cache;
// use DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/devotional/setReady', function (Request $request) {
    //"mysafa.co.za/api/player/{id}"
    // return Person::where('regid',$id)->first();
    //id is hash....
    // $ddid = base64_encode($request->ddid);

    $decrypted_id = base64_decode($request->ddid);
    $dev = Dailydevotional::find($decrypted_id);
    // dd($dev);
    if ($dev)
      if ($dev->status == "Review") {

        $dev->status = "Ready";
        $dev->save();
        return "Daily devotional: Ready to Publish!! Have a great day!";
      } else {
        return "Daily devotional: Already set to Ready to Publish!! Have a great day!";
      }
    return "Daily devotional: could not be updated, please log in to the admin system to change to Ready to Publish";


});


?>
