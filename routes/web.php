<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('landing', function() {
  // return view('layouts.landing');
  //  return redirect('home/dashboard');
});
Route::get('/', function () {
  // return view('welcome');
    return redirect('/home');
    //return redirect('/login');
});

Route::get('/privacypolicy',  function() {
  return view('regulation.privacypolicy');
});

Route::get('/login', ['uses'=>'HomeController@index', 'as' => 'home']);

Auth::routes();
Route::get('devotional/show', ['uses' => 'DailydevotionalController@showdaily', 'as' => 'devotional.show']);
Route::get('devotional/email/{thedate?}', ['uses' => 'DailydevotionalController@emaildaily', 'as' => 'devotional.email']);
Route::get('subscribe', ['uses' => 'DailydevotionalController@subscribe', 'as' => 'devotional.subscribe']);
Route::get('registercc',['uses' => 'PeopleController@registerchildchurch', 'childrens.register']);
Route::group(['middleware' => ['auth']], function() {

	Route::get('/home', ['uses'=>'HomeController@index', 'as' => 'home']);
  Route::get('register/verify/{confirmationCode}', ['as' => 'confirmation_path','uses' => 'Auth\RegisterController@confirm']);
  Route::get('register/setlogin/{confirmationCode}', ['as' => 'setuplogin','uses' => 'Auth\RegisterController@showsetPasswordView']);
  Route::post('register/savelogin', ['as' => 'setlogin','uses' => 'Auth\RegisterController@set']);
  Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

	//routes for role creation
	// Route::get('roles',['as'=>'roles.index','uses'=>'RoleController@index','middleware' => ['role:SuperAdmin']]);
	// Route::get('roles/create',['as'=>'roles.create','uses'=>'RoleController@create','middleware' => ['role:SuperAdmin']]);
	// Route::post('roles/create',['as'=>'roles.store','uses'=>'RoleController@store','middleware' => ['role:SuperAdmin']]);
	// Route::get('roles/{id}',['as'=>'roles.show','uses'=>'RoleController@show']);
	// Route::get('roles/{id}/edit',['as'=>'roles.edit','uses'=>'RoleController@edit','middleware' => ['role:SuperAdmin']]);
	// Route::patch('roles/{id}',['as'=>'roles.update','uses'=>'RoleController@update','middleware' => ['role:SuperAdmin']]);
	// Route::delete('roles/{id}',['as'=>'roles.destroy','uses'=>'RoleController@destroy','middleware' => ['role:SuperAdmin']]);

  Route::post('devotional/mailchimp', ['uses' => 'MailChimpController@sendCompaign', 'as' => 'devotional.test','middleware' => ['role:SuperAdmin']]);
  Route::post('devotional/FB', ['uses' => 'DailydevotionalController@sendFB', 'as' => 'devotional.testFB','middleware' => ['role:SuperAdmin']]);

  Route::get('devotional/{thedate?}', ['uses' => 'DailydevotionalController@editDaily', 'as' => 'devotional.edit']);
  Route::post('devotional', ['uses' => 'DailydevotionalController@updateDaily', 'as' => 'devotional.save']);


  Route::post('testmail', ['uses' => 'HomeController@testmail', 'as' => 'mail.test', 'middleware' => ['role:SuperAdmin']]);

  Route::get('people', ['uses'=>'PeopleController@index', 'as' => 'people']);
  Route::get('people', ['uses'=>'PeopleController@ajaxPeopleList', 'as' => 'people.data']);
  Route::get('person/{id}', ['uses' => 'PeopleController@showPerson', 'as' => 'person.show']);
  Route::post('person/address', ['uses' => 'PeopleController@getAddressDetails', 'as' => 'person.get_address']);
  Route::post('people', ['uses' => 'PeopleController@updatePerson', 'as' => 'person.save']);
  Route::get('people/existing', ['uses' => 'PeopleController@getExisting', 'as' => 'person.existing']);
  Route::post('people/checkPerson', ['uses' => 'PeopleController@checkPersonExists', 'as' => 'person.exists']);
  Route::post('people/newuser', ['uses' => 'UserController@tempAddNewUser', 'as' => 'user.tempnew','middleware' => ['role:SuperAdmin']]);
  Route::get('people/downloadbirthdays/{month}', ['as' => 'people.downloadbdays', 'uses' => 'PeopleController@birthdaylist']);
  Route::get('childrens', ['uses'=>'PeopleController@indexChildren', 'as' => 'children']);
  Route::get('childrenlist', ['uses'=>'PeopleController@ajaxChildrenList', 'as' => 'children.data']);
  Route::get('child/{id}', ['uses' => 'PeopleController@showChildren', 'as' => 'children.show']);
  Route::post('childrens',['uses' => 'PeopleController@updateChild', 'as' => 'children.save']);
  Route::post('childrens/checkChild', ['uses' => 'PeopleController@checkChildExists', 'as' => 'child.exists']);

  Route::get('residence/{resid}/{surname}', ['uses' => 'ResidenceController@index', 'as' => 'residence']);
  Route::post('residence/{resid}/{surname}', ['uses' => 'ResidenceController@ajaxResidenceList', 'as' => 'residence.data']);

  Route::get('users',['as'=>'users','uses'=>'UserController@index','middleware' => ['role:SuperAdmin']]);
  Route::get('usersdata',['as'=>'users.data','uses'=>'UserController@ajaxUsersList','middleware' => ['role:SuperAdmin']]);
  Route::get('useraccount', ['as'=>'user.account','uses'=>'UserController@showAccount']);
  Route::post('useraccount', ['as'=>'user.save','uses'=>'UserController@updateAccount']);

  Route::get('church', ['uses'=>'ChurchController@listChurches', 'as' => 'churches']);
  // Route::get('church', ['uses'=>'ChurchController@ajaxChurchesList', 'as' => 'churches.data']);
  Route::get('church/{id}', ['uses' => 'ChurchController@showChurch', 'as' => 'church.show']);
  Route::post('church', ['uses' => 'ChurchController@updateChurch', 'as' => 'church.save']);

  Route::get('minister', ['uses'=>'MinisterController@listMinisters', 'as' => 'ministers']);
  Route::get('minister/{id}', ['uses' => 'MinisterController@showMinister', 'as' => 'minister.show']);
  Route::post('minister', ['uses' => 'MinisterController@updateMinister', 'as' => 'minister.save']);

  Route::get('ministry', ['uses'=>'MinistryController@listMinistries', 'as' => 'ministries']);
  Route::get('ministry/{id}', ['uses' => 'MinistryController@showMinistry', 'as' => 'ministry.show']);
  Route::get('ministrymembers/{id}', ['uses' => 'MinistryController@showMinistrymembers', 'as' => 'ministry.members']);
  Route::get('ministrynotices/{id}', ['uses' => 'MinistryController@listNotifications', 'as' => 'ministry.notices']);
  Route::post('ministry', ['uses' => 'MinistryController@updateMinistry', 'as' => 'ministry.save']);

  Route::get('wedding', ['uses'=>'WeddingController@index', 'as' => 'weddings']);
  Route::get('wedding', ['uses'=>'WeddingController@ajaxWeddingList', 'as' => 'wedding.data']);
  Route::get('wedding/{id}', ['uses' => 'WeddingController@showWedding', 'as' => 'wedding.show']);

  Route::get('funeral', ['uses'=>'FuneralController@index', 'as' => 'funerals']);
  Route::get('funeral/{id}', ['uses' => 'FuneralController@showFuneral', 'as' => 'funeral.show']);

  Route::get('cradleroll', ['uses'=>'CradlerollController@index', 'as' => 'cradleroll']);
  Route::get('cradleroll/{id}', ['uses' => 'CradlerollController@showCradleroll', 'as' => 'cradleroll.show']);

});
