@extends('layouts/default')

@section('content')


  <div class="banner-div">
    {{ $ministry->name}} Members
    <a class="btn btn-default pull-right" href="{{ route('ministry.notices', $ministry->id)}}" style='margin-left:2 0px;'>
          <i class="fa fa-home"></i>
    </a>
    <a class="btn btn-default pull-right" href="{{ route('ministry.show', $ministry->id)}}" style='margin-left:2 0px;'>
          <i class="fa fa-cog"></i>
    </a>
  </div>

  @foreach ($members as $mem)
  <div class="col-sm-6 col-md-4">
    <div class="card">
      <div class="card-block">
        <div class="card-header">
          <table><tr>

            <td>
              <img src="{{ $mem->photo }}" >
            </td>

            <td><h3 class="card-title">{{$mem->firstname . ' ' . $mem->surname}}</h3></td></tr>
          </table>
        </div>
        <p class="card-text">{{$mem->role}}</p>

      </br>

      </div>
      <div class="card-footer">
        <a class="fill-div" href="{{ route('person.show', ['id' => $mem->id])}}" class="card-button btn btn-primary">View Person</a>
      </div>

    </div>
  </div>
  @endforeach


@stop
