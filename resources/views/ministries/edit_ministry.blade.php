@extends('layouts/default')

@section('content')
<div class="banner-div">
  <?= $ministry->name ?>
  @if (isset($ministry->id))
  <a class="btn btn-default pull-right" href="{{ route('ministry.notices', ['id' => isset($ministry->id) ? $ministry->id : 0])}}" style='margin-left:2 0px;'>
        <i class="fa fa-home"></i>
  </a>
  <a class="btn btn-default pull-right" href="{{ route('ministry.members', ['id' => isset($ministry->id) ? $ministry->id : 0])}}" style='margin-left:2 0px;'>
        <i class="fa fa-users"></i>
  </a>
  @endif
</div>
  <div class="form-block">

    {!! Form::model($ministry, ['route' => 'ministry.save', 'files'=>true, 'id'=>'formsubmit']) !!}
      {!! csrf_field() !!}
      <!-- <div class="form-section">First Name &amp; Address</div> -->
        <div class="form-inner-wrap">
          <img src="{{ $photo_url }}" id="headshot" alt="headshot" style="border:1px solid #000000; text-align:center;width:200px;height:200px;user-drag: none; -moz-user-select: none; -webkit-user-drag: none;">
          <input type="hidden" id='photo_filename' name='pic' value='<?=$ministry['pic']?>' />
          <div id='crop-image' style="display:none; width:200px;height:300px;"></div>
          <input type="file"  name='headshot_file' id="selectedFile" style="display: none;" onchange="readURL(this);" />
          <input type="button" class="btn btn-pic btn-default" style="width:85px" value="Browse..." onclick="browseImage();"/>
          <a data-toggle="tooltip" data-placement="top" class="btn btn-pic btn-default" {{ (!isset($ministry->pic ) || !$ministry->pic) ? 'style=display:none'  : '' }} value="Remove image" id="removeBtn" title="Remove image">
            <i class="fa fa-remove " aria-hidden="true"></i>&nbsp;Remove
          </a>
          <a id='rotateBtn' data-toggle="tooltip" data-placement="top" onclick="rotateImage();" style="display:none;width:auto;" class="btn btn-pic btn-default" title="Rotate image right">
            <i class="fa fa-rotate-right " aria-hidden="true"></i><br/>&nbsp;
          </a>
          <a id='saveBtn' data-toggle="tooltip" data-placement="top"  style="display:none;width:90px" onclick="saveImage();" class="btn btn-pic btn-success btn-default" title="Set image for player">
            <i class="fa fa-check " aria-hidden="true"></i>&nbsp;Done
          </a>
          <span class="help-block has-error" id="help-block-head">
          </span>
        </br>
      </br>
            <label>Name </label> {!!Form::text('name', Input::get('name'), ['class' => 'form-control']) !!}
            <label>Description </label> {!!Form::text('description', Input::get('description'), ['class' => 'form-control']) !!}
            <label>Minister </label> {!!Form::select('minister_id', $list_ministers, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10']) !!}
</div>
        <input type="hidden" name="id" value="<?= $ministry['id'] ?>" >
        <input type="hidden" id='imagebase64' name='imagebase64' value='' style='display:none;'/>

        <input type='submit' id="submit" name="submit" value="Submit" class='btn btn-thin btn-success'>
          <!-- Save
        </button> -->
        <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
          Cancel
        </button>



    </form>




  </div>

@stop

@section('scripts')
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete2"
       async defer></script> -->

<script type="text/javascript">



var $uploadCrop;
$(document).ready(function(){
  	$('.btn-pic').tooltip();
      $uploadCrop = $('#crop-image').croppie({
         viewport: {
            width: 180,
            height: 180,
            type: 'rectangle'
         },
         boundary: {
            width: 200,
            height: 200
         },
         enableOrientation: true,
      });



    });


var godzilla = document.getElementById('selectedFile');

function roar()
{
    if(godzilla.value.length) {
      var file = $('#selectedFile')[0].files[0];
      if ($('#photo_filename').val() == file.name) {
        readURL(godzilla);
      }
      //alert('pong');
    } else {
      //alert('ping');
      $('#crop-image').hide();
      $('#saveBtn').hide();
      $('#rotateBtn').hide();
      if ($('#photo_filename').val()) {
        $('#removeBtn').show();
      }
      $('#headshot').show();
    }
    document.body.onfocus = null

}

function browseImage() {
  document.getElementById('selectedFile').click();
  document.body.onfocus = roar

}

$('#removeBtn').on('click', function (ev) {
    $('#imagebase64').val(null);
    $('#photo_filename').val('');
    $('#headshot_file').val(null);
    $('#headshot')
            .attr('src', "{{url('/images/unknown.gif')}}")
            .width(200)
            .height(200)
            .show();
    $('#removeBtn').hide();
    });

function saveImage() {
  $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
        $('#imagebase64').val(resp);
        //console.log('response: '+resp);
        $('#headshot')
                .attr('src', resp)
                .width(200)
                .height(200)
                .toggle();
        $('#crop-image').toggle();
        $('#saveBtn').toggle();
        $('#rotateBtn').toggle();
        $('#removeBtn').show();
        //$('#form').submit();
    });
}

function readURL(input) {
//  alert('ping');
        if (input.files && input.files[0]) {
          var tipe = input.files[0].type;
          if (tipe == "image/jpeg" || tipe=="image/png") {
            var reader = new FileReader();
            $('#crop-image').show();
            $('#saveBtn').show();
            $('#rotateBtn').show();
            $('#headshot').hide();
            $('#removeBtn').hide();

            reader.onload = function (e) {
            //  console.log(e.target.result);
            $uploadCrop.croppie('bind', {

                url: e.target.result
            });

            //$('.upload-demo').addClass('ready');
        }

            reader.readAsDataURL(input.files[0]);
            var file = $('#selectedFile')[0].files[0]

            $('#photo_filename').val(file.name);

            document.getElementById("help-block-head").innerHTML = "";
        } else {
          document.getElementById("help-block-head").innerHTML = "<strong>Invalid file type - please select a valid image file (jpeg or png)</strong>";
        }
      }
    }







</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete"
       async defer></script>
@stop
