@extends('layouts/default')

@section('content')


  <div class="banner-div">
    Ministries
    <a class="btn btn-default pull-right" href="{{ route('ministry.show', 0)}}" style='margin-left:2 0px;'>
          <i class="fa fa-plus"></i>
          <span>&nbsp; New ministry</span>
    </a>
  </div>

  @foreach ($ministries as $mini)
  <div class="col-sm-6 col-md-4">
    <div class="card">
      <div class="card-block">
        <div class="card-header">
          <table><tr>
            @if ($mini->pic)
            <td>
              <img src="{{ $mini->pic }}" >
            </td>
            @endif
            <td><h3 class="card-title">{{$mini->name}}</h3></td></tr>
          </table>
        </div>
        <p class="card-text">{{$mini->description}}</p>
        <!-- <a  href="{{ route('minister.show', ['id' => $mini->id])}}" class="card-button btn btn-primary">Show</a> -->
      </br>

      </div>
      <!-- <div class="card-footer" style="border-bottom: 1px solid #fee">
        <a class="fill-div" href="{{ route('ministry.members', ['id' => $mini->id])}}" class="card-button btn btn-primary">Members</a>
      </div> -->
      <div class="card-footer">
        <a class="fill-div" href="{{ route('ministry.notices', ['id' => $mini->id])}}" class="card-button btn btn-primary">Show</a>
      </div>
    </div>
  </div>
  @endforeach


@stop
