@extends('layouts/default')

@section('content')
<div class="banner-div">
  Residence for
  @if (Auth::user()->hasRole("Admin") || Auth::user()->hasRole("SuperAdmin"))
    <a class="btn btn-default pull-right" href="{{ route('person.show', 0)}}" style='margin-right:10px;'>
          <i class="fa fa-plus"></i>
          <span>&nbsp; New residence</span>
    </a>
  @endif
</div>


<div class="datatable-list">
<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Status</th>
                <th>Church</th>
                <th>Contact</th>
                <th>Address</th>
                <th></th>
            </tr>
        </thead>
</table>
</div>


@stop

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
@section('scripts')
<script type="text/javascript">





var person_id = 0;
function edit_row(id) {
  //
  //
  //   $.showLoading({
  //    name: 'circle-fade'
  //   });
  //
  // if (id > 0) {
  // $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //         $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //         //$("#editbtn_"+id).button('reset');
  //     });
  //   } else {
  //   //  $('#add_verifyidModal').load('/Person/show_verify', function(response) {
  //   //     $('#add_verifyidModal').modal( {backdrop:'static'} );
  //   $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //           $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //   });
  //
  //
  // }

}

$(document).ready(function (){
 // Array holding selected row IDs
 var rows_selected = [];
 var table = $('#users-table').DataTable({
   processing: true,
   serverSide: true,
   ajax: '{!! route('residence.data') !!}',
  columns: [{data: 'id', name: 'id', visible: false,},
            {data: 'firstname', name: 'firstname'},
            {data: 'surname', name: 'surname'},
            {data: 'status', name: 'status', searchable: false},
            {data: 'churchname', name: 'church', searchable: false},
            {data: 'cellno', name: 'cellno', searchable: false},
            {data: 'street', name: 'street'},
            {data: 'id',
                searchable: false,
                orderable: false,
                width: '1%',
                className: 'dt-body-center',
              render: function(data){
                    return '<a class="btn btn-thin btn-default pull-right" href="/person/'+data+'" style="margin-right:10px;">Edit</a>';
                },}
          //   { targets: 4,
          //   searchable: false,
          //   orderable: false,
          //   width: '1%',
          //   className: 'dt-body-center',
          //   render: function (data, type, full, meta){
          //     //  var b1 = "<button class='btn btn-xs btn-success' id='editbtn_"+data[0]+"' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //     //  onclick='edit_row("+data[0]+")' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //
          // //    var b1 = "<button class='btn btn-xs btn-success' id='editbtn_1' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //   //   onclick='edit_row(1)' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //   //    return b1;
          //   return '<button title="checkbox'+data.id+'">Edit</button>';
          //   }},
        ],
        aaSorting: [ [1,'asc'],[2, 'asc']],



 });



});



</script>
@stop
