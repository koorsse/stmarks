<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<style>
  html, body {
    min-height:100%;
    /*background-color: gray;*/
  }

  body{
    background-size: cover;
    background-repeat: no-repeat;
    background-image: url('../images/background1.jpg');

  }

  .container-fluid .row-fluid {
    margin: 0px;
    padding: 0px;
  }

  .main-leftwrapper {
    height: 90%;
    padding-left: 25px;
  }

  .main-right {
    height: 90%;
    /*display: table-cell;
    vertical-align: middle;*/
    margin-top: 25px;
    padding-left: 25px;
  }

  .main-left p {
    text-align: left;
    font-size: 13px;
    font-weight: normal;

  }



  .main-left {
    display: table-cell;
    vertical-align: middle;
    text-align: center;

  }

  .landing-footer {
    position: relative;
    margin-top: 0px; /* negative value of footer height */
    height: 30px;
    clear: both;
  }

  #info .landing-footer {
    text-align: right;
  }

  #login {
    position: relative;
    height: 100%;
    /*background-color: black;
    opacity:0.4;
    filter:alpha(opacity=40); /* For IE8 and earlier */*/
  }

  a:link {color: #fff; text-decoration: underline; }
  a:active {color: #fff; text-decoration: underline; }
  a:visited {color: #fff; text-decoration: underline; }
  a:hover {color: #fff; text-decoration: underline; }

  #block-right {
    background: #fff;
    filter: alpha(opacity=10);
    /* IE */
    -moz-opacity: 0.1;
    /* Mozilla */
    opacity: 0.1;
    /* CSS3 */
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  #block {
    background: #000;
    filter: alpha(opacity=60);
    /* IE */
    -moz-opacity: 0.6;
    /* Mozilla */
    opacity: 0.6;
    /* CSS3 */
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  h2 {
    color: white;
  }

  h1 {
    color: white;
    text-align:center;
    font-size: 28px;
    font-weight: 200;

    /*margin-left: 20px;*/
  }

  .form-control {
    color: black;
    border-radius: 1px;
  }

  .form-group {
    margin-bottom:1px;
    text-align: center;
  }

  #info {
    /*margin: 20px;*/
    position: relative;
    height: 100%;

  }

  #info h1 {
    color: white;
    text-align:center;
    font-size: 40px;
    /*margin-left: 20px;*/
  }

  #info h2 {
    color:white;
    font-size:22px;
    font-weight: 200;
  }



  #info p {
    color: white;
    font-size: 14px;
    letter-spacing: 1.22px;

  }

  #text {
    position: absolute;
    color: white;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  #registersubmit {
    /*width: 80px;*/
    background-color: #426FA8;
    border: none;
    color:white;
  }
</style>



  <div class="container-fluid" style="margin: 0px; padding: 0px;height: 100%;">
    <div class="row-fluid" style="height:100%;">
      <div id="login" class="col-sm-5" >
        <div id="block"></div>
        <div id="text">

          <div class="main-leftwrapper">
            <div class="main-left col-sm-9 col-sm-offset-1">
              <div class="row" style="height: 100px; margin-top:55px;">
                <img src="{{asset('images/logo_new.png')}}" height="100px"/>
              </div>

              @yield('login')
              <!-- <h2>REGISTER</h2>
              <p style="font-size: 12px;">To register simply fill in your details here, and click 'Register'</p>
              <div class="form-group">
                <input class="form-control" type="text" name="email" placeholder="Email address">
              </div>
              <div class="form-group">
                <input class="form-control" type="text" name="password" placeholder="Password">
              </div>
              <div class="form-group">
                <input class="form-control" type="text" name="confirmpassword" placeholder="Confirm password">
              </div>
              <div class="form-group " >
                <input id="registersubmit" class="form-control " type="submit" name="submit" value="Register">
              </div> -->

              <!-- <div class="form-group" style="padding-top: 35px;">
                <h2>LOGIN</h2>
              </div>
              <form  role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <input class="form-control" type="text" name="email" placeholder="Email address" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} col-sm-6" style="padding-left: 0px; padding-right: 0px;">
                  <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                  <-- @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif ->
                </div>


                <div class="form-group col-sm-6" style="padding-left:5px; padding-right: 0px;">
                  <button id="registersubmit" class="form-control" type="submit" name="submit">Login</button>
                </div>


                <div class="form-group">

                    <div >
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>

                </div>
                </form>
                <p style="font-size: 13px; margin-bottom: 30px;"><a  href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a></p> -->
              <!-- <p style="font-size: 13px; color: #426FC8; ">Request access for your <b>Region or LFA</b></p> -->




              <h1>ST MARKS CONGREGATIONAL CHURCH</h1>
              <p>
                Reach, preach and teach!
              </p>

            </div>
          </div>
          <!-- <div class="landing-footer col-sm-9 col-sm-offset-1">
            <div class="heading" style="color:white;"><img style="float: left; margin: 7px 7px 7px 0px;" src="{{asset('images/mysafalogo.png')}}" height="15px"/>Registration</div>
            <div> Powered by: INQAKU and SAFA Digital</div>
          </div> -->
        </div>
      </div>
      <div id="info" class=" col-sm-7" >
        <div id="block-right"></div>
        <div id="text">
          <div class="main-right col-sm-10 col-sm-offset-1">

            <!-- <h1>GET 100% SAFA <span style="color: #426FA8">ACCREDITED</span></h1>
            <h2>YOUR ONE STOP SHOP FOR TRACKING PROGRESS AND INVOLVEMENT IN SOCCER</h2> -->
            <!-- <h1>BRINGING SA FOOTBALL TOGETHER</h1>
            <p>
              MYSAFA is a stakeholder management and communication system launched by the South African Football Association (SAFA) for use within its internal structures.
            </p>
            <p>
              By aligning all Provinces, Regions and LFA’s onto a single platform, MYSAFA will promote efficient football administration and the enhancement of the sport throughout South Africa.
            </p> -->
          </div>
          <div class="landing-footer col-sm-12 " style="margin-top: -10px;">

            <!-- <img style="margin-right: 10px;" src="{{asset('images/safadigital_white.png')}}"  height="40px"/>
            <img src="{{asset('images/inqaku_white.png')}}" height="50px"/> -->
          </div>
        </div>

      </div>
    </div>
  </div>






  @include('layouts.script')
</body>
</html>
