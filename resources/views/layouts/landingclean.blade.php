<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<style>
  html, body {
    min-height:100%;
    /*background-color: gray;*/
  }

  body{
    background: white;
    text: #004661;

  }

  .container-fluid .row-fluid {
    margin: 0px;
    padding: 0px;
  }

  .main-leftwrapper {
    height: 90%;
    padding-left: 25px;
  }

  .main-right {
    height: 90%;
    /*display: table-cell;
    vertical-align: middle;*/
    margin-top: 25px;
    padding-left: 25px;
  }

  .main-left p {
    text-align: left;
    font-size: 13px;
    font-weight: normal;

  }



  .main-left {
    display: table-cell;
    vertical-align: middle;
    text-align: center;

  }

  .landing-footer {
    position: relative;
    margin-top: 0px; /* negative value of footer height */
    height: 30px;
    clear: both;
  }

  #info .landing-footer {
    text-align: right;
  }

  #login {
    position: relative;
    height: 100%;
    /*background-color: black;
    opacity:0.4;
    filter:alpha(opacity=40); /* For IE8 and earlier */*/
  }

  a:link {color: #004661; text-decoration: underline; }
  a:active {color: #004661; text-decoration: underline; }
  a:visited {color: #004661; text-decoration: underline; }
  a:hover {color: #004661; text-decoration: underline; }

  #block-right {
    background: white;
    filter: alpha(opacity=10);
    /* IE */
    -moz-opacity: 0.1;
    /* Mozilla */
    opacity: 0.1;
    /* CSS3 */
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  #block {
    background: #000;
    filter: alpha(opacity=60);
    /* IE */
    -moz-opacity: 0.6;
    /* Mozilla */
    opacity: 0.6;
    /* CSS3 */
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  h2 {
    color: #004661;
    /*font-family: Open Sans Condensed*/
  }

  h1 {
    color: #004661;
    text-align:center;
    font-size: 28px;
    font-weight: 200;

    /*margin-left: 20px;*/
  }

  .form-control {
    color: #004661;
    border-radius: 1px;
  }

  .form-group {
    color: #004661;
    margin-bottom:1px;
    text-align: center;
  }

  #info {
    /*margin: 20px;*/
    position: relative;
    height: 100%;

  }

  #info h1 {
    color: #004661;
    text-align:center;
    font-size: 40px;
    /*margin-left: 20px;*/
  }

  #info h2 {
    color:#004661;
    font-size:22px;
    font-weight: 200;
  }



  #info p {
    color: #004661;
    font-size: 14px;
    letter-spacing: 1.22px;

  }

  #text {
    position: absolute;
    color: #004661;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  #registersubmit {
    /*width: 80px;*/
    background-color: #004661;
    border: none;
    color:#fff;
  }
  @media (max-width: 40em) {
    body {
      font-size: 25px;
    }

    .form-control {
      font-size: 50px;
      background-color: pink;
    }
}

  @media screen and (max-width: 1000px) {


    body {
      font-size: 25px;
    }

    .form-control {
      font-size: 42px;
      /*background-color: pink;*/
      height: 70px;

    }
    .form-group {
      padding-bottom: 30px;
    }
  }
</style>



  <div class="container-fluid" style="margin: 0px; padding: 0px;height: 100%;">
    <div class="row-fluid" style="height:100%;">



          <div class="main-leftwrapper">
            <div class="main-left col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3">
              <div class="row" style="height: 100px; margin-top:55px;">
                <img src="{{asset('images/stm_logo_1.png')}}" height="100px"/>
              </div>
            <div class="col-xs-12 col-md-8 col-md-offset-2" style="margin-top:35px; margin-bottom: 15px;padding-top:20px;padding-bottom:15px;">
              @yield('login')
            </div>



              <!-- <h1>ST MARKS CONGREGATIONAL CHURCH</h1> -->
              <br/>
              <div class="col-sm-12">
                <p style="text-align:center;color:#494949;font-family: Source Sans Pro; font-weight: 300 !important; font-size: 35px;font-style:italic; line-height:65px;">
                  <span>Reach, teach and preach</span>
                </p>
              </div>

            </div>
          </div>


    </div>
  </div>






  @include('layouts.script')
</body>
</html>
