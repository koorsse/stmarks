<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
  <body>
    <header >
        @include('includes.navbar')
    </header>

<!-- <div class="s-layout"> -->
<div class="wrapper">
    @include('includes.sidebar')

    <!-- Content -->
    <!-- <main class="s-layout__content">
      yield('content')
    </main> -->
    <div class="main col-sm-10 col-xs-11" id="main">
      <!-- @if (isset($pagetitle))
      <div class="banner-div">
        <p>{{ $pagetitle }}</p>
      </div>
      @endif -->
      @yield('content')
    </div>
</div>

@include('layouts.script')
</body>
</html>
