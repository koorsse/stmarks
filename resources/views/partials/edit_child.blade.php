

  <div class="form-block">

    {!! Form::model($child, ['route' => 'children.save', 'id'=>'personform']) !!}
      {!! csrf_field() !!}
        <div class="form-inner-wrap">
          <img src="{{ $photo_url }}" id="headshot" alt="headshot" style="border:1px solid #000000; text-align:center;width:200px;height:200px;user-drag: none; -moz-user-select: none; -webkit-user-drag: none;">
          <input type="hidden" id='photo_filename' name='photo' value='<?=$child['photo']?>' />
          <div id='crop-image' style="display:none; width:200px;height:300px;"></div>
          <input type="file"  name='headshot_file' id="selectedFile" style="display: none;" onchange="readURL(this);" />
          @if (!$readonly)
          <input type="button" class="btn btn-pic btn-default" style="width:85px"  value="Browse..." onclick="browseImage();"/>

            <a data-toggle="tooltip" data-placement="top" class="btn btn-pic btn-default" {{ (!isset($child->photo ) || !$child->photo) ? 'style=display:none'  : '' }} value="Remove image" id="removeBtn" title="Remove image">
              <i class="fa fa-remove " aria-hidden="true"></i>&nbsp;Remove
            </a>

            <a id='rotateBtn' data-toggle="tooltip" data-placement="top" onclick="rotateImage();" style="display:none;width:auto;" class="btn btn-pic btn-default" title="Rotate image right">
              <i class="fa fa-rotate-right " aria-hidden="true"></i><br/>&nbsp;
            </a>
            <a id='saveBtn' data-toggle="tooltip" data-placement="top"  style="display:none;width:90px" onclick="saveImage();" class="btn btn-pic btn-success btn-default" title="Set image for player">
              <i class="fa fa-check " aria-hidden="true"></i>&nbsp;Done
            </a>
          @endif
          <span class="help-block has-error" id="help-block-head">
          </span>
        </br>
      </br>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <label>Name </label> {!!Form::text('name', Input::get('name'), ['class' => 'form-control', 'readonly' => $readonly, 'id' => 'name']) !!}
            <label>Surname </label> {!!Form::text('lastname', Input::get('lastname'), ['class' => 'form-control', 'readonly' => $readonly, 'id' => 'surname']) !!}

            <!-- <label>Gender </label><br>
            <div style="margin-left: 30%;" class="col-sm-12 col-md-12 col-lg-12">
              <div class="col-sm-2 col-md-2 col-lg-2">
                <input class="gender" type="radio" value="M" <= $person['gender'] == 'M' ? 'checked' : '' ?> <= $readonly ? 'disabled' : '' ?> id="genderM" name="gender">
              <label for="male"> Male </label>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2">
              <input class="gender" type="radio" value="F" <= $person['gender'] == 'F' ? 'checked' : '' ?> <= $readonly ? 'disabled' : '' ?> id="genderF" name="gender">
              <label for="female">Female </label><br>
            </div>
          </div> -->
          <label>Date of birth </label> {!!Form::text('dob', Input::get('dob'), ['class' => 'datepicker form-control', 'readonly' => $readonly]) !!}
          <label style="font-size:smaller;">Specify birth year as 1900 if only birth day (day-month) is known. Specify date of birth as 1/1/1901 if not known.</label>
          <br/>
          <label>Grade </label> {!!Form::select('grade', $grades, null, ['class' => 'select-picker form-control', 'data-live-search'=>"true", 'data-size' => '10', 'disabled' => $readonly]) !!}

          <label>Home address </label> {!!Form::text('homeaddr', Input::get('homeaddr'), ['class' => 'form-control', 'readonly' => $readonly, 'id' => 'homeaddr']) !!}
          <!-- <label>Visitor? </label> {!!Form::checkbox('visitor', 0,[ 'class' => 'form-control', 'value' => '0','disabled' => $readonly]) !!} -->


        </div>
        <div class="form-inner-wrap">
            <label>Mother name </label> {!!Form::text('name_mom', Input::get('name_mom'), ['id' => 'name_mom', 'class' => 'form-control', 'readonly' => $readonly]) !!}
            <label>Mother contact </label> {!!Form::text('contact_mom', Input::get('contact_mom'), ['class' => 'contact-tel form-control', 'readonly' => $readonly]) !!}
            <label>Father name </label> {!!Form::text('name_dad', Input::get('name_dad'), ['class' => 'form-control', 'readonly' => $readonly]) !!}
            <label>Father contact </label> {!!Form::text('contact_dad', Input::get('contact_dad'), ['class' => 'contact-tel form-control', 'readonly' => $readonly]) !!}
            <label>Email </label> {!!Form::email('email1', Input::get('email1'), ['class' => 'form-control', 'readonly' => $readonly]) !!}
            <label>Email (alt) </label> {!!Form::email('email2', Input::get('email2'), ['class' => 'form-control', 'readonly' => $readonly]) !!}
        </div>




        <input type="hidden" id='childid' name="id" value="<?= $child['id'] ?>" >

        <input type="hidden" id='imagebase64' name='imagebase64' value='' style='display:none;'/>


        <input type='button' id="submitbtn" name="submitbtn" value="Submit" class='btn btn-thin btn-success'>
          <!-- Save
        </button> -->
        <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
          Cancel
        </button>

      </form>
  </div>




@section('scripts')
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete2"
       async defer></script> -->

<script type="text/javascript">

var csrf_token = $('meta[name="csrf-token"]').attr('content');


$(document).on('keyup', '.contact-tel', fixContactNumber);
$(document).on('keypress', '.contact-tel', fixContactNumber);
function fixContactNumber(event)
    {
//accepts two formats +27 82 123 4567 or 041 123 4567 (automatically adds spaces) does not accept letters (only numbers and +)
        var contactNumber = $(this);
        var key = event.which ? event.which : event.keyCode;
        var field = contactNumber.val();

        if ((key < 48 || key > 57) && key !== 43)
          event.preventDefault();
        if(key === 189 || key === 45 || (field[0] !== '+' && field.length === 12) || (field[0] === '+' && field.length === 15)) //189 dash, 45 insert
            event.preventDefault();

        if (field[0] !== '+') {
          if ((field.length === 3 || field.length === 7) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        } else {
          if ((field.length === 3 || field.length === 6 || field.length === 10) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        }

    }


var $uploadCrop;
$(document).ready(function(){





    $('#submitbtn').click(function (e) {

        if ($('#childid').val() == 0) {
          //e.preventDefault();
          // check to see if person already exists....
          if ($('#name').val() == '' || $('#surname').val() == '') {
            bootbox.alert('A name and surname is required');
          } else {
            $.ajax({
                type: "POST",
                url: "{!! route('child.exists') !!}",
                data: {
                  name: $('#name').val(),
                  surname: $('#surname').val(),
                  dob: $('#dob').val(),
                   _token :csrf_token,
                },
                success: function (data) {

                  if (data.exists) {
                    bootbox.confirm ({
                      message: "There is already a person with the name " + data.search[0]['name'] + " " + data.search[0]['lastname'] + ". Do you still want to continue adding " + $('#name').val() + " " + $('#surname').val() + "?",
                        buttons: {
                            confirm: {
                                label: 'Yes, create new person',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No, cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                          if (result) {
                            $('#personform')[0].submit();
                          }

                        }
                    });
                  }
                  else {
                    //just submit as no duplicate...

                    $('#personform')[0].submit();

                  }
                },
                error: function (response, x, y) {
                  bootbox.alert('Error saving person');
                //  $('#formsubmit').submit();
                }
              });
          }
        } else {
          //submit as not a new person

          $('#personform')[0].submit();
        }

    });




  	$('.btn-pic').tooltip();
      $uploadCrop = $('#crop-image').croppie({
         viewport: {
            width: 180,
            height: 180,
            type: 'rectangle'
         },
         boundary: {
            width: 200,
            height: 200
         },
         enableOrientation: true,
      });



    }); //end document ready


var d = new Date(00,0,1);
$( ".datepicker" ).datepicker({
  defaultDate:d,
  dateFormat: 'dd/mm/yy',
  changeMonth: true,
  changeYear: true,
  yearRange: "1900:+0", // last hundred years
  onChangeMonthYear: function (year, month, inst) {
              var date = $("#datepicker").val();
              if ($.trim(date) != "") {
                  var newDate = month + "/" + inst.currentDay + "/" + year;
                  $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
              }
          }
});

$( ".currdatepicker" ).datepicker({

  dateFormat: 'dd/mm/yy',
  changeMonth: true,
  changeYear: true,
  yearRange: "-50:+0", // last hundred years
  onChangeMonthYear: function (year, month, inst) {
              var date = $("#datepicker").val();
              if ($.trim(date) != "") {
                  var newDate = month + "/" + inst.currentDay + "/" + year;
                  $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
              }
          }
});

var godzilla = document.getElementById('selectedFile');

function roar()
{
    if(godzilla.value.length) {
      var file = $('#selectedFile')[0].files[0];
      if ($('#photo_filename').val() == file.name) {
        readURL(godzilla);
      }
      //alert('pong');
    } else {
      //alert('ping');
      $('#crop-image').hide();
      $('#saveBtn').hide();
      $('#rotateBtn').hide();
      if ($('#photo_filename').val()) {
        $('#removeBtn').show();
      }
      $('#headshot').show();
    }
    document.body.onfocus = null

}

function browseImage() {
  document.getElementById('selectedFile').click();
  document.body.onfocus = roar

}

$('#removeBtn').on('click', function (ev) {
    $('#imagebase64').val(null);
    $('#photo_filename').val('');
    $('#headshot_file').val(null);
    $('#headshot')
            .attr('src', "{{url('/images/unknown.gif')}}")
            .width(200)
            .height(200)
            .show();
    $('#removeBtn').hide();
    });

function saveImage() {
  $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
        $('#imagebase64').val(resp);
        //console.log('response: '+resp);
        $('#headshot')
                .attr('src', resp)
                .width(200)
                .height(200)
                .toggle();
        $('#crop-image').toggle();
        $('#saveBtn').toggle();
        $('#rotateBtn').toggle();
        $('#removeBtn').show();
        //$('#form').submit();
    });
}

function readURL(input) {
//  alert('ping');
        if (input.files && input.files[0]) {
          var tipe = input.files[0].type;
          if (tipe == "image/jpeg" || tipe=="image/png") {
            var reader = new FileReader();
            $('#crop-image').show();
            $('#saveBtn').show();
            $('#rotateBtn').show();
            $('#headshot').hide();
            $('#removeBtn').hide();

            reader.onload = function (e) {
            //  console.log(e.target.result);
            $uploadCrop.croppie('bind', {

                url: e.target.result
            });

            //$('.upload-demo').addClass('ready');
        }

            reader.readAsDataURL(input.files[0]);
            var file = $('#selectedFile')[0].files[0]

            $('#photo_filename').val(file.name);

            document.getElementById("help-block-head").innerHTML = "";
        } else {
          document.getElementById("help-block-head").innerHTML = "<strong>Invalid file type - please select a valid image file (jpeg or png)</strong>";
        }
      }
    }


  </script>
@stop
