@extends('layouts/default')

@section('content')

  <div class="form-block">
    <!-- {!! Form::model($person, ['id' => 'person-form', ]) !!} -->
    {!! Form::model($person, ['route' => 'person.save', 'files'=>true, 'id'=>'personform']) !!}
      {!! csrf_field() !!}
        <div class="form-inner-wrap">
          <img src="{{ $photo_url }}" id="headshot" alt="headshot" style="border:1px solid #000000; text-align:center;width:200px;height:200px;user-drag: none; -moz-user-select: none; -webkit-user-drag: none;">
          <input type="hidden" id='photo_filename' name='photo' value='<?=$person['photo']?>' />
          <div id='crop-image' style="display:none; width:200px;height:300px;"></div>
          <input type="file"  name='headshot_file' id="selectedFile" style="display: none;" onchange="readURL(this);" />
          @if (!$readonly)
          <input type="button" class="btn btn-pic btn-default" style="width:85px"  value="Browse..." onclick="browseImage();"/>

            <a data-toggle="tooltip" data-placement="top" class="btn btn-pic btn-default" {{ (!isset($person->photo ) || !$person->photo) ? 'style=display:none'  : '' }} value="Remove image" id="removeBtn" title="Remove image">
              <i class="fa fa-remove " aria-hidden="true"></i>&nbsp;Remove
            </a>

            <a id='rotateBtn' data-toggle="tooltip" data-placement="top" onclick="rotateImage();" style="display:none;width:auto;" class="btn btn-pic btn-default" title="Rotate image right">
              <i class="fa fa-rotate-right " aria-hidden="true"></i><br/>&nbsp;
            </a>
            <a id='saveBtn' data-toggle="tooltip" data-placement="top"  style="display:none;width:90px" onclick="saveImage();" class="btn btn-pic btn-success btn-default" title="Set image for player">
              <i class="fa fa-check " aria-hidden="true"></i>&nbsp;Done
            </a>
          @endif
          <span class="help-block has-error" id="help-block-head">
          </span>
        </br>
      </br>
            @if (Auth::user()->hasRole('SuperAdmin'))

              <button type="button" class="btn btn-default" id="newuser"><?php if ($isuser) echo 'Update user'; else echo 'Add as user';?></button>
              <?php if ($isuser) echo $userrole; ?>
              <br/>
            @endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <label>Firstname </label> {!!Form::text('firstname', Input::get('firstname'), ['class' => 'form-control', 'readonly' => $readonly, 'id' => 'firstname']) !!}
            <label>Preferred name </label> {!!Form::text('preferred_name', Input::get('preferred_name'), ['class' => 'form-control', 'readonly' => $readonly]) !!}
            <label>Surname </label> {!!Form::text('surname', Input::get('surname'), ['class' => 'form-control', 'readonly' => $readonly, 'id' => 'surname']) !!}
            <label>Title </label> {!!Form::select('title', $titlelist, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10', 'disabled' => $readonly]) !!}
            <label>Gender </label><br>
            <div style="margin-left: 30%;" class="col-sm-12 col-md-12 col-lg-12">
              <div class="col-sm-2 col-md-2 col-lg-2">
                <input class="gender" type="radio" value="M" <?= $person['gender'] == 'M' ? 'checked' : '' ?> <?= $readonly ? 'disabled' : '' ?> id="genderM" name="gender">
              <label for="male"> Male </label>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2">
              <input class="gender" type="radio" value="F" <?= $person['gender'] == 'F' ? 'checked' : '' ?> <?= $readonly ? 'disabled' : '' ?> id="genderF" name="gender">
              <label for="female">Female </label><br>
            </div>
            </div>

            <label class="maidengroup" <?= $person['gender'] == "F" ? '' : 'hidden' ?>>Maiden </label>

            <input type="text" class="maidengroup form-control" name="maiden" id="maiden" value="<?= $person['maiden']?>" <?= $readonly ? 'readonly' : '' ?> hidden>
             <!-- <?= $person['gender'] == "F" ? 'hidden' : 'hidden' ?> -->
            <label>Date of birth </label> {!!Form::text('birthday', Input::get('birthday'), ['class' => 'datepicker form-control', 'readonly' => $readonly]) !!}
            <label style="font-size:smaller;">Specify birth year as 1900 if only birth day (day-month) is known. Specify date of birth as 1/1/1901 if not known.</label>
            <br/>
            <label>Deceased? </label> <input type="checkbox"  value="1" <?= $person['deceased_date'] ? 'checked' : '' ?> <?= $readonly ? 'disabled' : '' ?> name="deceased" id='deceased' >
            <br/>
            <label class="deceased">Deceased date </label> {!!Form::text('deceased_date', Input::get('deceased_date'), ['class' => 'currdatepicker form-control deceased', 'readonly' => $readonly]) !!}
            <br/>
            <label>Married? </label> <input type="checkbox"  value="1" <?= $person['married_date'] ? 'checked' : '' ?> <?= $readonly ? 'disabled' : '' ?> name="married" id='married' >
            <br/>
            <label class="married">Married date </label> {!!Form::text('married_date', Input::get('married_date'), ['class' => 'currdatepicker form-control married', 'readonly' => $readonly]) !!}
        </div>
        <div class="form-inner-wrap">
            <label>Email </label> {!!Form::text('email', Input::get('email'), ['id' => 'email', 'class' => 'form-control', 'readonly' => $readonly]) !!}
            <label>Mobile </label> {!!Form::text('cellno', Input::get('cellno'), ['class' => 'contact-tel form-control', 'readonly' => $readonly]) !!}
            <label>Work tel. </label> {!!Form::text('worktel', Input::get('worktel'), ['class' => 'contact-tel form-control', 'readonly' => $readonly]) !!}
            <label>Occupation </label> {!!Form::text('occupation', Input::get('occupation'), ['class' => 'form-control', 'readonly' => $readonly]) !!}
        </div>
        <div class="form-inner-wrap">
          @if (count($livethere) > 0)
          <div id="otherresidents" class="panel col-sm-12 col-md-12 col-lg-12">
            <h3>
              Other residents:
            </h3>
            @foreach($livethere as $occ)
            <div class = "col-sm-6 col-md-2">
              <div class = "thumbnail">
                 <img src ="<?= $occ->photo ?>" >
              </div>

              <div class = "caption">
                 <a  href="/person/<?= $occ->id?>" ><span class="label label-default"> {{$occ->firstname .' '.$occ->surname}}</span></a>

              </div>
           </div>
            @endforeach
          </div>
          @endif
          <!-- <button id="resbtn" type="button">Residence Info</button>
          <br/>

          Make all below readonly...
          <br/> -->
          <label>Home tel. </label> {!!Form::text('telephone', Input::get('telephone'), ['class' => 'contact-tel form-control', 'readonly' => $readonly]) !!}
          <label>Number and street </label>
          <!-- <input class="form-control" name="street" id="street"  value=""
                 onFocus="geolocate()" type="text"></input> -->
                {!!Form::text('street', Input::get('street'), ['class' => 'form-control', 'onFocus'=>'geolocate();', 'id' => 'street', 'readonly' => $readonly]) !!}
          <label>Suburb </label> {!!Form::text('suburb', Input::get('suburb'), ['class' => 'form-control', 'id' => 'suburb', 'readonly' => $readonly]) !!}
          <label>City/town </label> {!!Form::text('city', Input::get('city'), ['class' => 'form-control', 'id' => 'city', 'readonly' => $readonly]) !!}
          <label>Province </label> {!!Form::text('province', Input::get('province'), ['class' => 'form-control', 'id' => 'province', 'readonly' => $readonly]) !!}
          <label>Country </label> {!!Form::text('country', Input::get('country'), ['class' => 'form-control', 'id' => 'country', 'readonly' => $readonly]) !!}
          <label>Address to </label> {!!Form::text('address_to', Input::get('address_to'), ['class' => 'form-control', 'readonly' => $readonly]) !!}

          <input type="hidden" id="gpslocation" name="gpslocation" value='<?= $person->gps_location?>' >
          <br>
          <label>Different postal? </label> <input type="checkbox"  value="1" <?= $person['mailtopostal'] ? 'checked' : '' ?> <?= $readonly ? 'disabled' : '' ?> name="mailtopostal" id='mailtopostal' >
          <br>
        <br>
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?> >Postal address </label>
            <input class="postal form-control" type='text' name="postal_address" id="postal_address" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> <?= $readonly ? 'readonly' : '' ?> value="<?= $person["postal_address"]?>" >

          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>Suburb </label>
            <input class="postal form-control" type='text' name="postal_suburb" id="postal_suburb" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> <?= $readonly ? 'readonly' : '' ?> value="<?= $person["postal_suburb"]?>" >
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>City </label>
            <input class="postal form-control" type='text' name="postal_city" id="postal_city" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> <?= $readonly ? 'readonly' : '' ?> value="<?= $person["postal_city"]?>" >
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>Country </label>
            <input class="postal form-control" type='text' name="postal_country" id="postal_country" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> <?= $readonly ? 'readonly' : '' ?> value="<?= $person["postal_country"]?>" >
          <label>Code </label> {!!Form::text('code', Input::get('code'), ['class' => 'form-control', 'id' => 'code', 'readonly' => $readonly]) !!}

        </div>
        <div class="form-inner-wrap">
            <label>Church </label> {!!Form::select('church_id', $churches, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10', 'disabled' => $readonly]) !!}
            <label>Status </label> {!!Form::select('status_id', $statuslist, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10', 'disabled' => $readonly]) !!}
            <label>Join date </label> {!!Form::text('joindate', Input::get('joindate'), ['class' => 'form-control currdatepicker', 'readonly' => $readonly]) !!}
            <label>"The Voice" email </label> {!!Form::checkbox('voice_edistr', 1, null,[ 'disabled' => $readonly]) !!}
            <br/>
            <label>Comments </label> {!!Form::textarea('comments', Input::get('comments'), ['class' => 'form-control']) !!}
        </div>

        <div class="form-inner-wrap">
          <table>
            <tr><td style="width: 200px;">Minister </td>
              <?php
                $disabled =  $readonly ? 'disabled' : '';
                echo '<td><input type="checkbox" name="minister" ' . $disabled;

                if (isset($minister) && $minister) {
                  echo 'checked value="true" >';
                }
                // echo '</td>';
                if (isset($minister) && $minister && !$readonly)
                  echo '<td style="padding-left:5px;"><a class="btn btn-default" href="{{ route(\'ministers\')}}" >View </a></td>';

             ?>
            </tr>
            <tr><td style="width: 200px;">Pastor </td>
              <?php
                $disabled =  $readonly ? 'disabled' : '';
                echo '<td><input type="checkbox" name="pastor" ' . $disabled;

                if (isset($pastor) && $pastor) {

                  echo 'checked value="true" >';

                }
                // echo '</td>';
                if (isset($pastor) && $pastor && !$readonly)
                  echo '<td style="padding-left:5px;"><a class="btn btn-default" href="{{ route(\'ministers\')}}" >View </a></td>';

             ?>
            </tr>

            <?php
              foreach($ministries as $min) {
                $disabled =  $readonly ? 'disabled' : '';
                echo '<tr><td style="width: 200px;">' . $min['name'] . '</td>';
                echo '<td><input type="checkbox" name="' . str_replace(" ","_",$min['name']) . '"';
                if ($min['member']) {
                  echo 'checked ';
                }
                echo $disabled . ' >';
                if ($min['member'] && !$readonly) {
                //  route('minister.show', ['id' => $mini->id])}}
                  echo '<td><a class="btn btn-default" href="../ministrynotices/'.$min['id'].'">View</a></td>';
                }
                echo '</tr>';
              }
            ?>

            <!-- <tr><td style="width: 200px;">Minister </td> <td>{!!Form::checkbox('minister', 1) !!}</td>
            @if (isset($minister) && $minister)
              <a href="" >View </a>
            @endif
            </tr>
            <tr><td>Homegroup </td> <td> {!!Form::checkbox('homegroupleader', 1) !!} </td></tr>
            <tr><td>Visitations </td> <td>{!!Form::checkbox('visitation', 1) !!} </td></tr>
            <tr><td>Tea </td> <td> {!!Form::checkbox('tea', 1) !!} </td></tr>
            <tr><td>Worship </td> <td> {!!Form::checkbox('worship', 1) !!} </td></tr>
            <tr><td>Sound </td> <td>{!!Form::checkbox('sound', 1) !!} </td></tr>
            <tr><td>Youth leader </td> <td> {!!Form::checkbox('youth', 1) !!} </td></tr>
            <tr><td>Children's church </td> <td> {!!Form::checkbox('childrens', 1) !!} </td></tr>
            <tr><td>Deacon </td> <td>{!!Form::checkbox('deacon', 1) !!} </td></tr>
            <tr><td>Bookshop </td> <td> {!!Form::checkbox('bookshop', 1) !!} </td></tr> -->
          </table>

        </div>


        <input type="hidden" id='personid' name="id" value="<?= $person['id'] ?>" >
        <input type="hidden" id="res_id" name="res_id" value="<?= $person['res_id'] ?>" >
        <input type="hidden" id='imagebase64' name='imagebase64' value='' style='display:none;'/>
        <input type="hidden" id='updateallresidence' name="updateallresidence" value=true >

        <input type='button' id="submitbtn" name="submitbtn" value="Submit" class='btn btn-thin btn-success'>
          <!-- Save
        </button> -->
        <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
          Cancel
        </button>

      </form>
  </div>

@stop


@section('scripts')
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete2"
       async defer></script> -->

<script type="text/javascript">

var originalstreet = "";
var originalpostal = "";


$(document).on('keyup', '.contact-tel', fixContactNumber);
$(document).on('keypress', '.contact-tel', fixContactNumber);
function fixContactNumber(event)
    {
//accepts two formats +27 82 123 4567 or 041 123 4567 (automatically adds spaces) does not accept letters (only numbers and +)
        var contactNumber = $(this);
        var key = event.which ? event.which : event.keyCode;
        var field = contactNumber.val();

        if ((key < 48 || key > 57) && key !== 43)
          event.preventDefault();
        if(key === 189 || key === 45 || (field[0] !== '+' && field.length === 12) || (field[0] === '+' && field.length === 15)) //189 dash, 45 insert
            event.preventDefault();

        if (field[0] !== '+') {
          if ((field.length === 3 || field.length === 7) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        } else {
          if ((field.length === 3 || field.length === 6 || field.length === 10) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        }

    }


var $uploadCrop;
$(document).ready(function(){

  originalstreet = $('#street').val();
  originalpostal = $('#postal_address').val();
  // console.log($('#genderF').is(':checked'));
  // alert(<? $person['gender'] ?>);

  if ($('#deceased').is(':checked')) {
    $('.deceased').show();
  } else {

    $('.deceased').hide();
  }

  if ($('#married').is(':checked')) {
    $('.married').show();
  } else {

    $('.married').hide();
  }

    if ($('#genderF').is(':checked')) {
      $('.maidengroup').show();
    } else {
      $('.maidengroup').hide();
    }



    $('#submitbtn').click(function (e) {

        if ($('#personid').val() == 0) {
          //e.preventDefault();
          // check to see if person already exists....
          if ($('#firstname').val() == '' || $('#surname').val() == '') {
            bootbox.alert('A name and surname is required');
          } else {
            $.ajax({
                type: "POST",
                url: "{!! route('person.exists') !!}",
                data: {
                  name: $('#firstname').val(),
                  surname: $('#surname').val(),
                   _token :csrf_token,
                },
                success: function (data) {
                  // bootbox.alert('New user added');
                  if (data.exists) {
                    bootbox.confirm ({
                      message: "There is already a person with the name " + $('#firstname').val() + " " + $('#surname').val() + ". Do you still want to continue adding a new person?",
                        buttons: {
                            confirm: {
                                label: 'Yes, create new person',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No, cancel',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {

                          if (result) {
                            $('#personform')[0].submit();
                          }

                        }
                    });
                  }
                  else {
                    //just submit as no duplicate...

                    $('#personform')[0].submit();

                  }
                },
                error: function (response, x, y) {
                  bootbox.alert('Error saving person');
                //  $('#formsubmit').submit();
                }
              });
          }
        } else {
          //submit as not a new person

          $('#personform')[0].submit();
        }

    });


    $('#newuser').click(function (e){
    //  alert('clicked');
      //does user have an email address....
      if ($('#email').val() == '') {
        bootbox.alert('User needs an email address to login with');
      }
      else

      bootbox.confirm("<form id='infos' action=''>\
          Enter password:<input class='form-control' type='text' id='password' /><br/>\
          Role:<select class='form-control' id='role' ><option selected value='readonly'>Read user</option>\
                <option value='admin'>Admin user</option> \
                </select>\
          </form>", function(result) {
              if(result) {
                data = {
                     personid: $('#personid').val(),
                     password: $('#password').val(),
                     role: $('#role').val(),
                     _token :csrf_token,
                   }
                   $.ajax({
                       type: "POST",
                       url: "{!! route('user.tempnew') !!}",
                       data: data,
                       success: function (response) {
                         bootbox.alert('New user added');
                       },
                       error: function (response, x, y) {
                         bootbox.alert('User could not be created');
                       }
                     });
              }


          });





      // bootbox.prompt({
      //   size: "small",
      //   title: "Enter user password:",
      //   callback: function(result){
      //   /* result = String containing user input if OK clicked or null if Cancel clicked */
      //   if (result) {
      //     ///ajax to post.....
      //
      //     data = {
      //       personid: $('#personid').val(),
      //      password: result,
      //       _token :csrf_token,
      //     }
      //     $.ajax({
      //         type: "POST",
      //         url: "{!! route('user.tempnew') !!}",
      //         data: data,
      //         success: function (response) {
      //           bootbox.alert('New user added');
      //         },
      //         error: function (response, x, y) {
      //           bootbox.alert('User could not be created');
      //         }
      //       });
      //   }
      //   }
      // });
    });


  	$('.btn-pic').tooltip();
      $uploadCrop = $('#crop-image').croppie({
         viewport: {
            width: 180,
            height: 180,
            type: 'rectangle'
         },
         boundary: {
            width: 200,
            height: 200
         },
         enableOrientation: true,
      });



    }); //end document ready

  $('#deceased').click(function() {
    if ($('#deceased').is(':checked')) {
      $('.deceased').show();
    } else {
      $('.deceased').hide();
    }
  });

  $('#married').click(function() {
    if ($('#married').is(':checked')) {
      $('.married').show();
    } else {
      $('.married').hide();
    }
  });

  $('#mailtopostal').click(function() {
    if ($('#mailtopostal').is(':checked')) {
      $('.postal').show();
    } else {
      $('.postal').hide();
    }


  });

  $('.gender').click(function () {
    //toggle maiden

    if ($(this).val() == "F") {
      $('.maidengroup').show();
    } else {
      $('.maidengroup').hide();
    }
  })

var d = new Date(00,0,1);
$( ".datepicker" ).datepicker({
  defaultDate:d,
  dateFormat: 'dd/mm/yy',
  changeMonth: true,
  changeYear: true,
  yearRange: "1900:+0", // last hundred years
  onChangeMonthYear: function (year, month, inst) {
              var date = $("#datepicker").val();
              if ($.trim(date) != "") {
                  var newDate = month + "/" + inst.currentDay + "/" + year;
                  $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
              }
          }
});

$( ".currdatepicker" ).datepicker({

  dateFormat: 'dd/mm/yy',
  changeMonth: true,
  changeYear: true,
  yearRange: "-50:+0", // last hundred years
  onChangeMonthYear: function (year, month, inst) {
              var date = $("#datepicker").val();
              if ($.trim(date) != "") {
                  var newDate = month + "/" + inst.currentDay + "/" + year;
                  $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
              }
          }
});

var godzilla = document.getElementById('selectedFile');

function roar()
{
    if(godzilla.value.length) {
      var file = $('#selectedFile')[0].files[0];
      if ($('#photo_filename').val() == file.name) {
        readURL(godzilla);
      }
      //alert('pong');
    } else {
      //alert('ping');
      $('#crop-image').hide();
      $('#saveBtn').hide();
      $('#rotateBtn').hide();
      if ($('#photo_filename').val()) {
        $('#removeBtn').show();
      }
      $('#headshot').show();
    }
    document.body.onfocus = null

}

function browseImage() {
  document.getElementById('selectedFile').click();
  document.body.onfocus = roar

}

$('#removeBtn').on('click', function (ev) {
    $('#imagebase64').val(null);
    $('#photo_filename').val('');
    $('#headshot_file').val(null);
    $('#headshot')
            .attr('src', "{{url('/images/unknown.gif')}}")
            .width(200)
            .height(200)
            .show();
    $('#removeBtn').hide();
    });

function saveImage() {
  $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
        $('#imagebase64').val(resp);
        //console.log('response: '+resp);
        $('#headshot')
                .attr('src', resp)
                .width(200)
                .height(200)
                .toggle();
        $('#crop-image').toggle();
        $('#saveBtn').toggle();
        $('#rotateBtn').toggle();
        $('#removeBtn').show();
        //$('#form').submit();
    });
}

function readURL(input) {
//  alert('ping');
        if (input.files && input.files[0]) {
          var tipe = input.files[0].type;
          if (tipe == "image/jpeg" || tipe=="image/png") {
            var reader = new FileReader();
            $('#crop-image').show();
            $('#saveBtn').show();
            $('#rotateBtn').show();
            $('#headshot').hide();
            $('#removeBtn').hide();

            reader.onload = function (e) {
            //  console.log(e.target.result);
            $uploadCrop.croppie('bind', {

                url: e.target.result
            });

            //$('.upload-demo').addClass('ready');
        }

            reader.readAsDataURL(input.files[0]);
            var file = $('#selectedFile')[0].files[0]

            $('#photo_filename').val(file.name);

            document.getElementById("help-block-head").innerHTML = "";
        } else {
          document.getElementById("help-block-head").innerHTML = "<strong>Invalid file type - please select a valid image file (jpeg or png)</strong>";
        }
      }
    }


    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    $('#street').change( function (e) {
      if ($('#street').val() != originalstreet && $('#otherresidents').length) {
        bootbox.confirm({
            message: "Should the address be updated for all residents?",
            buttons: {
                confirm: {
                    label: 'Update all',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Update this person only',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                autofillAddress();
                if (result) {
                  $('#updateallresidence').attr("value", true);
                } else {
                  $('#updateallresidence').attr("value", false);
                }
            }
        });
      } else {
        //check to see this address already exists....
        autofillAddress();
      }
    });

    function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.

            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('street')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        // alert(place.geometry.location);
        document.getElementById('gpslocation').value = place.geometry.location;

        var street = new Array("", "");
        for (var i = 0; i < place.address_components.length; i++) {

          var addressType = place.address_components[i].types[0];
          var val = place.address_components[i]['long_name'];
          //bootbox.alert(addressType + ' ' + val);
          // console.log(addressType + '=' + val);
          if (addressType == 'street_number')
            street[0] = val;
          if (addressType == 'route')
            street[1] = val;
          if (addressType == 'sublocality_level_1')
            document.getElementById('suburb').value = val;
          if (addressType == 'administrative_area_level_1')
              document.getElementById('province').value = val;
          if (addressType == 'locality')
            document.getElementById('city').value = val;
          if (addressType == 'country')
            document.getElementById('country').value = val;
          if (addressType == 'postal_code')
            document.getElementById('code').value = val;

        }

        var newStreet = street[1];
        if (street[0])
          newStreet = street[0] + ' ' + street[1];

        //console.log('current: ' + $('#street').val()+ '; new: ' + newStreet);
        // if ($('#street').val() != '' && $('#street').val() != newStreet && $('#otherresidents').length) {
        //   bootbox.confirm({
        //       message: "Should the address be updated for all residents?",
        //       buttons: {
        //           confirm: {
        //               label: 'Yes',
        //               className: 'btn-success'
        //           },
        //           cancel: {
        //               label: 'No',
        //               className: 'btn-danger'
        //           }
        //       },
        //       callback: function (result) {
        //         console.log(result);
        //           if (result) {
        //             $('#updateallresidence').attr("value", true);
        //           } else {
        //             $('#updateallresidence').attr("value", false);
        //           }
        //       }
        //   });
        // }
        document.getElementById('street').value = newStreet;
        autofillAddress();

        //but now also check whether this address already in the database for residence then populate other fields as well...
      //  autofillAddress();

      }

      var csrf_token = $('meta[name="csrf-token"]').attr('content');

      function autofillAddress() {

        if (street != "") {

    		    postdata = { street: $('#street').val(),
                         city: $('#city').val(),
                         _token :csrf_token,
    						       },
    		    $.ajax({
    								type: "POST",

    							  url: "{!! route('person.get_address') !!}",
    							  dataType: "json",

    							  data: postdata,
    								success: function (data) {

    								   if (data.exists) {

                         //create inputOptions array.....

                         console.log(data.reslist);


                         bootbox.prompt({
                            title: "Select an address from the list of member addresses:",
                            buttons: {
                                  cancel: {
                                      label: "Not in the list",
                                      className: 'btn-danger',
                                      callback: function(){
                                        //  Example.show('Custom cancel clicked');
                                      }
                                  },

                                  confirm: {
                                      label: "Select address",
                                      className: 'btn-info',
                                      callback: function(){
                                        //  Example.show('Custom OK clicked');
                                      }
                                  }
                                },

                            inputType: 'select',
                            inputOptions: data.reslist,
                            callback: function (result) {

                                //result is the resid....
                                if (result && result > -1) {
                                  //need to find the matching residence from the list of residence.....
                                  $.each(data.residence, function(index, val) {

                                    if (val.id == result) {
                                      //console.log(data.residence[0].id);
                                       $('#postal_street').val(val.postal_address);
                                        $('#res_id').val(data.residence[0].id);
                                        document.getElementById('suburb').value = val.suburb;
                                       document.getElementById('province').value = val.province;
                                       document.getElementById('city').value = val.city;
                                       document.getElementById('country').value = val.country;
                                       document.getElementById('code').value = val.code;
                                       $('#postal_address').val(val.postal_address);
                                       $('#postal_suburb').val(val.postal_suburb);
                                        $('#postal_city').val(val.postal_city);
                                       $('#postal_country').val(val.postal_country);
                                       console.log(val.mailtopostal);
                                       if (val.mailtopostal == 1) {
                                         $('#mailtopostal').attr('checked', true);
                                       } else {
                                         $('#mailtopostal').attr('checked', false);
                                       }
                                       $('#mailtopostal').val(val.mailtopostal);
                                       $('#telephone').val(val.telephone);
                                       $('#address_to').val(val.address_to);
                                       $('#code').val(val.code);

                                       if (val.mailtopostal == 1) {
                                           $('.postal').show();
                                       } else {
                                           $('.postal').hide();
                                       }
                                    }
                                  })
                                }
                              }
                            //            $('#postal_street').val(data.residence.postal_address);
                            //             $('#res_id').val(data.residence.id);
                            //             document.getElementById('suburb').value = data.residence.suburb;
                            //            document.getElementById('province').value = data.residence.province;
                            //            document.getElementById('city').value = data.residence.city;
                            //            document.getElementById('country').value = data.residence.country;
                            //            document.getElementById('code').value = data.residence.code;
                            //
                            //            $('#postal_suburb').val(data.residence.postal_suburb);
                            //             $('#postal_city').val(data.residence.postal_city);
                            //            $('#postal_country').val(data.residence.postal_country);
                            //            $('#mailtopostal').val(data.residence.mailtopostal);
                            //            $('#telephone').val(data.residence.telephone);
                            //            $('#address_to').val(data.residence.address_to);
                            //            $('#code').val(data.residence.code);
                            //
                            //            if (data.residence.mailtopostal) {
                            //                $('.postal').show();
                            //            } else {
                            //                $('.postal').hide();
                            //            }
                            //          } else {
                            //            $('#res_id').val('-1');
                            //          }
                            // }
                        });
                        //  bootbox.confirm({
                        //      message: "Is this the address: " + data.residence.street + ", " + data.residence.suburb + "?",
                        //      buttons: {
                        //          confirm: {
                        //              label: 'Yes this is the address',
                        //              className: 'btn-success'
                        //          },
                        //          cancel: {
                        //              label: 'No it is not',
                        //              className: 'btn-danger'
                        //          }
                        //      },
                        //      callback: function (result) {
                         //
                        //          if (result) {
                        //            $('#postal_street').val(data.residence.postal_address);
                        //             $('#res_id').val(data.residence.id);
                        //             document.getElementById('suburb').value = data.residence.suburb;
                        //            document.getElementById('province').value = data.residence.province;
                        //            document.getElementById('city').value = data.residence.city;
                        //            document.getElementById('country').value = data.residence.country;
                        //            document.getElementById('code').value = data.residence.code;
                         //
                        //            $('#postal_suburb').val(data.residence.postal_suburb);
                        //             $('#postal_city').val(data.residence.postal_city);
                        //            $('#postal_country').val(data.residence.postal_country);
                        //            $('#mailtopostal').val(data.residence.mailtopostal);
                        //            $('#telephone').val(data.residence.telephone);
                        //            $('#address_to').val(data.residence.address_to);
                        //            $('#code').val(data.residence.code);
                         //
                        //            if (data.residence.mailtopostal) {
                        //                $('.postal').show();
                        //            } else {
                        //                $('.postal').hide();
                        //            }
                        //          } else {
                        //            $('#res_id').val('-1');
                        //          }
                        //      }
                        //  });

    										 } else {
                           //data does not exist so ...
                           $('#res_id').val('-1');
                         }

    								 },
    								 error: function() {
    								     alert('error');
    								 }
    							});
    			}
      }


// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
          });
            autocomplete.setBounds(circle.getBounds());
          });
      }
    }





            // var i = 0;
            //
            // var $canSubmit = false;
            //
            // function submitAgain() {
            //   $("#formsubmit").submit()
            // }
            // //   var i = 0;
            //   $("#formsubmit").submit(function (event) {
            //       // If answered "ok" previously continue submitting the form.
            //       // return true;
            //       i++;
            //
            //       if (i>6) {
            //         return true;
            //       }
            //
            //       if (i > 5) {
            //         submitAgain();
            //
            //       }
            //
            //       // Confirm is not ok yet so prevent the form from submitting and show the message.
            //       event.preventDefault();
            //
            //       //sweetalert2 code
            //       console.log('ping');
            //           $("#formsubmit").submit();
            //       });
            //


              //   $("#formsubmit").submit( function(e){
              //     i++;
              //     if (i > 1) {
              //             return true;
              //           }
              //     // console.log($canSubmit);
              //     //  if (!$canSubmit) {
              //   //      console.log('returning true...');
              //   //      return true;
              //   //    } else {
              //         // console.log(canSubmit + '*');
              //
              //       // console.log(originalstreet + ' => new = ' + $('#street').val());
              //       if  (i == 1 && $('#street').val() != originalstreet && $('#otherresidents').length) {
              //         // console.log('here');
              //         e.preventDefault();
              //         bootbox.confirm({
              //             message: "Should the address be updated for all residents?",
              //             buttons: {
              //                 confirm: {
              //                     label: 'Update all',
              //                     className: 'btn-success'
              //                 },
              //                 cancel: {
              //                     label: 'Just this person',
              //                     className: 'btn-danger'
              //                 }
              //             },
              //             callback: function (result) {
              //
              //                 if (result) {
              //                   $('#updateallresidence').attr("value", true);
              //                 } else {
              //                   $('#updateallresidence').attr("value", false);
              //                 }
              //               //  console.log('here');
              //                 // $canSubmit = true;
              //                 // form.submit();
              //             }
              //         });
              //
              //       }
              //     //   else {
              //     // //   else {
              //     //     return true;
              //
              //       // console.log('returning true');
              //       form.submit();
              //     // }
              //
              //     //return true;
              //
              // //  }
              //
              //   });
              //  var form = $("#formsubmit");
              //   // // keep track of the swall popup answer.
              //   // var isConfirmed = false;
              //   var i = 0;
              //   form.submit(function (event) {
              //       // If answered "ok" previously continue submitting the form.
              //       i++;
              //       if (i > 5) {
              //         return true;
              //       }
               //
              //       // Confirm is not ok yet so prevent the form from submitting and show the message.
              //       event.preventDefault();
               //
              //       //sweetalert2 code
              //       console.log('ping');
              //           form.submit();
              //       });




</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete"
       async defer></script>
@stop
