@extends('layouts/default')

@section('content')
<div class="banner-div">
  People
  <a class="btn btn-default pull-right" href="{{ route('person.show', 0)}}" style='margin-right:10px;'>
        <i class="fa fa-plus"></i>
        <span>&nbsp; New person</span>
    </a>
</div>

<div class="datatable-list">
<table class="table table-bordered" id="users-table">

</table>
</div>


@stop

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
@section('scripts')
<script type="text/javascript">
//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table){
 var $table             = table.table().node();
 var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
 var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
 var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

 // If none of the checkboxes are checked
 if($chkbox_checked.length === 0){
    chkbox_select_all.checked = false;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If all of the checkboxes are checked
 } else if ($chkbox_checked.length === $chkbox_all.length){
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If some of the checkboxes are checked
 } else {
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = true;
    }
 }
}

var person_id = 0;
function edit_row(id) {
  //
  //
  //   $.showLoading({
  //     name: 'circle-fade'
  //   });
  //
  // if (id > 0) {
  // $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //         $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //         //$("#editbtn_"+id).button('reset');
  //     });
  //   } else {
  //   //  $('#add_verifyidModal').load('/Person/show_verify', function(response) {
  //   //     $('#add_verifyidModal').modal( {backdrop:'static'} );
  //   $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //           $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //   });
  //
  //
  // }

}

$(document).ready(function (){
 // Array holding selected row IDs
 var rows_selected = [];
 var table = $('#users-table').DataTable({
   processing: true,
   serverSide: true,
   ajax: '{!! route('people.data') !!}',
  columns: [{data: 0, name: 'id', visible: false,},
            {data: 1, name: 'firstname'},
            {data: 2, name: 'surname'},
            // {
            //   render: function(data, type, full, meta){
            //     return full[1] + ' ' + full[2];
            //   }},
            {data: 3, name: 'status'},
            {data: 4, name: 'church'},
            {data: 5, name: 'cellno'},
            {data: 0,
                searchable: false,
                orderable: false,
                width: '1%',
                className: 'dt-body-center',
              render: function(data){
                    return '<a class="btn btn-thin btn-default pull-right" href="/person/'+data+'" style="margin-right:10px;">Edit</a>';
                },}
          //   { targets: 4,
          //   searchable: false,
          //   orderable: false,
          //   width: '1%',
          //   className: 'dt-body-center',
          //   render: function (data, type, full, meta){
          //     //  var b1 = "<button class='btn btn-xs btn-success' id='editbtn_"+data[0]+"' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //     //  onclick='edit_row("+data[0]+")' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //
          // //    var b1 = "<button class='btn btn-xs btn-success' id='editbtn_1' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //   //   onclick='edit_row(1)' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //   //    return b1;
          //   return '<button title="checkbox'+data.id+'">Edit</button>';
          //   }},
        ],
        aaSorting: [[2, 'asc'], [1, 'asc']],



 });



});



</script>
@stop
