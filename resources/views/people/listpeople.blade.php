@extends('layouts/default')

@section('content')
<div class="banner-div">
  People

  @if (Auth::user()->hasRole("Admin") || Auth::user()->hasRole("SuperAdmin"))
    <a class="btn btn-default pull-right" href="{{ route('person.show', 0)}}" style='margin-right:10px;'>
          <i class="fa fa-plus"></i>
          <span>&nbsp; New person</span>
    </a>
  @endif
  @if (Auth::user()->hasRole("SuperAdmin"))
    <a class="btn btn-default pull-right" href="{{ route('users')}}" style='margin-right:10px;'>

          <span>&nbsp; System users</span>
    </a>
  @endif
</div>
<div class="container">
  <div class="statbox col-sm-3">

    Total: <?php echo $counts['all'] ;?>
    <br/>
    Total members: <? echo $counts['member']; ?>
    <br/>
    Total non-members: <? echo $counts['non']; ?>
  </div>


  <div class="statbox col-sm-3">
      {!! Form::select('bmonth',$months,1, array('id' => 'bselect', 'class' => 'form-control')) !!}
     <!-- <a  href=" route('people.downloadBirthday', ['month' => 9] )}}"> -->
      <button style="margin-bottom:5px;" id="downloadbdays" class=" btn btn-default">Download Birthdays as Excel</button>
  <!-- <a> -->
  </div>
</div>

<div class="datatable-list">
<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Status</th>
                <th>Church</th>
                <th>Contact</th>
                <th>Address</th>
                <th></th>
            </tr>
        </thead>
</table>
</div>


@stop

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
@section('scripts')
<script type="text/javascript">
//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table){
 var $table             = table.table().node();
 var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
 var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
 var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

 // If none of the checkboxes are checked
 if($chkbox_checked.length === 0){
    chkbox_select_all.checked = false;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If all of the checkboxes are checked
 } else if ($chkbox_checked.length === $chkbox_all.length){
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = false;
    }

 // If some of the checkboxes are checked
 } else {
    chkbox_select_all.checked = true;
    if('indeterminate' in chkbox_select_all){
       chkbox_select_all.indeterminate = true;
    }
 }
}


$('#downloadbdays').click(function(e) {
  var url = '/people/downloadbirthdays/' + $('#bselect').val();
  window.location=url;
//   $.get( url, function( data ) {
//
//   alert( "Load was performed." );
// });
})

var person_id = 0;
function edit_row(id) {
  //
  //
  //   $.showLoading({
  //     name: 'circle-fade'
  //   });
  //
  // if (id > 0) {
  // $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //         $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //         //$("#editbtn_"+id).button('reset');
  //     });
  //   } else {
  //   //  $('#add_verifyidModal').load('/Person/show_verify', function(response) {
  //   //     $('#add_verifyidModal').modal( {backdrop:'static'} );
  //   $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //           $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //   });
  //
  //
  // }

}

$(document).ready(function (){
 // Array holding selected row IDs
 var rows_selected = [];
 var table = $('#users-table').DataTable({
   processing: true,
   serverSide: true,
   ajax: '{!! route('people.data') !!}',
  columns: [{data: 'id', name: 'id', visible: false,},
            {data: 'firstname', name: 'firstname'},
            {data: 'surname', name: 'surname'},
            {data: 'status', name: 'status', searchable: false},
            {data: 'churchname', name: 'church', searchable: false},
            {data: 'cellno', name: 'cellno', searchable: false},
            {data: 'street', name: 'street'},
            {data: 'id',
                searchable: false,
                orderable: false,
                width: '1%',
                className: 'dt-body-center',
              render: function(data){
                    return '<a class="btn btn-thin btn-default pull-right" href="/person/'+data+'" style="margin-right:10px;">Edit</a>';
                },}
          //   { targets: 4,
          //   searchable: false,
          //   orderable: false,
          //   width: '1%',
          //   className: 'dt-body-center',
          //   render: function (data, type, full, meta){
          //     //  var b1 = "<button class='btn btn-xs btn-success' id='editbtn_"+data[0]+"' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //     //  onclick='edit_row("+data[0]+")' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //
          // //    var b1 = "<button class='btn btn-xs btn-success' id='editbtn_1' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //   //   onclick='edit_row(1)' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //   //    return b1;
          //   return '<button title="checkbox'+data.id+'">Edit</button>';
          //   }},
        ],
        aaSorting: [ [1,'asc'],[2, 'asc']],



 });



});



</script>
@stop
