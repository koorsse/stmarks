@extends('layouts/default')

@section('content')
<div class="banner-div">
  Users

</div>


<div class="datatable-list">
<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th></th>
            </tr>
        </thead>
</table>
</div>


@stop

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
@section('scripts')
<script type="text/javascript">





var person_id = 0;
function edit_row(id) {


}

$(document).ready(function (){
 // Array holding selected row IDs
 var rows_selected = [];
 var table = $('#users-table').DataTable({
   processing: true,
   serverSide: true,
   ajax: '{!! route('users.data') !!}',
  columns: [{data: 'id', name: 'id', visible: false,},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'role', searchable: false},
            {data: 'id',
                searchable: false,
                orderable: false,
                width: '1%',
                className: 'dt-body-center',
              render: function(data){
                    return '';
                },}
          //   { targets: 4,
          //   searchable: false,
          //   orderable: false,
          //   width: '1%',
          //   className: 'dt-body-center',
          //   render: function (data, type, full, meta){
          //     //  var b1 = "<button class='btn btn-xs btn-success' id='editbtn_"+data[0]+"' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //     //  onclick='edit_row("+data[0]+")' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //
          // //    var b1 = "<button class='btn btn-xs btn-success' id='editbtn_1' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //   //   onclick='edit_row(1)' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //   //    return b1;
          //   return '<button title="checkbox'+data.id+'">Edit</button>';
          //   }},
        ],
        aaSorting: [ [1,'asc'],[2, 'asc']],



 });



});



</script>
@stop
