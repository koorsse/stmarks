@extends('layouts/default')

@section('content')

  <div class="form-block">
    @if(Session::has('flash_message'))
               <div class="alert alert-success"><span class="fa icon-ok"></span><em> {!! session('flash_message') !!}</em></div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($user, ['route' => 'user.save', 'id'=>'userform']) !!}
      <!-- {!! csrf_field() !!} -->

      <div class="form-inner-wrap">
            <label>Name </label> {!!Form::text('name', Input::get('name'), ['class' => 'form-control', 'id' => 'name']) !!}
            <label>Login email </label> {!!Form::text('email', Input::get('email'), ['class' => 'form-control']) !!}
            <br/>

            <label>Password </label> <br/>{!!Form::password('password', null, ['class' => 'form-control',  'id' => 'password']) !!}
            <span id="helpBlock" class="help-block">If no change, leave blank.</span>
            <br/>
            <label>Password Confirmation</label> <br/>{!!Form::password('password_confirmation', null, ['class' => 'form-control',  'id' => 'password_confirmation']) !!}
      </div>
          <br/>
          <input type="hidden" id='userid' name="id" value="<?= $user->id ?>" >
        <input type='submit' id="submitbtn" name="submitbtn" value="Submit" class='btn btn-thin btn-success'>
          <!-- Save
        </button> -->
        <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
          Cancel
        </button>

      </form>
  </div>

@stop
