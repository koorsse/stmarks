@extends('layouts/default')

@section('content')

  <div class="form-block">
    <!-- {!! Form::model($person, ['id' => 'person-form', ]) !!} -->
    {!! Form::model($person, ['route' => 'person.save', 'files'=>true, 'id'=>'formsubmit']) !!}
      {!! csrf_field() !!}
      <!-- <div class="form-section">First Name &amp; Address</div> -->
        <div class="form-inner-wrap">
          <img src="{{ $photo_url }}" id="headshot" alt="headshot" style="border:1px solid #000000; text-align:center;width:200px;height:200px;user-drag: none; -moz-user-select: none; -webkit-user-drag: none;">
          <input type="hidden" id='photo_filename' name='photo' value='<?=$person['photo']?>' />
          <div id='crop-image' style="display:none; width:200px;height:300px;"></div>
          <input type="file"  name='headshot_file' id="selectedFile" style="display: none;" onchange="readURL(this);" />
          <input type="button" class="btn btn-pic btn-default" style="width:85px" value="Browse..." onclick="browseImage();"/>
          <a data-toggle="tooltip" data-placement="top" class="btn btn-pic btn-default" {{ (!isset($person->photo_filename ) || !$person->photo_filename) ? 'style=display:none'  : '' }} value="Remove image" id="removeBtn" title="Remove image">
            <i class="fa fa-remove " aria-hidden="true"></i>&nbsp;Remove
          </a>
          <a id='rotateBtn' data-toggle="tooltip" data-placement="top" onclick="rotateImage();" style="display:none;width:auto;" class="btn btn-pic btn-default" title="Rotate image right">
            <i class="fa fa-rotate-right " aria-hidden="true"></i><br/>&nbsp;
          </a>
          <a id='saveBtn' data-toggle="tooltip" data-placement="top"  style="display:none;width:90px" onclick="saveImage();" class="btn btn-pic btn-success btn-default" title="Set image for player">
            <i class="fa fa-check " aria-hidden="true"></i>&nbsp;Done
          </a>
          <span class="help-block has-error" id="help-block-head">
          </span>
        </br>
      </br>
            <label>Firstname </label> {!!Form::text('firstname', Input::get('firstname'), ['class' => 'form-control']) !!}
            <label>Surname </label> {!!Form::text('surname', Input::get('surname'), ['class' => 'form-control']) !!}
            <label>Title </label> {!!Form::select('title', $titlelist, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10']) !!}
            <label>Gender </label><br>
            <div style="margin-left: 30%;" class="col-sm-12 col-md-12 col-lg-12">
              <div class="col-sm-2 col-md-2 col-lg-2">
                <input class="gender" type="radio" value="M" <?= $person['gender'] == 'M' ? 'checked' : '' ?> id="gender" name="gender">
              <label for="male"> Male </label>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2">
              <input class="gender" type="radio" value="F" <?= $person['gender'] == 'F' ? 'checked' : '' ?> id="gender" name="gender">
              <label for="female">Female </label><br>
            </div>
            </div>

            <label class="maidengroup" <?= $person['gender'] == "F" ? '' : 'hidden' ?>>Maiden </label>
            <input type="text" class="maidengroup form-control" name="maiden" id="maiden" value="<?= $person['maiden']?>" <?= $person['gender'] == "F" ? '' : 'hidden' ?>>
            <label>Date of birth </label> {!!Form::text('birthday', Input::get('birthday'), ['class' => 'datepicker form-control']) !!}
        </div>
        <div class="form-inner-wrap">
            <label>Email </label> {!!Form::text('email', Input::get('email'), ['class' => 'form-control']) !!}
            <label>Mobile </label> {!!Form::text('cellno', Input::get('cellno'), ['class' => 'contact-tel form-control']) !!}
            <label>Work tel. </label> {!!Form::text('worktel', Input::get('worktel'), ['class' => 'contact-tel form-control']) !!}
            <label>Occupation </label> {!!Form::text('occupation', Input::get('occupation'), ['class' => 'form-control']) !!}
        </div>
        <div class="form-inner-wrap">
          @if (count($livethere) > 0)
          <div class="panel col-sm-12 col-md-12 col-lg-12">
            <h3>
              Other residents:
            </h3>
            @foreach($livethere as $occ)
            <div class = "col-sm-6 col-md-2">
              <div class = "thumbnail">
                 <img src ="<?= $occ->photo ?>" >
              </div>

              <div class = "caption">
                 <a  href="/person/<?= $occ->id?>" ><span class="label label-default"> {{$occ->firstname .' '.$occ->surname}}</span></a>

              </div>
           </div>
            @endforeach
          </div>
          @endif
          <label>Home tel. </label> {!!Form::text('telephone', Input::get('telephone'), ['class' => 'contact-tel form-control']) !!}
          <label>Number and street </label>
          <!-- <input class="form-control" name="street" id="street"  value=""
                 onFocus="geolocate()" type="text"></input> -->
                {!!Form::text('street', Input::get('street'), ['class' => 'form-control', 'onFocus'=>'geolocate();', 'id' => 'street']) !!}
          <label>Suburb </label> {!!Form::text('suburb', Input::get('suburb'), ['class' => 'form-control', 'id' => 'suburb']) !!}
          <label>City/town </label> {!!Form::text('city', Input::get('city'), ['class' => 'form-control', 'id' => 'city']) !!}
          <label>Province </label> {!!Form::text('province', Input::get('province'), ['class' => 'form-control', 'id' => 'province']) !!}
          <label>Country </label> {!!Form::text('country', Input::get('country'), ['class' => 'form-control', 'id' => 'country']) !!}
          <label>Address to </label> {!!Form::text('address_to', Input::get('address_to'), ['class' => 'form-control']) !!}

          <input type="hidden" id="gpslocation" name="gpslocation" value='<?= $person->gps_location?>' >
          <br>
          <label>Different postal? </label> <input type="checkbox"  value="<?= $person['mailtopostal'] ?>" name="mailtopostal" id='mailtopostal' >
          <br>
        <br>
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?> >Postal address </label>
            <input class="postal form-control" type='text' name="postal_address" id="postal_address" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> value="<?= $person["postal_address"]?>" >

          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>Suburb </label>
            <input class="postal form-control" type='text' name="postal_suburb" id="postal_suburb" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> value="<?= $person["postal_suburb"]?>" >
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>City </label>
            <input class="postal form-control" type='text' name="postal_city" id="postal_city" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> value="<?= $person["postal_city"]?>" >
          <label class="postal" <?= $person['mailtopostal'] ? '' : 'hidden' ?>>Country </label>
            <input class="postal form-control" type='text' name="postal_country" id="postal_country" <?= $person['mailtopostal'] ? '' : 'style="display:none;"' ?> value="<?= $person["postal_country"]?>" >
          <label>Code </label> {!!Form::text('code', Input::get('code'), ['class' => 'form-control', 'id' => 'code']) !!}

        </div>
        <div class="form-inner-wrap">
            <label>Church </label> {!!Form::select('church_id', $churches, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10']) !!}
            <label>Status </label> {!!Form::select('status_id', $statuslist, null, ['class' => 'selectpicker form-control', 'data-live-search'=>"true", 'data-size' => '10']) !!}
            <label>Join date </label> {!!Form::text('joindate', Input::get('joindate'), ['class' => 'form-control datepicker']) !!}
            <label>"The Voice" email </label> {!!Form::checkbox('voice_edistr', 1) !!}
        </div>

        <input type="hidden" name="id" value="<?= $person['id'] ?>" >
        <input type="hidden" id="res_id" name="res_id" value="<?= $person['res_id'] ?>" >
        <input type="hidden" id='imagebase64' name='imagebase64' value='' style='display:none;'/>

        <input type='submit' id="submit" name="submit" value="Submit" class='btn btn-thin btn-success'>
          <!-- Save
        </button> -->
        <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
          Cancel
        </button>



    </form>




  </div>

@stop

@section('scripts')
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete2"
       async defer></script> -->

<script type="text/javascript">

$(document).on('keyup', '.contact-tel', fixContactNumber);
$(document).on('keypress', '.contact-tel', fixContactNumber);
function fixContactNumber(event)
    {
//accepts two formats +27 82 123 4567 or 041 123 4567 (automatically adds spaces) does not accept letters (only numbers and +)
        var contactNumber = $(this);
        var key = event.which ? event.which : event.keyCode;
        var field = contactNumber.val();

        if ((key < 48 || key > 57) && key !== 43)
          event.preventDefault();
        if(key === 189 || key === 45 || (field[0] !== '+' && field.length === 12) || (field[0] === '+' && field.length === 15)) //189 dash, 45 insert
            event.preventDefault();

        if (field[0] !== '+') {
          if ((field.length === 3 || field.length === 7) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        } else {
          if ((field.length === 3 || field.length === 6 || field.length === 10) && (key !== 8 && key !== 46)) //backspace = 8; delete = 46
          {
              contactNumber.val(field + ' ');
          }
        }

    }


var $uploadCrop;
$(document).ready(function(){
  	$('.btn-pic').tooltip();
      $uploadCrop = $('#crop-image').croppie({
         viewport: {
            width: 180,
            height: 180,
            type: 'rectangle'
         },
         boundary: {
            width: 200,
            height: 200
         },
         enableOrientation: true,
      });



    });

  $('#mailtopostal').click(function() {
    $('.postal').toggle();
  });

  $('.gender').click(function () {
    //toggle maiden

    if ($(this).val() == "F") {
      $('.maidengroup').show();
    } else {
      $('.maidengroup').hide();
    }
  })


$( ".datepicker" ).datepicker({
  dateFormat: 'dd/mm/yy',
  changeMonth: true,
  changeYear: true,
  yearRange: "-80:+0", // last hundred years
  onChangeMonthYear: function (year, month, inst) {
              var date = $("#datepicker").val();
              if ($.trim(date) != "") {
                  var newDate = month + "/" + inst.currentDay + "/" + year;
                  $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
              }
          }
});

var godzilla = document.getElementById('selectedFile');

function roar()
{
    if(godzilla.value.length) {
      var file = $('#selectedFile')[0].files[0];
      if ($('#photo_filename').val() == file.name) {
        readURL(godzilla);
      }
      //alert('pong');
    } else {
      //alert('ping');
      $('#crop-image').hide();
      $('#saveBtn').hide();
      $('#rotateBtn').hide();
      if ($('#photo_filename').val()) {
        $('#removeBtn').show();
      }
      $('#headshot').show();
    }
    document.body.onfocus = null

}

function browseImage() {
  document.getElementById('selectedFile').click();
  document.body.onfocus = roar

}

$('#removeBtn').on('click', function (ev) {
    $('#imagebase64').val(null);
    $('#photo_filename').val('');
    $('#headshot_file').val(null);
    $('#headshot')
            .attr('src', "{{url('/images/unknown.gif')}}")
            .width(200)
            .height(200)
            .show();
    $('#removeBtn').hide();
    });

function saveImage() {
  $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
        $('#imagebase64').val(resp);
        //console.log('response: '+resp);
        $('#headshot')
                .attr('src', resp)
                .width(200)
                .height(200)
                .toggle();
        $('#crop-image').toggle();
        $('#saveBtn').toggle();
        $('#rotateBtn').toggle();
        $('#removeBtn').show();
        //$('#form').submit();
    });
}

function readURL(input) {
//  alert('ping');
        if (input.files && input.files[0]) {
          var tipe = input.files[0].type;
          if (tipe == "image/jpeg" || tipe=="image/png") {
            var reader = new FileReader();
            $('#crop-image').show();
            $('#saveBtn').show();
            $('#rotateBtn').show();
            $('#headshot').hide();
            $('#removeBtn').hide();

            reader.onload = function (e) {
            //  console.log(e.target.result);
            $uploadCrop.croppie('bind', {

                url: e.target.result
            });

            //$('.upload-demo').addClass('ready');
        }

            reader.readAsDataURL(input.files[0]);
            var file = $('#selectedFile')[0].files[0]

            $('#photo_filename').val(file.name);

            document.getElementById("help-block-head").innerHTML = "";
        } else {
          document.getElementById("help-block-head").innerHTML = "<strong>Invalid file type - please select a valid image file (jpeg or png)</strong>";
        }
      }
    }


    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    function initAutocomplete2() {

    }
    function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.

            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('street')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        // alert(place.geometry.location);
        document.getElementById('gpslocation').value = place.geometry.location;

        var street = new Array("", "");
        for (var i = 0; i < place.address_components.length; i++) {

          var addressType = place.address_components[i].types[0];
          var val = place.address_components[i]['long_name'];
          //bootbox.alert(addressType + ' ' + val);
          console.log(addressType + '=' + val);
          if (addressType == 'street_number')
            street[0] = val;
          if (addressType == 'route')
            street[1] = val;
          if (addressType == 'sublocality_level_1')
            document.getElementById('suburb').value = val;
          if (addressType == 'administrative_area_level_1')
              document.getElementById('province').value = val;
          if (addressType == 'locality')
            document.getElementById('city').value = val;
            if (addressType == 'country')
              document.getElementById('country').value = val;
              if (addressType == 'postal_code')
                document.getElementById('code').value = val;

        }

        if (street[0])
          document.getElementById('street').value = street[0] + ' ' + street[1];
        else
          document.getElementById('street').value = street[1];

        //but now also check whether this address already in the database for residence then populate other fields as well...
        autofillAddress();

      }

      var csrf_token = $('meta[name="csrf-token"]').attr('content');
      function autofillAddress() {

        if (street != "") {
    		    postdata = { street: $('#street').val(),
                         city: $('#city').val(),
                         _token :csrf_token,
    						       },
    		    $.ajax({
    								type: "POST",

    							  url: "{!! route('person.get_address') !!}",
    							  dataType: "json",

    							  data: postdata,
    								success: function (data) {

    								   if (data.exists) {
     											 $('#postal_street').val(data.residence.postal_address);
     											 $('#res_id').val(data.residence.id);
                           $('#postal_suburb').val(data.residence.postal_suburb);
     											 $('#postal_city').val(data.residence.postal_city);
                           $('#postal_country').val(data.residence.postal_country);
                           $('#mailtopostal').val(data.residence.mailtopostal);
                           $('#telephone').val(data.residence.telephone);
                           $('#address_to').val(data.residence.address_to);
                           $('#code').val(data.residence.code);

                           if (data.residence.mailtopostal) {
                               $('.postal').show();
                           } else {
                               $('.postal').hide();
                           }
    										 }

    								 },
    								 error: function() {
    								     alert('error');
    								 }
    							});
    			}
      }


// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
          });
            autocomplete.setBounds(circle.getBounds());
          });
      }
    }

    $("#formsubmit").submit( function(){

      if ($("input[type='submit']").val() == "Submit") {
        // bootbox.dialog("Please confirm if everything is correct");
        // $("input[type='submit']").val("text 2");
        // return false;
       }
    });

        $(document).ready(function () {
              //  var form = $("#formsubmit");
              //   // // keep track of the swall popup answer.
              //   // var isConfirmed = false;
              //   var i = 0;
              //   form.submit(function (event) {
              //       // If answered "ok" previously continue submitting the form.
              //       i++;
              //       if (i > 5) {
              //         return true;
              //       }
               //
              //       // Confirm is not ok yet so prevent the form from submitting and show the message.
              //       event.preventDefault();
               //
              //       //sweetalert2 code
              //       console.log('ping');
              //           form.submit();
              //       });
                });




</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8IzijkxDgMkBnK2vbiYC1ta59AAinHac&libraries=places&callback=initAutocomplete"
       async defer></script>
@stop
