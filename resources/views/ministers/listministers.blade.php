@extends('layouts/default')

@section('content')


  <div class="banner-div">
    Ministers
    <a class="btn btn-default pull-right" href="{{ route('minister.show', 0)}}" style='margin-left:2 0px;'>
          <i class="fa fa-plus"></i>
          <span>&nbsp; New minister</span>
    </a>
  </div>

  @foreach ($ministers as $mini)
  <div class="col-sm-6 col-md-4">
    <div class="card">
      <div class="card-block">
        <div class="card-header">
          <table><tr>
            @if ($mini->photo)
            <td>
              <img src="{{ $mini->photo }}" >
            </td>
            @endif
            <td><h3 class="card-title">{{$mini->firstname .' '. $mini->surname}}</h3></td></tr>
          </table>
        </div>

      </br>

      </div>
      <div class="card-footer">
        <a class="fill-div" href="{{ route('minister.show', ['id' => $mini->id])}}" class="card-button btn btn-primary">Show</a>
      </div>
    </div>
  </div>
  @endforeach


@stop
