@extends('layouts.landing')

@section('login')
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Email Verification</div>--}}
                {{--<div class="alert alert-success">--}}
                {{--Thank you for verifying your email, please click next to continue: {!!Html::linkroute('setuplogin','Next',array('confirmation_code' => $confirmation_code))!!}--}}
                {{--</div>--}}
           {{--</div>--}}
       {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group" style="padding-top: 35px;">
    <h2>Email Verification</h2>
</div>
<div class="panel-heading">Email Verification</div>
<div class="alert alert-success">
    Thank you for verifying your email, please click next to continue: {!!Html::linkroute('setuplogin','Next',array('confirmation_code' => $confirmation_code))!!}
</div>

@stop