
@extends('layouts.landingclean')
@section('login')
              <div class="form-group" >
                <!-- <h2>LOGIN</h2> -->

              <form  role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <input class="form-control" type="text" name="email" placeholder="Email address" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} " style="padding-left: 0px; padding-right: 0px;">
                  <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                  <!-- @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif -->
                </div>


                <div class="form-group " style="">
                  <button id="registersubmit" class="form-control" type="submit" name="submit">Login</button>
                </div>


                <!-- <div class="form-group">

                    <div >
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>

                </div> -->
                </form>
                </div>
                <!-- <p style="font-size: 13px; margin-bottom: 30px;"><a  href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a></p> -->
              <!-- <p style="font-size: 13px; color: #426FC8; ">Request access for your <b>Region or LFA</b></p> -->



@stop
