@extends('layouts.landing')

<!-- Main Content -->
@section('login')
      <div class="form-group" style="padding-top: 35px;">
        <h2>RESET PASSWORD</h2>
      </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                          <input class="form-control" type="text" name="email" placeholder="Email address" value="{{ old('email') }}" required autofocus>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group" >
                          <button id="registersubmit" class="form-control" type="submit" name="submit">Send Password Reset Link</button>
                        </div>



                    </form>
                </div>

@endsection
