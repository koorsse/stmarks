@extends('layouts/default')

@section('content')
<div class="banner-div">
  Weddings
  <a class="btn btn-default pull-right" href="{{ route('wedding.show', 0)}}" style='margin-left:2 0px;'>
        <i class="fa fa-plus"></i>
        <span>&nbsp; New wedding</span>
  </a>
</div>

<div class="datatable-list">
<table class="table table-bordered" id="wedding-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Couple</th>
                <th>Minister</th>
                <th>Venue</th>
                <th>Date</th>
                <th>Time</th>
                <th></th>
            </tr>
        </thead>
</table>
</div>

@stop

@section('scripts')
<script type="text/javascript">
//
// Updates "Select all" control in a data table
//


var person_id = 0;
function edit_row(id) {
  //
  //
  //   $.showLoading({
  //     name: 'circle-fade'
  //   });
  //
  // if (id > 0) {
  // $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //         $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //         //$("#editbtn_"+id).button('reset');
  //     });
  //   } else {
  //   //  $('#add_verifyidModal').load('/Person/show_verify', function(response) {
  //   //     $('#add_verifyidModal').modal( {backdrop:'static'} );
  //   $('#editPersonModal').load('/Person/show/'+id, function(response) {
  //           $('#editPersonModal').modal( {backdrop:'static'} );
  //           $.hideLoading();
  //   });
  //
  //
  // }

}

$(document).ready(function (){
 // Array holding selected row IDs
 var rows_selected = [];
 var table = $('#wedding-table').DataTable({
   processing: true,
   serverSide: true,
   ajax: '{!! route('wedding.data') !!}',
  columns: [{data: 'id', name: 'id', visible: false,},
            {data: 'couple', name: 'couple'},
            {data: 'minister', name: 'minister'},
            {data: 'venue', name: 'venue'},
            {data: 'wedding_date', name: 'wedding_date'},
            {data: 'wedding_time', name: 'wedding_time'},
            {data: 'id',
                searchable: false,
                orderable: false,
                width: '1%',
                className: 'dt-body-center',
              render: function(data){
                    return '<a class="btn btn-thin btn-default pull-right" href="/wedding/'+data+'" style="margin-right:10px;">Show</a>';
                  }}

          //   { targets: 4,
          //   searchable: false,
          //   orderable: false,
          //   width: '1%',
          //   className: 'dt-body-center',
          //   render: function (data, type, full, meta){
          //     //  var b1 = "<button class='btn btn-xs btn-success' id='editbtn_"+data[0]+"' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //     //  onclick='edit_row("+data[0]+")' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //
          // //    var b1 = "<button class='btn btn-xs btn-success' id='editbtn_1' data-loading-text=\"<i class='fa fa-spinner fa-spin '></i>\"
          //   //   onclick='edit_row(1)' title='View Person'><i class='fa fa-fw fa-edit'></i></button>";
          //   //    return b1;
          //   return '<button title="checkbox'+data.id+'">Edit</button>';
          //   }},
        ],
        aaSorting: [[4, 'desc'], [5, 'asc']],



 });



});



</script>
@stop
