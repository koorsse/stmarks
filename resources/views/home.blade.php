
@extends('layouts/default')

@section('content')

hello

@if (Auth::user()->hasRole('SuperAdmin'))

  <button  id="submitMailchimp" name="submit" value="Submit" class='btn btn-thin btn-success'>
    MAILCHIMP
  </button>
  <button  id="submitFB" name="submit" value="Submit" class='btn btn-thin btn-success'>
    FB
  </button>
  <button  id="submitMail" name="submit" value="Submit" class='btn btn-thin btn-success'>
    MAIL TEST
  </button>


@endif




@stop

@section('scripts')
<script type="text/javascript">
var csrf_token = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function() {


  $('#submitMailchimp').click(function () {


    postdata = {
                 _token :csrf_token,
               },
    $.ajax({
            type: "POST",

            url: "{!! route('devotional.test') !!}",
            dataType: "json",

            data: postdata,
            success: function (data) {
              alert('success');
            },
            error: function() {
                alert('error');
            }
         });
  });

  $('#submitFB').click(function () {


    postdata = {
                 _token :csrf_token,
               },
    $.ajax({
            type: "POST",

            url: "{!! route('devotional.testFB') !!}",
            dataType: "json",

            data: postdata,
            success: function (data) {
              alert('success');
            },
            error: function() {
                alert('error');
            }
         });
  });

  $('#submitMail').click(function () {


    postdata = {
                 _token :csrf_token,
               },
    $.ajax({
            type: "POST",

            url: "{!! route('mail.test') !!}",
            dataType: "json",

            data: postdata,
            success: function (data) {
              alert('success');
            },
            error: function() {
                alert('error');
            }
         });
  });


});

</script>
@append
