@extends('layouts/default')


@section('content')
<div class="banner-div">
  Churches
  <a class="btn btn-default pull-right" href="{{ route('church.show', 0)}}" style='margin-left:2 0px;'>
        <i class="fa fa-plus"></i>
        <span>&nbsp; New church</span>
  </a>
</div>
<!--
<div class="row" style='margin-bottom: 5px'>



  </div> -->
<div class="row">

  @foreach ($churches as $kerk)
  <div class="col-sm-6 col-md-4">
    <div class="card">
      <div class="card-block">
        <div class="card-header">
          <table><tr>
          @if ($kerk->churchpic)
            <td>
              <img src="{{ $kerk->churchpic }}" >
            </td>
          @endif
            <td><h3 class="card-title">{{$kerk->churchname}}</h3></td></tr>
          </table>
        </div>
        <p class="card-text">Email: {{$kerk->email}}</p>
        <p class="card-text">Tel: {{$kerk->contacttel}}</p>

      </br>
    </br>
      </div>
      <div class="card-footer">
        <a class="fill-div" href="{{ route('church.show', ['id' => $kerk->id])}}" class="card-button btn btn-primary">Show</a>
      </div>
    </div>
  </div>
  @endforeach
</div>

@stop
