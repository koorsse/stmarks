
<!-- Sidebar -->
<div class="s-layout__sidebar">
  <a class="s-sidebar__trigger" href="#0">
     <i class="fa fa-bars"></i>
  </a>

  <nav class="s-sidebar__nav">
     <ul>
       <li><a class="s-sidebar__nav-link" href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
       <li><a class="s-sidebar__nav-link" href="{{ url('/people') }}"><i class="fa fa-users"></i> <span>People</span></a></li>
       @if (Auth::user()->hasRole('SuperAdmin'))
         <li><a class="s-sidebar__nav-link" href="{{ url('/church') }}"><i class="fa fa-university"></i> <span >Churches</span></a></li>
         <li><a class="s-sidebar__nav-link" href="{{ url('/minister') }}"><i class="fa fa-user-circle"></i> <span>Ministers</span></a></li>
         <li><a class="s-sidebar__nav-link" href="{{ url('/ministry') }}"><i class="fa fa-handshake-o"></i> <span>Ministries</span></a></li>
         <li><a class="s-sidebar__nav-link" href="{{ url('/wedding') }}"><i class="fa fa-bell"></i> <span>Weddings</span></a></li>
         <li><a class="s-sidebar__nav-link" href="{{ url('/funeral') }}"><i class="fa fa-book"></i> <span>Funerals</span></a></li>
         <li><a class="s-sidebar__nav-link" href="{{ url('/cradleroll') }}"><i class="fa fa-file-text"></i> <span>Cradleroll</span></a></li>
       @endif


     </ul>
  </nav>
</div>

        <!-- <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">

            <ul class="nav" id="menu">
              @if (Auth::user())
                <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span class="collapse in hidden-xs">Dashboard</span></a></li>
                <li><a  href="{{ url('/people') }}"><i class="fa fa-users"></i> <span class="collapse in hidden-xs">People</span></a></li>
                <li><a  href="{{ url('/church') }}"><i class="fa fa-university"></i> <span class="collapse in hidden-xs">Churches</span></a></li>
                <li><a  href="{{ url('/minister') }}"><i class="fa fa-user-circle"></i> <span class="collapse in hidden-xs">Ministers</span></a></li>
                <li><a  href="{{ url('/ministry') }}"><i class="fa fa-handshake-o"></i> <span class="collapse in hidden-xs">Ministries</span></a></li>
                <li><a  href="{{ url('/wedding') }}"><i class="fa fa-bell"></i> <span class="collapse in hidden-xs">Weddings</span></a></li>
                <li><a  href="{{ url('/funeral') }}"><i class="fa fa-book"></i> <span class="collapse in hidden-xs">Funerals</span></a></li>
                <li><a  href="{{ url('/cradleroll') }}"><i class="fa fa-file-text"></i> <span class="collapse in hidden-xs">Cradleroll</span></a></li>

              @endif
            </ul>
        </div> -->

@section('scripts')
<script type="text/javascript">

</script>
@append
