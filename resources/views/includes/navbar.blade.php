<nav class="navbar navbar-inverse" style="background-color: #fff;">
  <div class="container-fluid" style="margin-right:25px;background-color: #fff;">

    <div class="navbar-header" >
      <!-- <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a> -->
    <!-- <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a> -->
      <a class="navbar-brand" style="margin-left: 10px;" href="{{ url('/home') }}"><img src="{{asset('images/stm_logo_1.png')}}" height="40px"/></a>
    </div>
    <ul class="nav navbar-nav navbar-right">

      @if (Auth::user())
        <li><a><span id='userdetails'  class="label label-default"></span></a></li>
      @endif
      @if (Auth::guest())
          <li><a href="{{ url('/login') }}">Login</a></li>
          <li><a href="{{ url('/register') }}">Register</a></li>
      @else
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                  {{--// aria-expanded="false">--}}
                  {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                  <li>
                      <a  href="{{ route('user.account')}}">
                          My Account
                      </a>
                      <a href="{{ url('/logout') }}"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                          Logout
                      </a>

                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </li>
              </ul>
          </li>
      @endif
    </ul>
  </div>
</nav>

@section('scripts')
<script type="text/javascript">
$('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
    $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
});
</script>
@append
