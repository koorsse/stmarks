
        <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">

            <ul class="nav" id="menu">

                <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span class="collapse in hidden-xs">Dashboard</span></a></li>
                @if (Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Daily'))
                  <li><a  href="{{ url('/devotional') }}"><i class="fa fa-bookmark"></i> <span class="collapse in hidden-xs">Daily Devotional</span></a></li>
                @endif
                <li><a  href="{{ url('/people') }}"><i class="fa fa-users"></i> <span class="collapse in hidden-xs">People</span></a></li>
               
                @if (Auth::user()->hasRole('SuperAdmin'))
                  <li><a  href="{{ url('/childrens') }}"><i class="fa fa-users"></i> <span class="collapse in hidden-xs">Childrens Church</span></a></li>
                  <li><a  href="{{ url('/church') }}"><i class="fa fa-university"></i> <span class="collapse in hidden-xs">Churches</span></a></li>
                  <li><a  href="{{ url('/minister') }}"><i class="fa fa-user-circle"></i> <span class="collapse in hidden-xs">Ministers</span></a></li>
                  <li><a  href="{{ url('/ministry') }}"><i class="fa fa-handshake-o"></i> <span class="collapse in hidden-xs">Ministries</span></a></li>
                  <li><a  href="{{ url('/wedding') }}"><i class="fa fa-bell"></i> <span class="collapse in hidden-xs">Weddings</span></a></li>
                  <li><a  href="{{ url('/funeral') }}"><i class="fa fa-book"></i> <span class="collapse in hidden-xs">Funerals</span></a></li>
                  <li><a  href="{{ url('/cradleroll') }}"><i class="fa fa-file-text"></i> <span class="collapse in hidden-xs">Cradleroll</span></a></li>
                  <li><a  href="{{ url('/schedulelist') }}"><i class="fa fa-file-text"></i> <span class="collapse in hidden-xs">Schedule</span></a></li>
                @endif


            </ul>
        </div>

@section('scripts')
<script type="text/javascript">

</script>
@append
