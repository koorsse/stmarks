@extends('layouts/default')
<style>
    .draft a{
     background-color : Orange !important;
     background-image :none !important;
     color: White !important;
     font-weight:bold !important;
     /*font-size: 11pt;*/
    }

    .review a{
       background-color : Yellow !important;
       background-image :none !important;
       color: #eee !important;
       font-weight:bold !important;
       /*font-size: 12pt;*/
    }

    .ready a{
       background-color : Green !important;
       background-image :none !important;
       color: White !important;
       font-weight:bold !important;
       /*font-size: 12pt;*/
    }

    .problem a{
       background-color : Red !important;
       background-image :none !important;
       color: White !important;
       font-weight:bold !important;
       /*font-size: 12pt;*/
    }
</style>
@section('content')

<div class="form-block">

  {!! Form::model($daily, ['route' => 'devotional.save', 'id'=>'dailyform']) !!}
    {!! csrf_field() !!}

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

      <!-- @if ($didpublish == 2)
        <div class="alert alert-danger">State: This devotional was not published</div>
      @elseif (!$canpublish)
        <div class="alert alert-warning">State: Draft - change draft to publish when ready to publish</div>
      @elseif ($canpublish && $didpublish == 0)
        <div class="alert alert-info">State: Ready to publish - change back to draft before to make changes before publishing</div>
      @else
        <div class="alert alert-success">State: Ready to publish - change back to draft before to make changes before publishing</div>
      @endif -->

      @if ($daily->id > 0 && $didpublish < 1)
        @include('dailydevotional.progressbar')
        <?php
          $todaydate = date('Y-m-d');
          $next_date = date('Y-m-d', strtotime($todaydate .' +1 day'));
          if ($todaydate == $thisnicedate) {
            echo "<div class='alert alert-info'>This devotional will be published at 06:13 today.</div>";
          } else if ($next_date == $thisnicedate) {
            echo "<div class='alert alert-info'>This devotional will be published at 06:13 tomorrow.</div>";
          }
        ?>
      @elseif ($didpublish == 1)
        <div class="alert alert-success">Devotional was published</div>
      @elseif ($didpublish == 2)
        <div class="alert alert-danger">Devotional not published - changes can be made for archive</div>
      @elseif ($daily->id == 0)
        <div class="alert alert-info">No devotional saved for this day</div>
      @endif
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-1" style="vertical-align: center;">Go to:</div>

        <div hidden>
          {!!Form::select('thedateslist', $thedateslist, key(json_decode($thedateslist, true)), ['id' => 'thedateslist', 'class' => 'form-control', 'hidden' => true]) !!}
        </div>
        <div class="col-md-3 ">
          <!-- <div id="datelist" > </div> -->

          {!! Form::text('datelist', $thisnicedate, ['class' => 'form-control', 'id' => 'datelist']) !!}
         </div>
      </div>
      <div class="row">

          <div class="col-md-2"><a href="{{ route('devotional.edit', ['thedate' => $prevdate]) }}" id="previouslink">Previous</a></div>
          <div class="col-md-8" style="text-align: center;"><p><h2><?= $displaydate ?></h2></p></div>
          <div class="col-md-2" style="text-align:right;"><a href="{{route('devotional.edit', ['thedate' => $nextdate ])}}" id="nextlink">Next</a></div>

      </div>




      <!-- <label>Date </label> {!!Form::text('message_date', Input::get('message_date'), ['class' => 'currdatepicker form-control', 'readonly' => $daily->has_published]) !!} -->
      <input type="hidden" name="message_date" value=<?= $daily['message_date'] ?> >
      <label>Author </label> {!!Form::text('author', Input::get('author'), ['class' => 'form-control', 'readonly' => $didpublish == 1]) !!}
      <label>Title </label> {!!Form::text('title', Input::get('title'), ['class' => 'form-control', 'readonly' => $didpublish == 1]) !!}
      <label>Message </label> {!!Form::textarea('messagebody', Input::get('messagebody'), ['class' => 'form-control', 'readonly' => $didpublish == 1]) !!}
      <label>Verse text </label> {!!Form::textarea('versetext', Input::get('versetext'), ['class' => 'form-control', 'readonly' => $didpublish == 1, 'rows' => 4]) !!}
      <label>Verse ref </label> {!!Form::text('verseref', Input::get('verseref'), ['class' => 'form-control', 'readonly' => $didpublish == 1]) !!}
      <label>Prayer </label> {!!Form::text('prayer', Input::get('prayer'), ['class' => 'form-control', 'readonly' => $didpublish == 1]) !!}
<br/>

      <input type="hidden" id='dailyid' name="id" value="<?= $daily['id'] ?>" >



@if (!$daily->has_published)
      <input type='submit' id="submitbtn" name="submitbtn" value="Save" class='btn btn-thin btn-success'>
        <!-- Save
      </button> -->
      <button type='submit' name='cancel' value="Cancel" class='btn btn-thin btn-default'>
        Cancel
      </button>
@endif
      @if ($daily->status == "Draft")
          <input type='submit' id="submitbtn" name="review" value="Submit for review" class='btn btn-thin btn-success'>
      @endif
      @if ($daily->status == "Review" && (Auth::user()->hasRole('SuperAdmin') || $canready))
          <input type='submit' id="submitbtn" name="ready" value="Ready to publish" class='btn btn-thin btn-success'>
      @endif

      <?php
         $buildurl = url('devotional/email') . "/" . $thisnicedate;
         echo "<a target='_blank' href='".$buildurl."' class='btn btn-thin btn-default'>Email preview</a>";
      ?>
      <!-- Facebook testing and review only -->
      <!-- if (Auth::user()->id == 1)

        <input type='submit' id="fbbtn" name="fbbtn" value="Publish to Facebook" class='btn btn-thin btn-success'>
      endif -->


    </form>
</div>





@stop

@section('scripts')

<script type="text/javascript">

jQuery(document).ready(function() {

     // An array of dates
     var eventDates = {};
     var draftDates = {};
     var readyDates = {};
     var reviewDates = {};
     var listItems = $("#thedateslist");

     var listItems2 =  <?php echo json_encode($thedateslist) ?>;
    //  console.log(listItems2);
     $.each(listItems2, function (i, elem) {
        // console.log(i);
  //      listItems2.each(function(idx, li) {
        // var product = $(li);
        //  console.log(new Date(product.val()).toString());
        if (elem.indexOf("DRAFT") >= 0) {
          draftDates[new Date(i)] = elem;//new Date(product.val()).toString();
        } else if (elem.indexOf("REVIEW") >= 0) {
          reviewDates[new Date(i)] = elem;//new Date(product.val()).toString();
        } else if (elem.indexOf("READY") >= 0) {
          readyDates[new Date(i)] = elem;//new Date(product.val()).toString();
        }
        // console.log(product.text());
      });

      // console.log(draftDates);
     //
    //  eventDates[ new Date( '11/18/2018' )] = new Date( '11/18/2018' ).toString();
    //  eventDates[ new Date( '11/11/2018')] = new Date( '11/11/2018' ).toString();
    //  eventDates[ new Date( '11/15/2018' )] = new Date( '11/03/2018' ).toString();
    //  eventDates[ new Date( '11/03/2018')] = new Date( '11/03/2018' ).toString();

     // datepicker
     jQuery('#datelist').datepicker({
         dateFormat: 'yy-mm-dd',
         beforeShowDay: function( date ) {
            //  var edate = eventDates[date];
            //  console.log(date);
             var highDraft = draftDates[date];
             var highReview = reviewDates[date];
             var highReady = readyDates[date];
             var todaydate = new Date();


            //  return [true, '', ''];
            //  if (edate) {
            //    return [true, "draft", edate];
            //  } else {
            //    return [true, '', ''];
            //  }
            // console.log(highDraft);
             if (date.getDay() == 0 || date.getDay() == 6) {
               return [true, '', ''];
             }
             else if( highDraft ) {
                  return [true, "draft", highDraft];
             } else if (highReview) {
                  return [true, "review", highReview];
             } else if (highReady) {
              //  console.log(highReady);
                  return [true, "ready", highReady];
             } else if (date > todaydate) {
                  return [true, 'problem', 'NO Devotional'];
             } else  {
                  return [true, '', ''];
             }
          },
          onSelect: function (dateText, inst) {
                  // var date = $("#age_at_date").val();
                  // if ($.trim(date) != "") {
                    var date = $(this).datepicker('getDate');
                  //  console.log(date);

                     day  = date.getDate();
                     month = date.getMonth() + 1;
                     year =  date.getFullYear();
                     if (day < 10) {
                      var newDate = year + "-" + month + "-0" + day;
                    } else {
                      var newDate = year + "-" + month + "-" + day;
                    }

                  //   console.log(newDate)
                    //   $("#age_at_date").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));
                      window.location.replace("/devotional/"
                     + newDate);

              }
     });
 });

    $(function() {
      $('#toggle-two').bootstrapToggle({
        on: 'Publish',
        off: 'Draft'
      });
    })

    $( ".currdatepicker" ).datepicker({

      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      yearRange: "-50:+0", // last hundred years
      onChangeMonthYear: function (year, month, inst) {
                  var date = $("#datepicker").val();
                  if ($.trim(date) != "") {
                      var newDate = month + "/" + inst.currentDay + "/" + year;
                      $("#datepicker").val($.datepicker.formatDate('dd/mm/yy', new Date(newDate)));

                      getDailyDevotional(newDate);
                  }
              }
    });


    function getDailyDevotional(newDate)
    {
        //populate the fields based on the date of the daily devotional....


    }

    $('#preview').click(function(e) {
      //use daily message_date to
      var win = window.open('http://stackoverflow.com/', '_blank');
      if (win) {
          //Browser has allowed it to be opened
          win.focus();
      } else {
          //Browser has blocked it
          alert('Please allow popups for this website');
      }
    });



</script>

@stop
