<html>
<body>
<style>

  .date {
    color: #7C7C7C;
  }

  .daily .verse {
    margin-top: 15px;
    background-color: #fff;

  }

  .message {
    font-family: Source Sans Pro, sans-serif;
  }


  @media (max-width: 40em) {

  }

  @media screen and (max-width: 1000px) {

  }

</style>



<div class="row" style="margin: 10px 0px;">
  <div class="col-md-2 date">
    <p style="text-align: center; font-weight: 400; font-size: 16px; line-height: 30px; margin-bottom: 0;"><span style="font-size: 33px;">Monday</span><br />3 September 2018</p>
  </div>
  <div class="col-md-7" style="text-align: center;">
    <span style="font-size: 40pt; line-height: 115%; color: #1f497d;">Time To Bond</span>

  </div>
  <div class="col-md-3" style="vertical-align: center;">

    <p style="padding-top: 15px;text-align: center; font-weight: 600; color: #004661; font-size: 16px; line-height: 30px; margin-bottom: 0;">by <b>Pastor Kenneth Woolard</b></p>
  </div>
</div>
<div class="container" style="width: 100%; background-color: #fff; margin: 0; padding:0;">
  <div class="col-md-7 col-md-offset-2 daily">
    <div class="col-md-12 verse">
      <p style="text-align: center;"><i>“After these things came Jesus and his disciples into the land of Judea; and there he tarried with them…”</i></p>
      <p style="text-align: right;">
        <b><a style="decoration:none;" class="rtBibleRef" href="https://bible.faithlife.com/bible/nkjv/John%203.22" data-reference="John 3.22" data-version="nkjv" data-purpose="bible-reference" target="_blank">John 3:22</a><b>
      </p>
    </div>
    <div class="message col-md-12" style="margin-top: 10px;">
      <p>Time is a precious commodity for most people. To be effective and efficient you need to manage your time wisely. Some can achieve a lot in a very short time while others get through very little. Yet, both parties can feel “busy”.</p>
      <p>Jesus spent time with His disciples. His earthly ministry was only around 3 years and He accomplished so much - and He still found time to spend with His disciples. The time He spent with them would, no doubt, have encouraged and help deepen their faith and widen their understanding of what they needed to do. This time was of extreme value to both Christ and the disciples.</p>
      <p>We, too, should never forget the value of time we spend unnecessarily. The value of your personal, quiet time with the Lord is of extreme importance to your faith and relationship with Christ. Take the time to let Christ deepen your faith and widen your understanding of what is important to Him, so you can be as effective as He needs you to be.</p>
    </div>
  </div>
  <div class="col-md-3 prayer">
    <h2 style="text-align: center; font-weight: 100; font-size: 51px; font-family: Source Sans Pro; font-style: italic; color: #494949;">Prayer</h2>
    <p style="text-align: center;"><b><i>Dear Lord, help me to see the importance of Your plan in my life.</i></b></p>
    <p style="text-align: center; color: #494949; font-weight: bold;">Amen</p>
  </div>
</div>







<script>
	var refTagger = {
		settings: {
			bibleVersion: "NKJV",
			roundCorners: true
		}
	};
	(function(d, t) {
		var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
		g.src = "//api.reftagger.com/v2/RefTagger.js";
		s.parentNode.insertBefore(g, s);
	}(document, "script"));
</script>
</body>
</html>
