<div class="row bs-wizard " style="border-bottom:0;">

  <div class="col-xs-2  bs-wizard-step <?= $steps[0] ?>">
    <div class="text-center <?= $steps[4] ?>">Draft</div>
    <div class="progress"><div class="progress-bar2"></div></div>
    <a href="#" onclick="document.getElementById('newleague').click();" class="bs-wizard-dot"></a>
    <div class="bs-wizard-info text-center"></div>
  </div>

  <div class="col-xs-2  bs-wizard-step <?= $steps[1] ?>">
    <div class="text-center <?= $steps[5] ?>">Review</div>
    <div class="progress"><div class="progress-bar2"></div></div>
    <a href="#" onclick="document.getElementById('newclub').click();" class="bs-wizard-dot"></a>
    <div class="bs-wizard-info text-center"></div>
  </div>

    <div class="col-xs-2  bs-wizard-step <?= $steps[2] ?>">
      <div class="text-center <?= $steps[6] ?>">Ready to publish</div>
      <div class="progress"><div class="progress-bar2"></div></div>
      <a href="#" onclick="document.getElementById('newclub').click();" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center"></div>
    </div>

    <div class="col-xs-2 bs-wizard-step <?= $steps[3] ?>">
      <div class="text-center <?= $steps[7] ?>">Published</div>
      <div class="progress"><div class="progress-bar"></div></div>
      <a href="#" onclick="document.getElementById('progbarid').click();" class="bs-wizard-dot"></a>
      <div class="bs-wizard-info text-center"></div>
    </div>


</div>
