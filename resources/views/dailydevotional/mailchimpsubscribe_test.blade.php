<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
  body {
    font-size: 50px;
  }
    @media only screen and (max-width: 1000px) {
      #mc_embed_signup{background:#fff; clear:left; font:30px Helvetica,Arial,sans-serif; }

      body {
        font-size: 50px;
      }
      label {
        font-size: 50px;
      }
      input {
        font-size: 60px;

      }

      #mc_embed_signup .indicates-required {
          text-align: right;
          font-size:35px;
          margin-right: 4%;
      }

      #mc_embed_signup .button {
        clear: both;
        background-color: #aaa;
        border: 0 none;
        border-radius: 4px;
        transition: all 0.23s ease-in-out 0s;
        color: #FFFFFF;
        cursor: pointer;
        display: inline-block;
        font-size: 70px;
        font-weight: normal;
        height: 80px;
        line-height: 80px;
        margin: 0 5px 10px 0;
        padding: 0 22px;
        text-align: center;
        text-decoration: none;
        vertical-align: top;
        white-space: nowrap;
        width: auto;
      }


    }
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div class="col-md-offset-3 col-md-6 col-sm-12">
  <div id="mc_embed_signup">
    <div style="text-align: center;padding-top:15px;">
      <img src="{{asset('images/stm_logo_1.png')}}" height="100px"/>
    </div>
  <form action="https://stmarks.us16.list-manage.com/subscribe/post?u=8410d0f9f6de03558d88555e1&amp;id=7d564e3c7f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
      <div id="mc_embed_signup_scroll">
  	<h2>Subscribe to our daily devotional mailing list</h2>
  <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
  <div class="mc-field-group">
  	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
  </label>
  	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
  </div>
  <div class="mc-field-group">
  	<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
  </label>
  	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
  </div>
  <div class="mc-field-group">
  	<label for="mce-LNAME">Last Name  <span class="asterisk">*</span>
  </label>
  	<input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
  </div>
	<div class="mc-field-group input-group">
    <strong>Mailing lists </strong>
    <ul><li><input type="checkbox" value="1" name="group[5153][1]" id="mce-group[5153]-5153-0"><label for="mce-group[5153]-5153-0">The Voice</label></li>
<li><input type="checkbox" value="2" name="group[5153][2]" id="mce-group[5153]-5153-1"><label for="mce-group[5153]-5153-1">Daily Devotional</label></li>
<li><input type="checkbox" value="4" name="group[5153][4]" id="mce-group[5153]-5153-2"><label for="mce-group[5153]-5153-2">Faith-Full Living</label></li>
</ul>
</div>
	  	<div id="mce-responses" class="clear">
  		<div class="response" id="mce-error-response" style="display:none"></div>
  		<div class="response" id="mce-success-response" style="display:none"></div>
  	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8410d0f9f6de03558d88555e1_7d564e3c7f" tabindex="-1" value=""></div>
      <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
      </div>
  </form>
  </div>
</div>

<!--End mc_embed_signup-->
