@extends('layouts/defaultdaily')
<style>
  .body {
    font-family: Source Sans Pro;
  }
  .date {
    color: #7C7C7C;
  }

  .daily .verse {
    margin-top: 15px;
    background-color: #fff;

  }

  .daily-header {
    background-image: linear-gradient(to right,#004661,#4a99aa 40%,#4a99aa 60%, #004661 100%);
    font-family: Source Sans Pro, sans-serif;
    font-size: 78px;
    color: #fff;
    font-weight: 100;

  }

  a {
    color: rgb(0, 69, 99);
    text-decoration: none;
    font-weight: 800;
  }

  a:hover, a:active, a:visited {
    text-decoration: none;
    color: rgb(0, 69, 99);

  }

  .contactblock {
    font-size: 16px;
    font-weight: lighter;
    line-height: 1.0;
    vertical-align: center;
    margin-top: 20px;
    font-family: Source Sans Pro;
    color: rgba(255, 255, 255, 0.75);
  }

  .message {
    font-family: Source Sans Pro, sans-serif;
    font-weight: 400;
    text-align: justify;
  }

  .response-center {
    text-align: left;
  }


  @media (max-width: 40em) {
    .response-center {
      text-align: center;

    }
  }

  @media screen and (max-width: 1000px) {
      .response-center {
        text-align: center;

      }
  }

</style>

@section('content')
<div class="daily-header col-md-12 col-sm-12 " style="padding: 0px;">
  <div class="col-md-2" style="text-align:center; margin:10px 0px 10px 0px;">
    <img src="{{asset('images/sm-logo-vertical.png')}}" height="80px"/>
  </div>
  <div class="col-md-7 responsive-center">
    <div style="margin-left: 12px; font-weight:200; font-family: Source Sans Pro;"><i>Daily Devotional</i></div>
  </div>
  <div class="col-md-3 contactblock">
    <p>Tel: +27 41 360 6060</p>
    <p>Email: devotional@stmarks.org.za</p>
    <p style="font-size: 18px; font-family: Open Sans Condensed;"><b>www.stmarks.org.za</b></p>
  </div>
</div>

<div class="row" style="background-color: #eee; margin: 5px 0px; padding-top: 5px; font-family: Source Sans Pro, sans-serif;">
  <div class="col-md-2 date">
    <p style="text-align: center; font-weight: 600;  font-size: 16px; line-height: 25px; margin-bottom: 0; padding-top: 20px;"><span style="font-size: 24px;">{{ $dayweek }}</span><br /><span style="font-size: 14px;">{{ $displaydate }}</span></p>
  </div>
  <div class="col-md-7 response-center">
    <p style="font-size: 51pt; font-weight: 600;  line-height: 115%; color: rgb(0, 69, 99); margin-left: 12px; padding-top:5px;"><i>{{ $daily->title }}</i></p>

  </div>
  <div class="col-md-3" style="padding-top:15px;">

    <p style="padding-top: 25px;text-align: left; font-weight: 600; color: rgb(0, 69, 99); font-size: 18px; line-height: 30px; margin-bottom: 0;">by <b>{{ $daily->author }}</b></p>
  </div>
</div>
<div class="container" style="width: 100%; background-color: #fff; margin: 0; padding:0;">
  <div class="col-md-7 col-md-offset-2 daily">
    <div class="col-md-12 verse" style=" margin-bottom:10px;">
      <p style="text-align: justify; font-size: 18px; color: rgb(0, 69, 99);"><i>“{{$daily->versetext}}”</i></p>
      <p style="text-align: left; font-size: 18px; color: rgb(0, 69, 99);">
        <b><a class="rtBibleRef" href="https://bible.faithlife.com/bible/nkjv/{{$verseformat}}" data-reference="{{ $verseformat }}" data-version="nkjv" data-purpose="bible-reference" target="_blank">{{ $daily->verseref }}</a><b>
      </p>
    </div>
    <hr/>
    <div class="message col-md-12" style="margin-top: 10px; font-family: Source Sans Pro; font-size: 16px;">
  <?php echo str_replace( "\n", '<br />', $daily->messagebody ); ?>
  <!-- {{ htmlentities(nl2br($daily->messagebody)) }} -->
    </div>
  </div>
  <div class="col-md-3 prayer"  style="padding: 15px;">
    <div class="col-md-12" style="border: 2px #eee solid;min-height: 250px; font-family: Source Sans Pro; color: rgb(88,89,91);">
      <h2 style="text-align: center; padding-bottom: 10px; font-weight: 200; font-size: 51px; font-family: Source Sans Pro; font-style: italic; ">Prayer</h2>
      <p style="text-align: center; padding-bottom: 10px; font-size: 18px; font-weight: 600;"><b><i>{{ $daily->prayer }}</i></b></p>
      <p style="text-align: center; font-weight: 800; font-size: 26px;"><b><i>Amen</i></b></p>
    </div>
  </div>
</div>




@stop

@section('scripts')
<script>
	var refTagger = {
		settings: {
			bibleVersion: "NKJV",
			roundCorners: true
		}
	};
	(function(d, t) {
		var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
		g.src = "//api.reftagger.com/v2/RefTagger.js";
		s.parentNode.insertBefore(g, s);
	}(document, "script"));
</script>

@append
